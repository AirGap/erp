unit ufrmBaseSanction;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, Vcl.ComCtrls, dxCore,
  cxDateUtils, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxNavigator, Data.DB, cxDBData, cxTextEdit, cxCheckBox,
  cxCalendar, cxDBLookupComboBox, cxDropDownEdit, cxCurrencyEdit, cxMemo,
  Vcl.ExtCtrls, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid, cxMaskEdit, cxLabel,
  RzButton, RzPanel, Vcl.Menus,ufrmBaseController, RzTabs, Datasnap.DBClient,
  cxImage,UnitADO, Vcl.StdCtrls;

type
  TfrmBaseSanction = class(TfrmBaseController)
    Print: TPopupMenu;
    Excel1: TMenuItem;
    N2: TMenuItem;
    RzPanel1: TRzPanel;
    RzToolbar3: TRzToolbar;
    RzSpacer13: TRzSpacer;
    Btn1: TRzToolButton;
    RzSpacer14: TRzSpacer;
    Btn2: TRzToolButton;
    RzSpacer15: TRzSpacer;
    Btn3: TRzToolButton;
    RzSpacer16: TRzSpacer;
    Btn4: TRzToolButton;
    Btn5: TRzToolButton;
    RzSpacer17: TRzSpacer;
    btnClose: TRzToolButton;
    RzSpacer18: TRzSpacer;
    RzSpacer19: TRzSpacer;
    RzSpacer31: TRzSpacer;
    RzSpacer32: TRzSpacer;
    Btn6: TRzToolButton;
    cxLabel17: TcxLabel;
    cxDateEdit1: TcxDateEdit;
    cxLabel37: TcxLabel;
    cxDateEdit2: TcxDateEdit;
    RzPanel2: TRzPanel;
    Splitter1: TSplitter;
    RzPageControl1: TRzPageControl;
    TabSheet1: TRzTabSheet;
    TabSheet2: TRzTabSheet;
    Grid: TcxGrid;
    tvPrize: TcxGridDBTableView;
    tvPrizeCol1: TcxGridDBColumn;
    tvPrizeCol2: TcxGridDBColumn;
    tvPrizeCol3: TcxGridDBColumn;
    tvPrizeCol4: TcxGridDBColumn;
    tvPrizeCol5: TcxGridDBColumn;
    tvPrizeCol6: TcxGridDBColumn;
    tvPrizeCol7: TcxGridDBColumn;
    tvPrizeCol9: TcxGridDBColumn;
    tvPrizeCol10: TcxGridDBColumn;
    tvPrizeCol11: TcxGridDBColumn;
    tvPrizeCol12: TcxGridDBColumn;
    tvPrizeCol13: TcxGridDBColumn;
    tvPrizeCol14: TcxGridDBColumn;
    tvPrizeCol15: TcxGridDBColumn;
    tvPrizeCol16: TcxGridDBColumn;
    tvPrizeCol17: TcxGridDBColumn;
    tvPrizeCol18: TcxGridDBColumn;
    tvPrizeCol19: TcxGridDBColumn;
    Lv: TcxGridLevel;
    cxGrid1: TcxGrid;
    tvBuckle: TcxGridDBTableView;
    tvBuckleCol1: TcxGridDBColumn;
    tvBuckleCol2: TcxGridDBColumn;
    tvBuckleCol3: TcxGridDBColumn;
    tvBuckleCol5: TcxGridDBColumn;
    tvBuckleCol6: TcxGridDBColumn;
    tvBuckleCol7: TcxGridDBColumn;
    tvBuckleCol8: TcxGridDBColumn;
    tvBuckleCol9: TcxGridDBColumn;
    tvBuckleCol10: TcxGridDBColumn;
    tvBuckleCol11: TcxGridDBColumn;
    tvBuckleCol12: TcxGridDBColumn;
    tvBuckleCol13: TcxGridDBColumn;
    tvBuckleCol14: TcxGridDBColumn;
    tvBuckleCol15: TcxGridDBColumn;
    tvBuckleCol16: TcxGridDBColumn;
    tvBuckleCol17: TcxGridDBColumn;
    tvBuckleCol18: TcxGridDBColumn;
    tvBuckleCol19: TcxGridDBColumn;
    tvBuckleCol20: TcxGridDBColumn;
    Lv1: TcxGridLevel;
    ClientPrize: TClientDataSet;
    DataPrize: TDataSource;
    ClientBuckle: TClientDataSet;
    DataBuckle: TDataSource;
    tvPrizeCol8: TcxGridDBColumn;
    RzSpacer1: TRzSpacer;
    RzToolButton1: TRzToolButton;
    pm1: TPopupMenu;
    N1: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Btn1Click(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ClientPrizeAfterInsert(DataSet: TDataSet);
    procedure tvPrizeCol1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure tvPrizeEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure tvPrizeStylesGetContentStyle(Sender: TcxCustomGridTableView;
      ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
      var AStyle: TcxStyle);
    procedure tvPrizeCol2PropertiesChange(Sender: TObject);
    procedure tvPrizeCol15PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure tvPrizeTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure tvBuckleColumnSizeChanged(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
    procedure tvPrizeColumnSizeChanged(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
    procedure ClientBuckleAfterInsert(DataSet: TDataSet);
    procedure tvBuckleCol16PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure tvBuckleTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure tvBuckleStylesGetContentStyle(Sender: TcxCustomGridTableView;
      ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
      var AStyle: TcxStyle);
    procedure tvBuckleCol2PropertiesChange(Sender: TObject);
    procedure tvBuckleEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure Btn6Click(Sender: TObject);
    procedure Excel1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure RzToolButton1Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure N5Click(Sender: TObject);
    procedure N7Click(Sender: TObject);
  private
    { Private declarations }
    dwCompanyPrize  : string;
    dwCompanybuckle : string;
    g_ProjectName : string;
    g_PostCode    : string;
    g_BuildStorage: string;

    dwCompanyParent : string;
    dwFullname : string;
    dwNumbers  : string;
    dwCompanyName : string;
    dwADO  : TADO;
    IsSave : Boolean;
    Prefix : string;
    Suffix : string;

  public
    { Public declarations }
    procedure WndProc(var Message: TMessage); override;  // 第一优先权
    procedure SetcxGrid(lpGridView : TcxGridDBTableView ; index : Byte ;lpSuffix:string);
  end;

var
  frmBaseSanction: TfrmBaseSanction;

implementation

uses
   ufrmCompanyFrame,ufunctions,global,uDataModule,ufrmIsViewGrid;

{$R *.dfm}

procedure TfrmBaseSanction.WndProc(var Message: TMessage);
var
  pMsg : pStaff;
  s : string;
  szColName : string;
  szCodeList : string;
  szTableList : array[0..1] of TTableList;
begin
  case Message.Msg of
    WM_FrameClose:
      begin

      end;
    WM_FrameView :
    begin
      pMsg := pStaff(Message.LParam);
      if Assigned(pMsg) then
      begin
        dwCompanyParent := pMsg.dwParent;
        dwFullname := pMsg.dwFullname;
        dwCompanyName   := pMsg.dwCompanyName;
        dwNumbers  := pMsg.dwNumbers;

        //sk_Company_buckle  扣罚
        //sk_Company_Prize   增奖
        szColName := Self.tvPrizeCol8.DataBinding.FieldName;
        dwCompanyPrize := 'Select * from ' + g_Table_Company_Prize + ' Where ' + szColName + '="' + dwFullname + '"';
        with DM.ADOQuery1 do
        begin
          Close;
          SQL.Text := dwCompanyPrize;
          Open;
          Self.ClientPrize.Data := DM.DataSetProvider1.Data;
          Self.ClientPrize.Open;
        end;

        dwCompanybuckle  := 'Select * from ' + g_Table_Company_buckle + ' Where ' + szColName + '="' + dwFullname + '"'; ;
        with DM.ADOQuery1 do
        begin
          Close;
          SQL.Text := dwCompanybuckle;
          Open;
          Self.ClientBuckle.Data := DM.DataSetProvider1.Data;
          Self.ClientBuckle.Open;
        end;

        Self.Btn1.Enabled := True;
        Self.Btn2.Enabled := True;
        Self.Btn3.Enabled := True;
        Self.Btn4.Enabled := True;
        Self.Btn5.Enabled := True;
        Self.Btn6.Enabled := True;
        Self.RzToolButton1.Enabled := True;
      end;

    end;
    WM_FrameDele :
    begin
      pMsg   := pStaff(Message.LParam);
      {
      szCodeList := pMsg.dwCodeList;
      szTableList[0].ATableName := g_Table_Project_BuildFlowingAccount;
      szTableList[0].ADirectory := g_Dir_Project_Finance;
      }
    //  DeleteTableFile(szTableList,szCodeList);
    //  DM.InDeleteData(g_Table_Project_BuildFlowingAccount,szCodeList); //清除主目录表内关联
    end;
  end;
  // 一定要加上这句，否则编译通不过。因为绝大部分消息没人处理了
  inherited WndProc(Message); // 会一路向上调用，直到TControl.WndProc调用Dispatch来寻找消息处理函数
end;

procedure TfrmBaseSanction.Btn6Click(Sender: TObject);
var
  szColName : string;
  sqltext : string;

begin
  inherited;
//奖励范围查询
  szColName   := Self.tvPrizeCol4.DataBinding.FieldName;
  sqltext := 'select *from ' + g_Table_Company_Prize +
             ' where '+ Self.tvPrizeCol8.DataBinding.FieldName +'= "' + dwFullname + '" and ' + szColName +' between :t1 and :t2';
  dwADO.ADOSearchDateRange(Self.cxDateEdit1.Text,Self.cxDateEdit2.Text,sqltext,Self.ClientPrize);

  //扣罚范围查询

  szColName   := Self.tvBuckleCol5.DataBinding.FieldName;
  sqltext := 'select *from ' + g_Table_Company_buckle +
             ' where '+ Self.tvBuckleCol9.DataBinding.FieldName +'= "' + dwFullname + ' " and ' + szColName +' between :t1 and :t2';

  dwADO.ADOSearchDateRange(Self.cxDateEdit1.Text,Self.cxDateEdit2.Text,sqltext,Self.ClientBuckle);

end;

procedure TfrmBaseSanction.btnCloseClick(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TfrmBaseSanction.ClientBuckleAfterInsert(DataSet: TDataSet);
var
  szColName : string;
  szCode : string;

begin
  inherited;
  with DataSet do
  begin
    FieldByName('code').Value    := dwNumbers ;      //员工编号
    FieldByName('Parent').Value  := dwCompanyParent; //公司编号

    szColName := Self.tvBuckleCol2.DataBinding.FieldName;
    FieldByName(szColName).Value := True;
    szColName := Self.tvBuckleCol9.DataBinding.FieldName;
    FieldByName(szColName).Value := dwFullname;

    FieldByName(Self.tvBuckleCol11.DataBinding.FieldName).Value := dwCompanyName;

    szColName := Self.tvBuckleCol5.DataBinding.FieldName;
    FieldByName(szColName).Value := Date;
    Prefix := 'BU';
    Suffix := '2000000';
    szColName := Self.tvBuckleCol3.DataBinding.FieldName; //编号
    szCode := Prefix + GetRowCode(g_Table_Company_buckle,szColName,Suffix,2000000);
    FieldByName(szColName).Value := szCode;
  end;
end;

procedure TfrmBaseSanction.ClientPrizeAfterInsert(DataSet: TDataSet);
var
  szColName : string;
  szCode : string;

begin
  inherited;
  with DataSet do
  begin
    FieldByName('code').Value    := dwNumbers;        //员工编号
    FieldByName('Parent').Value  := dwCompanyParent;  //公司编号

    szColName := Self.tvPrizeCol2.DataBinding.FieldName;
    FieldByName(szColName).Value := True;
    szColName := Self.tvPrizeCol8.DataBinding.FieldName;
    FieldByName(szColName).Value := dwFullname;
    FieldByName(Self.tvPrizeCol10.DataBinding.FieldName).Value := dwCompanyName;

    szColName := Self.tvPrizeCol4.DataBinding.FieldName;
    FieldByName(szColName).Value := Date;
    Prefix := 'PE';
    Suffix := '1000000';
    szColName := Self.tvPrizeCol3.DataBinding.FieldName; //编号
    szCode := Prefix + GetRowCode(g_Table_Company_Prize,szColName,Suffix,1000000);
    FieldByName(szColName).Value := szCode;

  end;
end;

procedure TfrmBaseSanction.Excel1Click(Sender: TObject);
begin
  inherited;
  case Self.RzPageControl1.ActivePageIndex of
    0: CxGridToExcel(Self.Grid,'增奖明细表');
    1: CxGridToExcel(Self.cxGrid1,'扣罚明细表');
  end;
end;

procedure TfrmBaseSanction.N1Click(Sender: TObject);
begin
  inherited;
  Btn1.Click;
end;

procedure TfrmBaseSanction.N2Click(Sender: TObject);
begin
  inherited;
  case Self.RzPageControl1.ActivePageIndex of
    0: DM.BasePrinterLink1.Component := Self.Grid;
    1: DM.BasePrinterLink1.Component := Self.cxGrid1;
  end;
  DM.BasePrinterLink1.ReportTitleText := '公司名称：' + dwCompanyName;
  DM.BasePrinter.Preview(True, nil);
end;

procedure TfrmBaseSanction.N3Click(Sender: TObject);
begin
  inherited;
  RzToolButton1.Click;
end;

procedure TfrmBaseSanction.N4Click(Sender: TObject);
begin
  inherited;
  Btn2.Click;
end;

procedure TfrmBaseSanction.N5Click(Sender: TObject);
begin
  inherited;
  Btn3.Click;
end;

procedure TfrmBaseSanction.N7Click(Sender: TObject);
var
  szDir : string;
begin
  inherited;
  case Self.RzPageControl1.ActivePageIndex of
    0:
    begin
      szDir := Concat(g_Resources, '\' + g_Dir_Company_Prize);
      CreateEnclosure(Self.tvPrize.DataController.DataSource.DataSet,Handle,szDir);
    end;
    1:
    begin
      szDir := Concat(g_Resources, '\' + g_Dir_Company_Buckle);
      CreateEnclosure(Self.tvBuckle.DataController.DataSource.DataSet,Handle,szDir);
    end;
  end;

end;

procedure TfrmBaseSanction.RzToolButton1Click(Sender: TObject);
begin
  inherited;
  //编辑
  case Self.RzPageControl1.ActivePageIndex of
    0:
    begin
      if Self.tvPrizeCol2.EditValue then
      begin
        with Self.ClientPrize do
        begin
          if State <> dsInactive then
          begin
            if (State <> dsEdit) and (State <> dsInsert)  then  Edit;
          end;
        end;
      end else
      begin
        Application.MessageBox( '冻结状态下不可以编辑！', '提示:', MB_OK + MB_ICONWARNING)
      end;
    end;
    1:
    begin
      if Self.tvBuckleCol2.EditValue then
      begin
        with Self.ClientBuckle do
        begin
          if State <> dsInactive then
          begin
            if (State <> dsEdit) AND (State <> dsInsert) then Edit;
          end;
        end;
      end else
      begin
        Application.MessageBox( '冻结状态下不可以编辑！', '提示:', MB_OK + MB_ICONWARNING)
      end;
    end;
  end;

end;

procedure TfrmBaseSanction.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmBaseSanction.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  inherited;
  if not dwADO.ADOExitIsSaveData(Handle,Self.ClientPrize,dwCompanyPrize) then
  begin
    if MessageBox(Handle, PChar('增奖数据未保存成功，现在是否要退出?'),'提示',
         MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = IDNO then
    begin
      CanClose := False;
      Exit;
    end;
  end;
  {
  if not dwADO.ADOExitIsSaveData(Handle,Self.ClientPrize,dwCompanyPrize) then
  begin
    if MessageBox(Handle, PChar('增奖数据未保存成功，现在是否要退出?'),'提示',
         MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = IDNO then
    begin
      CanClose := False;
      Exit;
    end;
  end;

  }
end;

procedure TfrmBaseSanction.FormCreate(Sender: TObject);
var
  ChildFrame : TfrmCompanyFrame;
begin
  inherited;
  Self.RzPageControl1.ActivePage := Self.TabSheet1;
  ChildFrame := TfrmCompanyFrame.Create(Self);
  ChildFrame.dwParentHandle := Handle;
  ChildFrame.Parent := Self.RzPanel2;
//  ChildFrame.IsSystem := True;
  ChildFrame.FrameClick(Self);

  Self.cxDateEdit1.Date := Date;
  Self.cxDateEdit2.Date := Date;

  SetcxGrid(Self.tvPrize,0,g_Dir_Company_Prize);
  SetcxGrid(Self.tvBuckle,0,g_Dir_Company_buckle);
end;

procedure TfrmBaseSanction.tvBuckleCol16PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  s : string;
  szColName : string;

begin
  inherited;
  if DisplayValue <> null then
  begin
    s := VarToStr(DisplayValue);
    with Self.ClientBuckle do
    begin
      if State <> dsInactive then
      begin
        szColName := Self.tvBuckleCol17.DataBinding.FieldName;
        FieldByName(szColName).Value := MoneyConvert(StrToCurrDef(s,0));
        szColName := Self.tvBuckleCol16.DataBinding.FieldName;
        FieldByName(szColName).Value := DisplayValue;
      end;

    end;

  end;

end;

procedure TfrmBaseSanction.tvBuckleCol2PropertiesChange(Sender: TObject);
begin
  inherited;
  if Self.ClientBuckle.State  =  dsEdit then Self.ClientBuckle.Post;
end;

procedure TfrmBaseSanction.tvBuckleColumnSizeChanged(Sender: TcxGridTableView;
  AColumn: TcxGridColumn);
begin
  inherited;
  SetcxGrid(Self.tvBuckle,1,g_Dir_Company_buckle);
end;

procedure TfrmBaseSanction.tvBuckleEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
var
  szColName : string;
  Value : Variant;

begin
  inherited;
  with Self.ClientBuckle do
  begin
    if State <> dsInactive then
    begin
      if (State <> dsEdit) AND (State <> dsInsert) then
        AAllow := False
      else
        AAllow := True;
    end;
  end;
  {
  szColName := Self.tvBuckleCol2.DataBinding.FieldName;
  if AItem.Index <> Self.tvBuckleCol2.Index then
  begin

    with Self.ClientBuckle do
    begin
      if State <> dsInactive then
        begin
          if FieldByName(szColName).Value = False then
          begin
            AAllow := False;
          end else
          begin
            AAllow := True;
          end;

        end;
    end;

  end;
  }
  if AItem.Index = Self.tvBuckleCol1.Index then AAllow := False;
end;

procedure TfrmBaseSanction.tvBuckleStylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
var
  szIndex : Integer;
  Value : Variant;
begin
  inherited;
  Value := ARecord.Values[Self.tvBuckleCol2.Index];
  if Value = False then
  begin
    AStyle := DM.cxStyle222;
  end;
end;

procedure TfrmBaseSanction.tvBuckleTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
var
  s : string;
begin
  inherited;
  if AValue <> null then
  begin
    s := VarToStr(AValue);
    Self.tvBuckleCol17.Summary.FooterFormat := MoneyConvert(StrToCurrDef(s,0));
  end;
end;

procedure TfrmBaseSanction.tvPrizeCol15PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  s : string;
  szColName : string;

begin
  inherited;
  if DisplayValue <> null then
  begin
    s := VarToStr(DisplayValue);
    with Self.ClientPrize do
    begin
      if State <> dsInactive then
      begin
        szColName := Self.tvPrizeCol16.DataBinding.FieldName;
        FieldByName(szColName).Value := MoneyConvert(StrToCurrDef(s,0));
        szColName := Self.tvPrizeCol15.DataBinding.FieldName;
        FieldByName(szColName).Value := DisplayValue;
      end;
    end;
  end;
end;

procedure TfrmBaseSanction.tvPrizeCol1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  inherited;
  AText := IntToStr(ARecord.Index + 1);
end;

procedure TfrmBaseSanction.tvPrizeCol2PropertiesChange(Sender: TObject);
begin
  inherited;
  if Self.ClientPrize.State  =  dsEdit then Self.ClientPrize.Post;
end;

procedure TfrmBaseSanction.tvPrizeColumnSizeChanged(Sender: TcxGridTableView;
  AColumn: TcxGridColumn);
begin
  inherited;
  SetcxGrid(Self.tvPrize,1,g_Dir_Company_Prize);
end;

procedure TfrmBaseSanction.tvPrizeEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
var
  szColName : string;
  Value : Variant;

begin
  inherited;
  with Self.ClientPrize do
  begin
    if State <> dsInactive then
    begin
      if (State <> dsEdit) AND (State <> dsInsert) then
        AAllow := False
      else
        AAllow := True;
    end;
  end;

  {
  szColName := Self.tvPrizeCol2.DataBinding.FieldName;
  if AItem.Index <> Self.tvPrizeCol2.Index then
  begin
  end;
  }
  if AItem.Index = Self.tvPrizeCol1.Index then
  begin
    AAllow := False;
  end;
end;

procedure TfrmBaseSanction.tvPrizeStylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
var
  szIndex : Integer;
  Value : Variant;
begin
  inherited;
  Value := ARecord.Values[Self.tvPrizeCol2.Index];
  if Value = False then
  begin
    AStyle := DM.cxStyle222;
  end;

end;

procedure TfrmBaseSanction.tvPrizeTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
var
  s : string;

begin
  inherited;
  if AValue <> null then
  begin
    s := VarToStr(AValue);
    Self.tvPrizeCol16.Summary.FooterFormat := MoneyConvert(StrToCurrDef(s,0));
  end;
end;

procedure TfrmBaseSanction.SetcxGrid(lpGridView : TcxGridDBTableView ; index : Byte ;
lpSuffix:string);
var
  i : Integer;
begin
  for I := 0 to lpGridView.ColumnCount - 1 do
  begin
    Application.ProcessMessages;
    SetGrid(lpGridView.Columns[i],index, Self.Name + lpSuffix);
  end;
end;

procedure TfrmBaseSanction.Btn1Click(Sender: TObject);
var
  ErrorCount : Integer;
  Child : TfrmIsViewGrid;
  i : Integer;

begin
  inherited;

  case self.RzPageControl1.ActivePageIndex of
   0:
    begin
      //增奖管理
      if Sender = Self.Btn1 then
      begin
        //新增
        dwADO.ADOAppend(Self.ClientPrize);
      end else
      if Sender = Self.Btn2 then
      begin
        //保存
        dwADO.ADOSaveData(Self.ClientPrize,dwCompanyPrize);
      end else
      if Sender = Self.Btn3 then
      begin
        //删除
        dwADO.ADODeleteSelection(Self.tvPrize,
                                 Concat(g_Resources,'\' + g_Dir_Company_Prize),
                                 g_Table_Company_Prize,
                                 Self.tvPrizeCol3.DataBinding.FieldName);
      end else
      if Sender = Self.Btn4 then
      begin
        //表格
        Child := TfrmIsViewGrid.Create(Application);
        try
          Child.g_fromName := Self.Name + g_Dir_Company_Prize;
          Child.ShowModal;
          SetcxGrid(Self.tvPrize,0,g_Dir_Company_Prize);
        finally
          Child.Free;
        end;

      end;
    end;
   1:
    begin
      //扣罚管理
      if Sender = Self.Btn1 then
      begin
        //新增
        dwADO.ADOAppend(Self.ClientBuckle);
      end else
      if Sender = Self.Btn2 then
      begin
        //保存
        dwADO.ADOSaveData(Self.ClientBuckle,dwCompanybuckle);
      end else
      if Sender = Self.Btn3 then
      begin
        //删除
        dwADO.ADODeleteSelection(Self.tvBuckle,
                                 Concat(g_Resources,'\' + g_Dir_Company_Prize),
                                 g_Table_Company_Prize,
                                 Self.tvPrizeCol3.DataBinding.FieldName);
      end else
      if Sender = Self.Btn4 then
      begin
        //表格

        Child := TfrmIsViewGrid.Create(Application);
        try
          Child.g_fromName := Self.Name + g_Dir_Company_buckle;
          Child.ShowModal;
          SetcxGrid(Self.tvBuckle,0,g_Dir_Company_buckle);
        finally
          Child.Free;
        end;

      end;

    end;

  end;

end;

end.
