inherited frmCompanyStorageEntry: TfrmCompanyStorageEntry
  Caption = #20844#21496#20179#24211' ['#20837#24211']'
  ClientHeight = 611
  ClientWidth = 1077
  ExplicitWidth = 1093
  ExplicitHeight = 649
  PixelsPerInch = 96
  TextHeight = 13
  inherited Splitter1: TSplitter
    Left = 249
    Height = 611
    ExplicitLeft = 249
    ExplicitHeight = 611
  end
  inherited RzPanel1: TRzPanel
    Left = 259
    Width = 818
    Height = 611
    ExplicitLeft = 259
    ExplicitWidth = 818
    ExplicitHeight = 611
    inherited RzToolbar13: TRzToolbar
      Width = 816
      ExplicitWidth = 816
      ToolbarControls = (
        RzSpacer30
        RzToolButton90
        RzSpacer120
        RzToolButton85
        RzSpacer113
        RzToolButton87
        RzToolButton88)
      inherited RzToolButton88: TRzToolButton
        Left = 214
        ExplicitLeft = 214
      end
    end
    inherited Grid: TcxGrid
      Width = 816
      Height = 582
      ExplicitWidth = 816
      ExplicitHeight = 582
      inherited tvView: TcxGridDBTableView
        DataController.Summary.DefaultGroupSummaryItems = <
          item
            Format = #21512#35745
            Position = spFooter
            Column = tvViewColumn1
          end
          item
            Format = #25968#20540':0.########;-0.########'
            Kind = skSum
            Column = tvViewColumn6
          end
          item
            Format = #25968#20540':0.########;-0.########'
            Kind = skSum
            Position = spFooter
            Column = tvViewColumn6
          end
          item
            Format = #25968#37327':0.###;-0.###'
            Kind = skSum
            Column = tvViewColumn9
          end
          item
            Format = #25968#37327':0.###;-0.###'
            Kind = skSum
            Position = spFooter
            Column = tvViewColumn9
          end
          item
            Format = #37329#39069':'#165',0.00;'#165'-,0.00'
            Kind = skSum
            Position = spFooter
            Column = tvViewColumn14
          end
          item
            Format = #37329#39069':'#165',0.00;'#165'-,0.00'
            Kind = skSum
            Column = tvViewColumn14
          end>
        DataController.Summary.FooterSummaryItems = <
          item
            Format = #37329#39069':'#165',0.00;'#165'-,0.00'
            Kind = skSum
            Column = tvViewColumn14
          end
          item
            Format = #21512#35745#65306
            Kind = skCount
            Column = tvViewColumn1
            Sorted = True
          end
          item
            Format = #25968#20540':0.########;-0.########'
            Kind = skSum
            Column = tvViewColumn6
          end
          item
            Format = #25968#37327':0.###;-0.###'
            Kind = skSum
            Column = tvViewColumn9
          end
          item
            Format = #165',0.00;'#165'-,0.00'
            Kind = skSum
            Column = tvViewColumn14
          end
          item
            Format = #22823#20889':'
            Kind = skCount
          end>
      end
    end
    inherited RzPanel4: TRzPanel
      Left = 13
      Top = 236
      ExplicitLeft = 13
      ExplicitTop = 236
    end
  end
  inherited RzPanel2: TRzPanel
    Width = 249
    Height = 611
    ExplicitWidth = 249
    ExplicitHeight = 611
    object tv: TTreeView
      Left = 1
      Top = 29
      Width = 247
      Height = 582
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Images = DM.TreeImageList
      Indent = 19
      ParentFont = False
      RowSelect = True
      TabOrder = 0
      Items.NodeData = {
        03010000002A0000000000000000000000FFFFFFFFFFFFFFFF00000000000000
        00010000000106E55D0B7A7998EE76A17B06742E0000000000000000000000FF
        FFFFFFFFFFFFFF0000000000000000000000000108B970FB510B4EB9657998EE
        76E55D0B7A}
    end
    object RzToolbar5: TRzToolbar
      Left = 1
      Top = 0
      Width = 247
      Height = 29
      AutoStyle = False
      Images = DM.cxImageList1
      BorderInner = fsFlat
      BorderOuter = fsNone
      BorderSides = [sdBottom]
      BorderWidth = 0
      GradientColorStyle = gcsCustom
      TabOrder = 1
      VisualStyle = vsGradient
      ToolbarControls = (
        RzSpacer27
        RzBut1
        RzSpacer28
        RzBut2
        RzSpacer29
        RzBut3
        RzToolButton1)
      object RzSpacer27: TRzSpacer
        Left = 4
        Top = 2
      end
      object RzBut1: TRzToolButton
        Left = 12
        Top = 2
        Width = 53
        ImageIndex = 37
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #26032#22686
      end
      object RzSpacer28: TRzSpacer
        Left = 65
        Top = 2
      end
      object RzBut2: TRzToolButton
        Left = 73
        Top = 2
        Width = 50
        ImageIndex = 0
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #32534#36753
      end
      object RzSpacer29: TRzSpacer
        Left = 123
        Top = 2
      end
      object RzBut3: TRzToolButton
        Left = 131
        Top = 2
        Width = 50
        ImageIndex = 2
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #21024#38500
      end
      object RzToolButton1: TRzToolButton
        Left = 181
        Top = 2
        Width = 50
        ImageIndex = 11
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #21047#26032
      end
    end
  end
  inherited frxStorageReport: TfrxDBDataset
    Left = 333
    Top = 157
  end
end
