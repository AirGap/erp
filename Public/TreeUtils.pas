unit TreeUtils;

interface
uses
  Windows, Messages, SysUtils, Variants, Classes, ComCtrls, DB, ADODB,
  TreeFillThrd;

type
  TTreeUtils = class
    TreeView: TTreeView;
    NData: TNodeData;
    Conn: TADOConnection;
    Query: TADOQuery;
    constructor Create(ATree: TTreeView;
                       AConn: TADOConnection;
                       ATableName    : string;
                       AFieldIndex   : string = 'parent';
                       AFieldPID     : string = 'PID';
                       AFieldCaption : string = 'Caption';
                       AFieldCode    : string = 'Code';
                       AFieldmoney   : string = 'sum_money';
                       AFieldphoto   : string = 'photo';
                       AFieldRemarks : string = 'Remarks';
                       AFieldPrice   : string = 'Price';
                       AfieldNum     : string = 'Num';
                       AfieldInput_time : string = 'Input_time';
                       AfieldEnd_time   : string = 'End_time';
                       AfieldPatType    : string = 'PatType';
                       ARootText: string = '节点管理');

    destructor Destroy;
  private
    TableName   : string;
    FieldIndex  : string;
    FieldPID    : string;
    FieldCaption: string;
    FieldCode   : string;
    FieldRemarks: string;
    Fieldmoney  : string;
    Fieldphoto  : string;
    FieldPrice  : string;
    FieldNum    : string;
    FieldInput_time : string;
    FieldEnd_time   : string;
    FieldPatType    : string;
    RootText  : string;
    sqlStr, errStr: string;
    function getMaxIndex(ATableName : string): integer;
    function execSQL(sqlStr: string; var errMsg: string): boolean;
  public
    //按照节点在数据库中的编号查找节点
    function FindTreeNode(Tree: TTreeView; Index: integer): TTreeNode;
    //删除子树
    function DeleteTree(Node: TTreeNode): Boolean;
    //查找节点在数据库中的数据
    function getTreeNodeData(TreeNode: TTreeNode): TNodeData;
    function getSelNodeIndex: integer;
    function getSelNodeCaption: string;
    function getNodeData(): PNodeData;
    function getTreeData():TTreeNode;

    //添加节点
    function AddNode(ACode , ARemarks :string ;AMoney,APrice : Currency;ANum:Integer;APhoto:string; ADatatime ,EndTime : TDateTime;APatType : Integer; ACaption: string = '新节点'; Index: integer = -1; ANode: TTreeNode = nil): Boolean;
    function AddChildNode(ACode , ARemarks : string ;AMoney,APrice : Currency;ANum:Integer;APhoto:string;ADatatime , EndTime: TDateTime;APatType : Integer;  ACaption: string = '新节点'; Index: integer = -1; ANode: TTreeNode = nil): Boolean;
    //修改节点
   //function ModifyNodeData(NewCaption , NewCode ,AMoney : Currency;APhone:string; NewRemarks : string; Node: TTreeNode): boolean;
    function ModifyNodeData(NewCaption , NewCode:string;AMoney,APrice : Currency; ANum: Integer;APhoto:string; NewRemarks : string;ADatatime,EndTime : TDateTime; Node:TTreeNode): boolean;

    function ModifyNodeCaption(NewCaption : string;Node: TTreeNode): boolean;
    function ModifyNodePID(PID: integer; Node: TTreeNode): boolean;
    //填充节点
    procedure FillTree(AParent , APatType : Integer);
  end;

implementation
//--------------------------------------------------------------
constructor TTreeUtils.Create(ATree: TTreeView; AConn: TADOConnection; ATableName: string;
                              AFieldIndex   : string = 'parent';
                              AFieldPID     : string = 'PID';
                              AFieldCaption : string = 'Caption';
                              AFieldCode    : string = 'Code';
                              AFieldmoney   : string = 'sum_money';
                              AFieldphoto   : string = 'photo';
                              AFieldRemarks : string = 'Remarks';
                              AFieldPrice   : string = 'Price';
                              AfieldNum     : string = 'Num';
                              AfieldInput_time : string = 'Input_time';
                              AfieldEnd_time   : string = 'End_time';
                              AfieldPatType    : string = 'PatType';
                              ARootText: string = '节点管理'
                              );
begin
  TreeView := ATree;
  Conn := AConn;
  TableName    := ATableName;
  FieldIndex   := AFieldIndex;
  FieldPID     := AFieldPID;
  FieldCode    := AFieldCode;
  Fieldmoney   := AFieldmoney;
  Fieldphoto   := AFieldphoto;
  FieldRemarks := AFieldRemarks;
  FieldCaption := AFieldCaption;
  FieldPrice   := AFieldPrice;
  FieldNum     := AfieldNum;
  FieldInput_time := AfieldInput_time;
  FieldEnd_time:= AfieldEnd_time;
  FieldPatType := AfieldPatType;
  RootText     := ARootText;
  Query        := TADOQuery.Create(nil);
  Query.Connection := Conn;
end;
//--------------------------------------------------------------
destructor TTreeUtils.Destroy;
begin
  Query.Free;
  inherited Destroy;
end;
//--------------------------------------------------------------
function TTreeUtils.FindTreeNode(Tree: TTreeView; Index: integer): TTreeNode;
var i: integer;
begin
  Result := nil;
  for i := 0 to Tree.Items.Count - 1 do
    if PNodeData(Tree.Items.Item[i].Data)^.Index = Index then
      Result := Tree.Items.Item[i];
end;
//--------------------------------------------------------------
function TTreeUtils.DeleteTree(Node: TTreeNode): Boolean;
  function DelTableFromIndex(index: integer): boolean;
  var
    sqlStr: string;
    errSTr: string;
  begin
    Result := False;
    SQLStr := 'Delete from ' + TableName + ' where ' + FieldIndex + ' = ' + IntToStr(index);
    if execSQL(sqlStr, errStr) then Result := True;
  end;
  //------------------------------------------------------------
  function DelTreeFromDB(ParentID: integer): Boolean;
  var
     Query: TADOQuery;
  begin
    Query := nil;
    Result := False;
    try
      Query := TADOQuery.Create(nil);
      Query.Connection := Conn;
      Query.SQL.Text := 'select * from ' + TableName + ' where PID = ' + IntToStr(ParentID);
      if Query.Active then Query.Close;
      Query.Open;
      while Query.Eof = False do
      begin
        DelTreeFromDB(Query.FieldValues[FieldIndex]);
        Result := DelTableFromIndex(Query.FieldByName(FieldIndex).AsInteger);
        Query.Next;
      end;
    finally
      Query.Free;
    end;
    Result := DelTableFromIndex(ParentID);
  end;
  //------------------------------------------------------------
begin
  Result := False;
  if Node.AbsoluteIndex = 0 then
  begin
    raise Exception.Create('禁止删除根节点！');
    Exit;
  end;

  if DelTreeFromDB(PNodeData(Node.Data)^.Index) then
  begin
    TreeView.Items.Delete(Node);
    Result := True;
  end;
end;
//--------------------------------------------------------------
function TTreeUtils.getTreeNodeData(TreeNode: TTreeNode): TNodeData;
begin
  if TreeNode = nil then
    Result := PNodeData(TreeView.Items.Item[0].Data)^
  else
    Result := PNodeData(TreeNode.Data)^;
end;

function TTreeUtils.getNodeData(): PNodeData;
var
  Node : TTreeNode;
  Data : Pointer;
begin
  Result := nil;

  Node := TreeView.Selected;
  if Assigned( Node ) then
  begin
    Result  := PNodeData( Node.Data );
    OutputDebugString('选中节点了');
  end
  else
  begin
     OutputDebugString('未选中节点');
  end;

end;

function TTreeUtils.getTreeData():TTreeNode;
begin
  result := TreeView.Selected;
end;
//--------------------------------------------------------------
function TTreeUtils.getSelNodeIndex: integer;
var
  PData: PNodeData;

begin
  Result := -1;
  if TreeView.Selected = nil then Exit;
  PData := PNodeData(TreeView.Selected.Data);
  Result := PData^.Index;
end;

function TTreeUtils.getSelNodeCaption: string;
var
  PData: PNodeData;

begin
  Result := '';
  if TreeView.Selected = nil then Exit;
  PData := PNodeData(TreeView.Selected.Data);
  Result := PData^.Caption;
end;
//--------------------------------------------------------------
function TTreeUtils.AddNode(ACode , ARemarks : string;AMoney,APrice : Currency;ANum:Integer; APhoto:string;ADatatime,EndTime : TDateTime;APatType : Integer; ACaption: string = '新节点'; Index: integer = -1;ANode: TTreeNode = nil): Boolean;
var
  Node, NewNode: TTreeNode;
  PData: PNodeData;

begin
//    Fieldmoney  : string;
//    Fieldphone  : string;
  Result := True;
  try
    if ANode = nil then Node := TreeView.Selected
    else Node := ANode;
    New(PData);
    PData^.Index   := getMaxIndex(TableName) + 1;
    PData^.Caption := ACaption;
    PData^.Code    := ACode;
    PData^.photo     := APhoto;
    PData^.sum_Money := AMoney;
    PData^.remarks   := ARemarks;
    PData^.Price     := APrice;
    PData^.Num       := ANum;
    PData^.Input_time:= ADatatime;
    PData^.End_time  := EndTime;
    PData^.PatType   := APatType;
    
    sqlStr := 'Insert into ' + TableName + ' (' + FieldIndex   + ',' +
                                                  FieldCode    + ',' +
                                                  Fieldmoney   + ',' +
                                                  Fieldphoto   + ',' +
                                                  FieldRemarks + ',' +
                                                  FieldPrice   + ',' +
                                                  FieldNum     + ',' +
                                                  FieldInput_time + ',' +
                                                  FieldEnd_time+ ',' +
                                                  FieldPatType + ',' +
                                                  FieldPID     + ',' +
                                                  FieldCaption + 

                                            ')' + ' values(' +
                                                 IntToStr(PData^.Index) + ',"' +
                                                 ACode    + '",' +
                                                 FloatToStr(AMoney)+ ',"' +
                                                 APhoto   + '","' +
                                                 ARemarks +  '",' +
                                                 FloatToStr(APrice) + ',' +
                                                 IntToStr(ANum)+',"' +
                                                 FormatDateTime('yyyy-MM-dd HH:mm:ss',ADatatime) +'","' +
                                                 FormatDateTime('yyyy-MM-dd HH:mm:ss',EndTime) +'",' +
                                                 IntToStr( APatType ) + ',' ;



    if Node = nil then Node := TreeView.Items.Item[0];
    if Node.Level = 0 then
      sqlStr := sqlStr + '0,"' + ACaption + '")'
    else
      sqlStr := sqlStr + IntToStr(PNodeData(Node.Parent.Data)^.Index) + ',"' + ACaption + '")';

    if execSQL(sqlStr, errStr) then
    begin
      if Node.Level = 0 then
        NewNode := TreeView.Items.AddChildObject(Node, ACaption, PData)
      else NewNode := TreeView.Items.AddObject(Node, ACaption, PData);
      NewNode.ImageIndex := 1;
      NewNode.SelectedIndex := 2;
      TreeView.Selected.Expanded := True;
    end;

  except
    Result := False;
  end;
  
end;
//--------------------------------------------------------------
function TTreeUtils.AddChildNode(ACode , ARemarks:string;
  AMoney,APrice : Currency;ANum:Integer;APhoto:string;
  ADatatime ,EndTime: TDateTime;APatType : Integer; ACaption: string = '新节点';
  Index: integer = -1; ANode: TTreeNode = nil): Boolean;
var
  Node, NewNode: TTreeNode;
  PData: PNodeData;

begin
  Result := True;
  try
    if ANode = nil then Node := TreeView.Selected
    else Node := ANode;
    New(PData);
    PData^.Index     := getMaxIndex(TableName) + 1;
    PData^.Caption   := ACaption;
    PData^.Code      := ACode;
    PData^.photo     := APhoto;
    PData^.sum_Money := AMoney;
    PData^.remarks   := ARemarks;
    PData^.Price     := APrice;
    PData^.Num       := ANum;
    PData^.Input_time:= ADatatime;
    PData^.End_time  := EndTime;
    PData^.PatType   := APatType;
    sqlStr := 'Insert into ' + TableName + ' (' + FieldIndex   + ',' +
                                                  FieldCode    + ',' +
                                                  Fieldmoney   + ',' +
                                                  Fieldphoto   + ',' +
                                                  FieldRemarks + ',' +
                                                  FieldPrice   + ',' +
                                                  FieldNum     + ',' +
                                                  FieldInput_time + ','+
                                                  FieldEnd_time+ ',' +
                                                  FieldPatType + ',' +
                                                  FieldPID     + ',' +
                                                  FieldCaption +
                                             ')' + ' values(' +
                                                  IntToStr(PData^.Index) + ',"' +
                                                  ACode    + '",' +
                                                  FloatToStr(AMoney)+ ',"' +
                                                  APhoto   + '","' +
                                                  ARemarks + '",' +
                                                  FloatToStr(APrice) + ',' +
                                                  IntToStr(ANum) + ',"' +
                                                  FormatDateTime('yyyy-MM-dd',ADatatime) +'","'+
                                                  FormatDateTime('yyyy-MM-dd',EndTime) +'",' +
                                                  Inttostr( APatType ) + ',';


    
    if Node = nil then Node := TreeView.Items.Item[0];
    if Node.Level = 0 then
      sqlStr := sqlStr + '0,"' + ACaption + '")'
    else
      sqlStr := sqlStr + IntToStr(PNodeData(Node.Data)^.Index) + ',"' + ACaption + '")';

    if execSQL(sqlStr, errStr) then
    begin
      NewNode := TreeView.Items.AddChildObject(Node, ACaption, PData);
      NewNode.ImageIndex := 1;
      NewNode.SelectedIndex := 2;
      TreeView.Selected.Expanded := True;
    end;

  except
    Result := False;
  end;
end;
//--------------------------------------------------------------
//执行SQL语言
function TTreeUtils.execSQL(sqlStr: string; var errMsg: string): boolean;
begin
  result := false;
  errMsg := '执行成功！';
  try
    Query.SQL.Text := sqlStr;
    if Query.Active then Query.Close;
    Query.ExecSQL;
    result := true;
  except
    on e: Exception do
      errMsg := e.Message;
  end;
end;
//------------------------------------------------------------------------------
function TTreeUtils.getMaxIndex(ATableName : string): integer;
var
  i: integer;
  //PNode: PNodeData;
  errMsg : string;
  s : string;
  Query : TADOQuery;
  
begin
  Result := 0;
  Query := TADOQuery.Create(nil);
  try
    Query.Connection := Conn;
    s := 'SELECT MAX(parent) FROM ' + ATableName;
    with Query do
    begin
      Close;
      SQL.Clear;
      SQL.Text := s;
      Open;
      if RecordCount <> 0 then
      begin
         //Result := fieldvalues['expr1000'] + 1;  //+ 21000 +1
         Result := FieldByName('expr1000').AsInteger;
         Inc(Result);
      end else
      begin
        Result := 1;
      end;

    end;
    
  finally
    Query.Free;
  end;
  
  {
  for i := 0 to TreeView.Items.Count - 1 do
  begin
    PNode := PNodeData(TreeView.Items.Item[i].Data);
    if Result < PNode^.Index then
      Result := PNode^.Index;
  end;

  }
end;
//------------------------------------------------------------------------------
procedure TTreeUtils.FillTree(AParent , APatType : Integer);
var
  H: TTreeFill;
begin
  H := TTreeFill.Create(TreeView, Conn,
                                  TableName,
                                  RootText,
                                  FieldIndex,
                                  FieldPID,
                                  FieldCaption,
                                  FieldCode,
                                  Fieldmoney,
                                  Fieldphoto,
                                  FieldRemarks,
                                  FieldPrice,
                                  fieldNum,
                                  fieldInput_time,
                                  fieldEnd_time,
                                  AParent,
                                  APatType
                                  );

end;
//------------------------------------------------------------------------------
//修改数据
function TTreeUtils.ModifyNodeData(NewCaption , NewCode:string;AMoney,APrice : Currency; ANum: Integer; APhoto:string; NewRemarks : string;ADatatime,EndTime : TDateTime; Node:TTreeNode): boolean;
begin
  Result := False;
  sqlStr := 'Update ' + TableName + ' set ' + FieldCaption + '="' + NewCaption + '",' +
                                              FieldCode    + '="' + newCode    + '",' +
                                              Fieldmoney   + '='  + FloatToStr(AMoney) + ','+
                                              Fieldphoto   + '="' + APhoto     + '",'  +
                                              FieldPrice   + '='  + FloatToStr(APrice) + ','+
                                              FieldNum     + '='  + IntToStr(ANum) + ',' +
                                              FieldInput_time + '="' + FormatDateTime('yyyy-MM-dd',ADatatime) + '",'+
                                              FieldEnd_time+ '="' + FormatDateTime('yyyy-MM-dd',EndTime) + '",' +
                                              FieldRemarks + '="' + NewRemarks + '" where ' +
                                              FieldIndex   + ' = ' + IntToStr( PNodeData(Node.Data)^.Index );

  if execSQL(sqlStr, errStr) then
  begin
    Node.Text := NewCaption;
    PNodeData(Node.Data)^.Caption   := NewCaption;
    PNodeData(Node.Data)^.Code      := NewCode;
    PNodeData(Node.Data)^.sum_Money := AMoney;
    PNodeData(Node.Data)^.photo     := APhoto;
    PNodeData(Node.Data)^.remarks   := NewRemarks;
    PNodeData(Node.Data)^.Price     := APrice;
    PNodeData(Node.Data)^.Num       := ANum;
    PNodeData(Node.Data)^.Input_time:= ADatatime;
    PNodeData(Node.Data)^.End_time  := EndTime;
    Result := True;
  end;

end;
//修改名字
function TTreeUtils.ModifyNodeCaption(NewCaption : string;Node: TTreeNode): boolean;
begin
  sqlStr := 'Update ' + TableName + ' set ' + FieldCaption + '="' + NewCaption + '" where '
    + FieldIndex + ' = ' + IntToStr(PNodeData(Node.Data)^.Index);
  if execSQL(sqlStr, errStr) then
  begin
    Node.Text := NewCaption;
    PNodeData(Node.Data)^.Caption := NewCaption;
  end;
  
end;
//------------------------------------------------------------------------------
function TTreeUtils.ModifyNodePID(PID: integer; Node: TTreeNode): boolean;
  function InChildNode(APID: integer; ANode: TTreeNode): boolean;
  var loop, NStart, NEnd: integer;
  begin
    Result := False;
    NStart := Node.AbsoluteIndex;
    if Node.getNextSibling = nil then NEnd := TreeView.Items.Count - 1
    else NEnd := Node.GetNextSibling.AbsoluteIndex;
    for loop := NStart + 1 to NEnd - 1 do
    begin
      Result := Result or (PNodeData(TreeView.Items.Item[loop].Data)^.Index = PID);
    end;
  end;
begin
  Result := False;
  //如果是根节点则忽略
  if Node.AbsoluteIndex = 0 then Exit;
  //如果PID不变则忽略
  if PID = PNodeData(Node.Parent.Data)^.Index then Exit;
  //如果父节点是自己也则忽略
  if PID = PNodeData(Node.Data)^.Index then Exit;
  //如果父节点编号是它其中一级子节点的编号，则忽略
  if InChildNode(PID, Node) then Exit;
  sqlStr := 'Update ' + TableName + ' set ' + FieldPID + '="' + IntToStr(PID) + '" where '
    + FieldIndex + ' = ' + IntToStr(PNodeData(Node.Data)^.Index);
  if execSQL(sqlStr, errStr) then Result := True;
end;

end.

