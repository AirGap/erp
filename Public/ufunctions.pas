unit ufunctions;

interface

uses
   Windows,Classes,SysUtils,ComObj,Dialogs, Grids, DBGrids,DBGridEh,Forms,StdCtrls,
   TreeFillThrd,TreeUtils,DB, ADODB, Variants,  Graphics, Controls,ComCtrls, RzFilSys;
   
type
   TTableList = record
     ATableName : string; //表名
     ADirectory : string; //附件目录
   end;

  function DBGridDelete(DBGrid : TDBGridEh ; Query : TADOQuery ; ATableName , dir : string ):Boolean;
  function ProjectPactDetele(Node : TTreeNode ; ATableName , szCodeList , dir : string):Boolean;
  function DeleteTableFile(AtableList : array of TTableList ; lpCodeList : string):Boolean;
  function getEnclosurePath(dir : string ; DBGrid : TDBGridEh):string;
  procedure SearchFiles(FileName , dir : string ;DBGrid:TDBGridEh;FileListBox : TRzFileListBox);
  procedure DeleteParentId(DBGrid : TDBGridEh ; Query : TADOQuery ; ATableName : string );

implementation

uses
   global,uDataModule;

function getEnclosurePath(dir : string ; DBGrid : TDBGridEh):string;
begin
  Result := ConcatEnclosure(dir,DBGrid.DataSource.DataSet.FieldByName('numbers').AsString) ;
end;

procedure SearchFiles(FileName , dir : string ;DBGrid:TDBGridEh;FileListBox : TRzFileListBox);
var
  str: string;
  List : TStringList;
  Items : TListItem;
  I: Integer;

begin

  str := getEnclosurePath(dir,DBGrid);
  List := TStringList.Create;
  try
    DoEnumAllFiles(str,list);
    for I := 0 to List.Count - 1 do
    begin
      if Pos(FileName,List.Strings[i]) <> 0 then
      begin
        FileListBox.ApplyFilePath(List.Strings[i]);
      end;
    end;

  finally
    List.Clear;
    List.Free;
  end;
  
end;

//删除表中数据时清除所有关联附件与表中数据
function DeleteTableFile(AtableList : array of TTableList ; lpCodeList : string):Boolean;
var
  s : Integer;
  i : Integer;
  szTableName : string;
  szDirectory : string;
  szPath : string;
  sqlstr : string;

begin
   Result := True;
   for I := 0 to High(AtableList) do
   begin

     szTableName := AtableList[i].ATableName;
     szDirectory := AtableList[i].ADirectory;

     if (Length(szTableName ) <> 0) and (Length(szDirectory) <> 0) then
     begin
       //查询附件路径
       sqlstr := 'select * from ' + szTableName +' where code in('''+ lpCodeList  +''')';

        with DM.Qry do
        begin
          Close;
          SQL.Clear;
          SQL.Text := sqlstr;
          Open;

          if 0 <> RecordCount then
          begin

            while not Eof do
            begin
              Application.ProcessMessages;
              szPath := Concat(g_Resources,'\' + szDirectory + '\' + FieldByName('numbers').AsString );
              if DirectoryExists(szPath) then
              begin
                DeleteDirectory(szPath);
              end;
              Next;
            end;

          end;

        end;

     end;

     DM.InDeleteData(szTableName,lpCodeList); //清理数据
   end;

end;

//删除Grid表中数据时同时删除附件
function DBGridDelete(DBGrid : TDBGridEh ; Query : TADOQuery ; ATableName , dir : string ):Boolean;
var
  szCode : string;
  s , Code : string;
  szNumber : string;
  i , j :  Integer;
  szFileName:string;
  szCodeList : string;
  szNumberList : string;
  str: string;

begin
  Result := False;
  if not DBGrid.DataSource.DataSet.IsEmpty then
  begin
    if DBGrid.SelectedRows.Count > 0 then
    begin
      if  Application.MessageBox( '您要删除所有选中的记录吗？', '删除记录？', MB_OKCANCEL + MB_ICONWARNING) = IDOK then
      begin
          Result := True;
          with Query do
          begin
            for i:=0 to (DBGrid.SelectedRows.Count - 1) do
            begin
            //  GotoBookmark(pointer(DBGrid.SelectedRows.Items[i])); //关键是这一句
              szNumber := DBGrid.DataSource.DataSet.FieldByName('numbers').AsString;//  DBGrid.DataSource.DataSet.FieldByName('编号').AsInteger;

              if Length(dir) <> 0 then
              begin
                str := ConcatEnclosure(Dir, szNumber);
              //  OutputLog(str);
                if DirectoryExists(str) then  DeleteDirectory(str);

              end;

              if Length(szNumberList) = 0 then
                 szNumberList := szNumber
              else
                szNumberList := szNumberList + '","' + szNumber;
            end;

            s := 'delete * from ' + ATableName +' where '+ 'numbers' +' in("' +  szNumberList+ '")';
            OutputLog(s);
            Close;
            SQL.Clear;
            SQL.Text := s;
            if 0 = ExecSQL then
            begin
              Application.MessageBox('删除失败',m_title,MB_OK + MB_ICONQUESTION ) ;
              Result := False;

            end;

          end;

      end;

    end
    else
    begin
      Application.MessageBox('请单击左边的序号选中一条记录或拖选一段记录，' +
        #13#10 +
        '也可以用Ctrl及Shift来进行多选，然后再删除记录。', '提示',
        MB_OK + MB_ICONINFORMATION);
    end;

  end;

end;

procedure DeleteParentId(DBGrid : TDBGridEh ; Query : TADOQuery ; ATableName : string );
var
  i : Integer;
  szParent : string;
  s : string;
  
begin

  if not DBGrid.DataSource.DataSet.IsEmpty then
  begin
    if DBGrid.SelectedRows.Count > 0 then
    begin
        with Query do
        begin
        //  OutputLog( 'Count:' + IntToStr(DBGrid.SelectedRows.Count));
          for i:=0 to (DBGrid.SelectedRows.Count - 1) do
          begin
          //  GotoBookmark(pointer(DBGrid.SelectedRows.Items[i])); //关键是这一句

            szParent := DBGrid.DataSource.DataSet.FieldByName('numbers').AsString;

            with DM.Qry do
            begin
              Close;
              SQL.Clear;
              SQL.Text := 'Select * from '+ ATableName +' where parent="' + szParent + '"';
              Open;

              if not Eof then
              begin

                  s := 'delete * from ' + ATableName +' where '+ 'Parent="' + szParent + '"';
                  Close;
                  SQL.Clear;
                  SQL.Text := s;
                  ExecSQL;

              end;

            end;

          end;


        end;

    end;

  end;

end;  

function ProjectPactDetele(Node : TTreeNode ; ATableName , szCodeList ,dir : string):Boolean;
var
  szNode : PNodeData;
  szDir : string;
begin
  Result:=False;
  if Assigned(Node) then
  begin
    if Node.Level > 1 then
    begin
       szNode := PNodeData(Node.Data);
       if MessageBox(0, PChar('你确认要删除"' + szNode.Caption + '"'),
             '提示', MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = IDYES then
       begin

          with DM.Qry do
          begin
            Close;
            SQL.Clear;
            SQL.Text := 'Select * from '+ ATableName +' where code in('''+ szCodeList  +''')';
            Open;
            while not Eof do
            begin
              szDir := ConcatEnclosure(Dir, '\' + FieldByName('numbers').AsString ) ;
              OutputLog(szDir);

              DeleteDirectory(szDir );
              Next;
            end;
          end;

          m_IsModify := True;
          Result := True;
          
       end;

    end
    else
    begin
      Application.MessageBox('禁止删除顶级节点',m_title, MB_OK + MB_ICONHAND )
    end;

  end
  else
  begin
     Application.MessageBox('请填选择要删除的合同!',m_title,MB_OK + MB_ICONQUESTION )
  end;

end;

end.
