unit ufrmMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, Dialogs, 
  dxBar, dxRibbon, dxRibbonForm, dxRibbonSkins, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinsdxRibbonPainter, dxRibbonCustomizationForm, cxContainer, cxEdit,
  cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxNavigator, Data.DB, cxDBData, cxCalc, cxCheckBox, dxSkinsdxBarPainter,
  cxTextEdit, Vcl.Menus, Vcl.ImgList, dxSkinsForm, cxBarEditItem, Vcl.ExtCtrls,
  cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxClasses, cxGridCustomView, cxGrid, RzTabs,
  Vcl.StdCtrls, RzPanel, RzButton, Vcl.ComCtrls, RzSplit, dxStatusBar,
  dxRibbonStatusBar, cxLabel, dxGallery, dxGalleryControl,
  dxRibbonBackstageViewGalleryControl, dxRibbonBackstageView, cxSplitter,TreeFillThrd,
  cxTreeView, cxTL, cxMaskEdit, cxTLdxBarBuiltInMenu, cxInplaceContainer,
  cxDBTL, cxTLData, System.ImageList, BMDThread;

type
  TfrmMain = class(TdxRibbonForm)
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxRibbon1: TdxRibbon;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbonBackstageView1: TdxRibbonBackstageView;
    dxRibbonBackstageViewTabSheet1: TdxRibbonBackstageViewTabSheet;
    dxRibbonStatusBar1: TdxRibbonStatusBar;
    dxRibbonBackstageViewGalleryControl1: TdxRibbonBackstageViewGalleryControl;
    cxLabel1: TcxLabel;
    dxRibbonBackstageViewGalleryControl1Group1: TdxRibbonBackstageViewGalleryGroup;
    dxSkinController1: TdxSkinController;
    dxRibbonBackstageViewGalleryControl1Group1Item1: TdxRibbonBackstageViewGalleryItem;
    dxBarManager1Bar2: TdxBar;
    cxBarEditItem1: TcxBarEditItem;
    il: TImageList;
    RzSplitter1: TRzSplitter;
    tv: TTreeView;
    RzToolbar1: TRzToolbar;
    RzToolButton1: TRzToolButton;
    RzSpacer2: TRzSpacer;
    RzToolButton2: TRzToolButton;
    RzSpacer3: TRzSpacer;
    RzToolButton3: TRzToolButton;
    RzSpacer4: TRzSpacer;
    Edit1: TEdit;
    cxLabel2: TcxLabel;
    RzSpacer5: TRzSpacer;
    RzToolButton4: TRzToolButton;
    RzPageControl1: TRzPageControl;
    TabSheet1: TRzTabSheet;
    TabSheet2: TRzTabSheet;
    TabSheet5: TRzTabSheet;
    Grid1: TcxGrid;
    tvView1: TcxGridDBBandedTableView;
    tvView1Column1: TcxGridDBBandedColumn;
    tvView1Column2: TcxGridDBBandedColumn;
    tvView1Column3: TcxGridDBBandedColumn;
    tvView1Column4: TcxGridDBBandedColumn;
    tvView1Column5: TcxGridDBBandedColumn;
    tvView1Column6: TcxGridDBBandedColumn;
    tvView1Column7: TcxGridDBBandedColumn;
    tvView1Column8: TcxGridDBBandedColumn;
    tvView1Column9: TcxGridDBBandedColumn;
    tvView1Column10: TcxGridDBBandedColumn;
    tvView1Column11: TcxGridDBBandedColumn;
    Lv1: TcxGridLevel;
    cxGrid3: TcxGrid;
    cxGridDBBandedTableView3: TcxGridDBBandedTableView;
    cxGridDBBandedColumn23: TcxGridDBBandedColumn;
    cxGridDBBandedColumn24: TcxGridDBBandedColumn;
    cxGridDBBandedColumn25: TcxGridDBBandedColumn;
    cxGridDBBandedColumn26: TcxGridDBBandedColumn;
    cxGridDBBandedColumn27: TcxGridDBBandedColumn;
    cxGridDBBandedColumn28: TcxGridDBBandedColumn;
    cxGridDBBandedColumn29: TcxGridDBBandedColumn;
    cxGridDBBandedColumn30: TcxGridDBBandedColumn;
    cxGridDBBandedColumn31: TcxGridDBBandedColumn;
    cxGridDBBandedColumn32: TcxGridDBBandedColumn;
    cxGridDBBandedColumn33: TcxGridDBBandedColumn;
    cxGridLevel3: TcxGridLevel;
    cxGrid4: TcxGrid;
    cxGridDBBandedTableView4: TcxGridDBBandedTableView;
    cxGridDBBandedColumn34: TcxGridDBBandedColumn;
    cxGridDBBandedColumn35: TcxGridDBBandedColumn;
    cxGridDBBandedColumn36: TcxGridDBBandedColumn;
    cxGridDBBandedColumn37: TcxGridDBBandedColumn;
    cxGridDBBandedColumn38: TcxGridDBBandedColumn;
    cxGridDBBandedColumn39: TcxGridDBBandedColumn;
    cxGridDBBandedColumn40: TcxGridDBBandedColumn;
    cxGridDBBandedColumn41: TcxGridDBBandedColumn;
    cxGridDBBandedColumn42: TcxGridDBBandedColumn;
    cxGridDBBandedColumn43: TcxGridDBBandedColumn;
    cxGridDBBandedColumn44: TcxGridDBBandedColumn;
    cxGridDBBandedColumn45: TcxGridDBBandedColumn;
    cxGridLevel4: TcxGridLevel;
    cxGridDBBandedTableView3Column1: TcxGridDBBandedColumn;
    cxGridDBBandedTableView3Column2: TcxGridDBBandedColumn;
    cxGridDBBandedTableView3Column3: TcxGridDBBandedColumn;
    cxGridDBBandedTableView3Column4: TcxGridDBBandedColumn;
    cxGridDBBandedTableView3Column5: TcxGridDBBandedColumn;
    cxGridDBBandedTableView3Column6: TcxGridDBBandedColumn;
    cxGridDBBandedTableView3Column7: TcxGridDBBandedColumn;
    RzToolbar2: TRzToolbar;
    RzSpacer1: TRzSpacer;
    RzToolButton5: TRzToolButton;
    RzSpacer6: TRzSpacer;
    RzToolButton6: TRzToolButton;
    RzSpacer7: TRzSpacer;
    RzToolButton7: TRzToolButton;
    RzSpacer8: TRzSpacer;
    RzToolButton8: TRzToolButton;
    RzSpacer9: TRzSpacer;
    RzToolButton9: TRzToolButton;
    RzSpacer10: TRzSpacer;
    cxLabel3: TcxLabel;
    Edit2: TEdit;
    RzSpacer11: TRzSpacer;
    RzToolButton10: TRzToolButton;
    dxBarButton1: TdxBarButton;
    dxBarButton2: TdxBarButton;
    dxBarButton3: TdxBarButton;
    dxBarButton4: TdxBarButton;
    il2: TcxImageList;
    cxGridDBBandedTableView3Column9: TcxGridDBBandedColumn;
    cxGridDBBandedTableView3Column10: TcxGridDBBandedColumn;
    RzSpacer12: TRzSpacer;
    RzToolButton11: TRzToolButton;
    TabSheet6: TRzTabSheet;
    cxGrid5: TcxGrid;
    cxGridDBBandedTableView5: TcxGridDBBandedTableView;
    cxGridDBBandedColumn46: TcxGridDBBandedColumn;
    cxGridDBBandedColumn47: TcxGridDBBandedColumn;
    cxGridDBBandedColumn48: TcxGridDBBandedColumn;
    cxGridDBBandedColumn49: TcxGridDBBandedColumn;
    cxGridDBBandedColumn50: TcxGridDBBandedColumn;
    cxGridDBBandedColumn51: TcxGridDBBandedColumn;
    cxGridDBBandedColumn52: TcxGridDBBandedColumn;
    cxGridDBBandedColumn55: TcxGridDBBandedColumn;
    cxGridDBBandedColumn56: TcxGridDBBandedColumn;
    cxGridDBBandedColumn57: TcxGridDBBandedColumn;
    cxGridLevel5: TcxGridLevel;
    tvView1Column12: TcxGridDBBandedColumn;
    cxGridDBBandedTableView5Column1: TcxGridDBBandedColumn;
    tvView1Column13: TcxGridDBBandedColumn;
    cxGridDBBandedTableView5Column2: TcxGridDBBandedColumn;
    cxGridDBBandedTableView4Column1: TcxGridDBBandedColumn;
    cxGridDBBandedTableView4Column2: TcxGridDBBandedColumn;
    cxGridDBBandedTableView4Column3: TcxGridDBBandedColumn;
    cxGridDBBandedTableView4Column5: TcxGridDBBandedColumn;
    cxGridDBBandedTableView4Column6: TcxGridDBBandedColumn;
    tvView1Column14: TcxGridDBBandedColumn;
    pm: TPopupMenu;
    RzSpacer13: TRzSpacer;
    RzToolButton12: TRzToolButton;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    tvView1Column15: TcxGridDBBandedColumn;
    tvView1Column16: TcxGridDBBandedColumn;
    tvView1Column18: TcxGridDBBandedColumn;
    tvView1Column17: TcxGridDBBandedColumn;
    tvView1Column19: TcxGridDBBandedColumn;
    RzSpacer14: TRzSpacer;
    cxGridDBBandedTableView5Column3: TcxGridDBBandedColumn;
    cxGridDBBandedTableView5Column4: TcxGridDBBandedColumn;
    cxGridDBBandedTableView5Column5: TcxGridDBBandedColumn;
    cxGridDBBandedTableView5Column6: TcxGridDBBandedColumn;
    RzToolButton13: TRzToolButton;
    pm2: TPopupMenu;
    N7: TMenuItem;
    N8: TMenuItem;
    RzSpacer15: TRzSpacer;
    RzToolButton14: TRzToolButton;
    RzSpacer16: TRzSpacer;
    RzToolButton15: TRzToolButton;
    Print: TPopupMenu;
    MenuItem5: TMenuItem;
    MenuItem6: TMenuItem;
    RzSpacer17: TRzSpacer;
    RzToolButton16: TRzToolButton;
    cxImageList1: TcxImageList;
    TabSheet3: TRzTabSheet;
    cxTreeList1: TcxTreeList;
    cxTreeList1Column1: TcxTreeListColumn;
    cxTreeList1Column2: TcxTreeListColumn;
    cxTreeList1Column3: TcxTreeListColumn;
    cxTreeList1Column4: TcxTreeListColumn;
    cxTreeList1Column5: TcxTreeListColumn;
    cxTreeList1Column6: TcxTreeListColumn;
    cxTreeList1Column7: TcxTreeListColumn;
    cxTreeList1Column8: TcxTreeListColumn;
    cxTreeList1Column9: TcxTreeListColumn;
    cxTreeList1Column10: TcxTreeListColumn;
    cxTreeList1Column11: TcxTreeListColumn;
    cxTreeList1Column12: TcxTreeListColumn;
    cxTreeList1Column13: TcxTreeListColumn;
    cxTreeList1Column14: TcxTreeListColumn;
    cxTreeList1Column15: TcxTreeListColumn;
    BMDThread1: TBMDThread;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton1: TdxBarLargeButton;
    procedure FormCreate(Sender: TObject);
    procedure RzToolButton1Click(Sender: TObject);
    procedure RzPageControl1TabClick(Sender: TObject);
    procedure tvClick(Sender: TObject);
    procedure RzToolButton16Click(Sender: TObject);
    procedure tvKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure tvDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure tvDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure tvEdited(Sender: TObject; Node: TTreeNode; var S: string);
    procedure tvExpanded(Sender: TObject; Node: TTreeNode);
    procedure RzToolButton5Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

{$R *.dfm}

{ TForm1 }

procedure getSontreeCode(Anode: TTreeNode;var s : string);
var
  i: integer;
  node: TTreeNode;
  szData : PNodeData;
begin
    for i := 0 to Anode.Count - 1 do
    begin
      Node := ANode.Item[i];
      if Assigned(node) then
      begin
        szData := PNodeData(Node.Data);
        if Length(s) = 0 then
          s := szData.Code + ''',''' + PNodeData(ANode.Data).Code
        else
          s := s + ''','''+ szData.Code;
      end;

      if (node.Count > 0)   then
      begin
        getSontreeCode(Node , s);
      end;

    end;
end;

function treeCldnode(Anode: TTreeNode;var s : string):string;stdcall;
var
  i: integer;
  node: TTreeNode;
  szData : PNodeData;
  szCodeList : string;

begin
  if Assigned(Anode) then
  begin
    szData := PNodeData(ANode.Data);
    s := szData.Code;
    if Anode.Count <> 0 then
    begin
      getSontreeCode(Anode,szCodeList);
      Result := szCodeList;
    end
    else
    begin
      Result := s;
    end;

    s := Result;
  end;

end;


procedure TfrmMain.FormCreate(Sender: TObject);
begin
  DisableAero := True;
end;

procedure TfrmMain.RzPageControl1TabClick(Sender: TObject);
begin
  case Self.RzPageControl1.ActivePageIndex of
    0:
    begin

    end;
    1:
    begin

    end;
    2:
    begin

    end;
    3:
    begin

    end;
    4:
    begin

    end;
  end;
end;

{
procedure TShowTextFrm.Button1Click(Sender: TObject);
var NowNode,Node : TTreeNode;
    iLevel:integer;
begin
  NowNode:=TreeView1.Selected;
  if NowNode.HasChildren then
  begin
    Node:=NowNode.getFirstChild;
    iLevel:=Node.Level;
    ShowMessage(Node.Text);
    While (Node.GetNext<>nil) and
          (Node.GetNext.Level>=iLevel) do
    begin
      Node:=Node.GetNext;
      ShowMessage(Node.Text);
    end;
  end;
end;

}

procedure TfrmMain.RzToolButton16Click(Sender: TObject);
var
  Node : TTreeNode;
begin
  Node := Self.tv.Selected;
  if Assigned(Node) then
  begin
    Node.EditText;
  end;
end;

procedure TfrmMain.RzToolButton1Click(Sender: TObject);
var
  Node , ChildNode : TTreeNode;

begin
  Node := Self.tv.Selected;
  if Assigned(Node) then
  begin
    if Sender = Self.RzToolButton1 then
    begin
      ChildNode := Self.tv.Items.AddChild(Node,'输入名称');
      ChildNode.Expand(True);
      ChildNode.Selected := True;
      ChildNode.EditText;
    end else
    if Sender = Self.RzToolButton2 then
    begin
      ChildNode.Expand(True);
      ChildNode.Selected := True;
      ChildNode.EditText;
    end;
   {
    if TreeNode.Level < 3  then
   TreeNode.Expand(true);
else
   TreeNode.Collapse(True);
   }
  end;
end;

procedure TfrmMain.RzToolButton5Click(Sender: TObject);
var
  NodeList : TcxTreeListNode;
  I: Integer;

begin
  for I := 0 to 300 do
  begin
    NodeList := Self.cxTreeList1.Add;
    NodeList.Texts[0] := '000';
  end;
end;

procedure TfrmMain.tvClick(Sender: TObject);
var
  Node : TTreeNode;

begin
  {
  Node := Self.tv.Selected;
  if Assigned(Node) then
  begin
    Self.tv.Items.Add(Node,'');
  end;
  }
end;

procedure TfrmMain.tvDragDrop(Sender, Source: TObject; X, Y: Integer);
begin

end;
{
var
  TargetNode, SourceNode: TTreeNode;

begin
  TargetNode := TreeView1.DropTarget;
  SourceNode := TreeView1.Selected;
  if MessageBox(handle, '您确认要移动合同吗？', '提示', MB_ICONQUESTION
    + MB_YESNO) = IDYES then
    if g_PatTree.ModifyNodePID(PNodeData(TargetNode.Data)^.Index, SourceNode) then
       g_PatTree.FillTree(g_Parent,g_pacttype);
end;
}
procedure TfrmMain.tvDragOver(Sender, Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
begin
  {
  if (Source = TreeView1) then
     Accept := True
  else
     Accept := False;
  }
end;


procedure TfrmMain.tvEdited(Sender: TObject; Node: TTreeNode; var S: string);
begin
//编辑完成
end;

procedure TfrmMain.tvExpanded(Sender: TObject; Node: TTreeNode);
begin
  if Node.Count = 0 then
  begin
    Node.ImageIndex := 1;
  end else
  begin
    Node.ImageIndex := 0;
  end;
end;

procedure TfrmMain.tvKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Node : TTreeNode;
begin
  if (ssCtrl in Shift) and (Key = 13) then
  begin
    Node := Self.tv.Selected;
  end;
end;

end.
