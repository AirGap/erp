unit ufrmProjectSubcontract;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls, RzButton, RzPanel,
  Vcl.ExtCtrls, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, dxSkinsCore, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxCore,
  cxDateUtils, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxNavigator, Data.DB, cxDBData, cxCheckBox, cxCalendar,
  cxTextEdit, cxCurrencyEdit, cxSpinEdit, cxMemo, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,ufrmBaseController,
  cxGridCustomView, cxGrid, cxMaskEdit, cxDropDownEdit, cxLabel, Data.Win.ADODB,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light, dxSkinVS2010,
  cxDBLookupComboBox, Vcl.StdCtrls, Vcl.Menus;

type
  TfrmProjectSubcontract = class(TfrmBaseController)
    RzPanel2: TRzPanel;
    Splitter1: TSplitter;
    RzPanel1: TRzPanel;
    RzToolbar12: TRzToolbar;
    RzSpacer100: TRzSpacer;
    RzToolButton75: TRzToolButton;
    RzSpacer101: TRzSpacer;
    RzToolButton76: TRzToolButton;
    RzSpacer102: TRzSpacer;
    RzToolButton77: TRzToolButton;
    RzSpacer103: TRzSpacer;
    RzToolButton78: TRzToolButton;
    RzToolButton79: TRzToolButton;
    RzSpacer104: TRzSpacer;
    RzToolButton80: TRzToolButton;
    RzSpacer105: TRzSpacer;
    RzSpacer106: TRzSpacer;
    RzSpacer107: TRzSpacer;
    RzSpacer108: TRzSpacer;
    RzToolButton81: TRzToolButton;
    RzSpacer109: TRzSpacer;
    RzToolButton82: TRzToolButton;
    RzSpacer110: TRzSpacer;
    RzSpacer111: TRzSpacer;
    RzToolButton83: TRzToolButton;
    cxLabel50: TcxLabel;
    cxDateEdit1: TcxDateEdit;
    cxLabel51: TcxLabel;
    cxDateEdit2: TcxDateEdit;
    Grid: TcxGrid;
    tvEngineeringQuantity: TcxGridDBTableView;
    tvQuantityCol1: TcxGridDBColumn;
    tvQuantityCol2: TcxGridDBColumn;
    tvEngineeringQuantityColumn1: TcxGridDBColumn;
    tvEngineeringQuantityColumn2: TcxGridDBColumn;
    tvEngineeringQuantityColumn3: TcxGridDBColumn;
    tvEngineeringQuantityColumn4: TcxGridDBColumn;
    tvEngineeringQuantityColumn5: TcxGridDBColumn;
    tvEngineeringQuantityColumn6: TcxGridDBColumn;
    tvEngineeringQuantityColumn7: TcxGridDBColumn;
    tvEngineeringQuantityColumn9: TcxGridDBColumn;
    tvEngineeringQuantityColumn10: TcxGridDBColumn;
    tvEngineeringQuantityColumn8: TcxGridDBColumn;
    tvEngineeringQuantityColumn11: TcxGridDBColumn;
    tvEngineeringQuantityColumn12: TcxGridDBColumn;
    tvEngineeringQuantityColumn14: TcxGridDBColumn;
    tvEngineeringQuantityColumn15: TcxGridDBColumn;
    tvEngineeringQuantityColumn16: TcxGridDBColumn;
    tvDetailedGrid: TcxGridDBTableView;
    tvDetailedGridColumn1: TcxGridDBColumn;
    tvDetailedGridColumn2: TcxGridDBColumn;
    tvDetailedGridColumn3: TcxGridDBColumn;
    tvDetailedGridColumn4: TcxGridDBColumn;
    tvDetailedGridColumn5: TcxGridDBColumn;
    tvDetailedGridColumn6: TcxGridDBColumn;
    tvDetailedGridColumn7: TcxGridDBColumn;
    tvDetailedGridColumn8: TcxGridDBColumn;
    LvQuantity: TcxGridLevel;
    LvDetailed: TcxGridLevel;
    ADOQuantity: TADOQuery;
    DataQuantity: TDataSource;
    tvEngineeringQuantityColumn13: TcxGridDBColumn;
    tvEngineeringQuantityColumn17: TcxGridDBColumn;
    tvEngineeringQuantityColumn18: TcxGridDBColumn;
    ADODetailed: TADOQuery;
    DataDetailed: TDataSource;
    qry: TADOQuery;
    ds: TDataSource;
    Print: TPopupMenu;
    Excel1: TMenuItem;
    N2: TMenuItem;
    RzSpacer1: TRzSpacer;
    RzToolButton1: TRzToolButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure RzToolButton80Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure RzToolButton75Click(Sender: TObject);
    procedure tvQuantityCol1GetDisplayText(
      Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
      var AText: string);
    procedure ADOQuantityAfterInsert(DataSet: TDataSet);
    procedure RzToolButton76Click(Sender: TObject);
    procedure tvEngineeringQuantityEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure RzToolButton77Click(Sender: TObject);
    procedure RzToolButton78Click(Sender: TObject);
    procedure RzToolButton83Click(Sender: TObject);
    procedure tvEngineeringQuantityColumnSizeChanged(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
    procedure RzToolButton82Click(Sender: TObject);
    procedure RzToolButton81Click(Sender: TObject);
    procedure tvEngineeringQuantityDblClick(Sender: TObject);
    procedure tvEngineeringQuantityColumn16GetPropertiesForEdit(
      Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
      var AProperties: TcxCustomEditProperties);
    procedure tvEngineeringQuantityColumn18GetPropertiesForEdit(
      Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
      var AProperties: TcxCustomEditProperties);
    procedure tvEngineeringQuantityColumn4GetPropertiesForEdit(
      Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
      var AProperties: TcxCustomEditProperties);
    procedure tvEngineeringQuantityColumn6PropertiesInitPopup(Sender: TObject);
    procedure tvEngineeringQuantityColumn6PropertiesCloseUp(Sender: TObject);
    procedure tvEngineeringQuantityEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure ADOQuantityAfterOpen(DataSet: TDataSet);
    procedure qryAfterOpen(DataSet: TDataSet);
    procedure ADODetailedAfterOpen(DataSet: TDataSet);
    procedure tvEngineeringQuantityColumn8PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure tvEngineeringQuantityColumn9PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure tvQuantityCol2PropertiesChange(Sender: TObject);
    procedure tvEngineeringQuantityStylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
    procedure tvEngineeringQuantityTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems1GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure tvEngineeringQuantityColumn10GetPropertiesForEdit(
      Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
      var AProperties: TcxCustomEditProperties);
    procedure N2Click(Sender: TObject);
    procedure Excel1Click(Sender: TObject);
    procedure RzToolButton1Click(Sender: TObject);
  private
    { Private declarations }
    g_ProjectName : string;
    g_PostCode    : string;
    IsSave : Boolean;
    Prefix : string;
    Suffix : string;
    function SetSumMoney(ACount , AUnitPrice : Variant):Boolean;
    procedure  SetcxGrid(lpGridView : TcxGridDBTableView ; index : Byte ;lpSuffix:string);
  public
    { Public declarations }
    procedure WndProc(var Message: TMessage); override;  // 第一优先权
  end;

var
  frmProjectSubcontract: TfrmProjectSubcontract;

implementation

uses
   uProjectFrame,global,ufunctions,uDataModule,ufrmIsViewGrid,ufrmCalcExpression;

{$R *.dfm}

procedure TfrmProjectSubcontract.WndProc(var Message: TMessage);
var
  pMsg : PProject;
  s : string;
  szCodeList : string;
  szTableList : array[0..2] of TTableList;
begin
  case Message.Msg of
    WM_FrameClose:
      begin
        Self.ADOQuantity.Close;
        Self.qry.Close;
        Self.ADODetailed.Close;
      end;
    WM_FrameView :
    begin
      pMsg := PProject(Message.LParam);
      g_PostCode := pMsg.dwCode;
      g_ProjectName := pMsg.dwName;

      s := 'Select *from ' + g_Table_Project_EngineeringQuantity + ' Where Code ="' +  g_PostCode + '"';

      with Self.ADOQuantity do
      begin
        Close;
        SQL.Clear;
        SQL.Text := s;
        Open;
      end;
      s := 'Select *from ' +  g_Table_Project_EngineeringDetailed;
      with Self.ADODetailed do
      begin
        Close;
        SQL.Clear;
        SQL.Text := s;
        Open;
      end;

      with Self.qry do
      begin
        Close;
        SQL.Clear;
      //  SQL.Text := 'Select * from ' + g_Table_Maintain_Work ;
        SQL.Text := 'Select * from '     + g_Table_Maintain_SubContract +
                    ' AS PT left join '  + g_Table_CompanyManage +
                    ' AS PS on PT.SignName = PS.SignName WHERE ProjectName="' + g_ProjectName + '"';
        Open;
      end;

    end;
    WM_FrameDele :
    begin
      pMsg   := PProject(Message.LParam);
      szCodeList := pMsg.dwCodeList;

      szTableList[0].ATableName := g_Table_Project_EngineeringQuantity;
      szTableList[0].ADirectory := g_Dir_Project_Quantity;

      szTableList[1].ATableName := g_Table_Project_EngineeringDetailed;
      szTableList[1].ADirectory := '';

      DeleteTableFile(szTableList,szCodeList);
    end;
  end;
  // 一定要加上这句，否则编译通不过。因为绝大部分消息没人处理了
  inherited WndProc(Message); // 会一路向上调用，直到TControl.WndProc调用Dispatch来寻找消息处理函数
end;

procedure  TfrmProjectSubcontract.SetcxGrid(lpGridView : TcxGridDBTableView ; index : Byte ;
lpSuffix:string);
var
  i : Integer;
begin
  for I := 0 to lpGridView.ColumnCount - 1 do
  begin
    Application.ProcessMessages;
    SetGrid(lpGridView.Columns[i],index, Self.Name + lpSuffix);
  end;

end;

procedure TfrmProjectSubcontract.ADODetailedAfterOpen(DataSet: TDataSet);
begin
  inherited;
  TADOQuery(DataSet).Properties.Get_Item('Update Criteria').Value :=0;
end;

procedure TfrmProjectSubcontract.ADOQuantityAfterInsert(DataSet: TDataSet);
var
  szColName : string;
  szCode : string;

begin
  with DataSet do
  begin
    if State <> dsInactive then
    begin
      szColName := Self.tvEngineeringQuantityColumn2.DataBinding.FieldName;
      FieldByName(szColName).Value := Date;

      FieldByName('Code').AsString := g_PostCode;

      szColName := Self.tvEngineeringQuantityColumn1.DataBinding.FieldName; //编号
      szCode := Prefix + GetRowCode(g_Table_Project_EngineeringQuantity,szColName,Suffix,50000);
      FieldByName(szColName).Value := szCode;

      szColName := Self.tvEngineeringQuantityColumn3.DataBinding.FieldName;
      FieldByName( szColName ).Value := g_ProjectName;

      szColName := Self.tvQuantityCol2.DataBinding.FieldName;
      FieldByName(szColName).Value := True;

      szColName := Self.tvEngineeringQuantityColumn9.DataBinding.FieldName;
      FieldByName(szColName).Value := 0;

      szColName := Self.tvEngineeringQuantityColumn8.DataBinding.FieldName;
      FieldByName(szColName).Value := 0;

      szColName := Self.tvEngineeringQuantityColumn11.DataBinding.FieldName;
      FieldByName(szColName).Value := 0;
    end;
  end;
end;

procedure TfrmProjectSubcontract.ADOQuantityAfterOpen(DataSet: TDataSet);
begin
  inherited;
  TADOQuery(DataSet).Properties.Get_Item('Update Criteria').Value :=0;
end;

procedure TfrmProjectSubcontract.Excel1Click(Sender: TObject);
begin
  inherited;
  CxGridToExcel(Self.Grid,'分包明细表');
end;

procedure TfrmProjectSubcontract.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmProjectSubcontract.FormCreate(Sender: TObject);
var
  ChildFrame : TProjectFrame;
begin
  Prefix := 'FB';
  Suffix := '00005';
  ChildFrame := TProjectFrame.Create(Self);
  ChildFrame.dwParentHandle := Handle;
  ChildFrame.Parent := Self.RzPanel2;
  ChildFrame.IsSystem := True;
  ChildFrame.FrameClick(Self);

  Self.cxDateEdit1.Date := Date;
  Self.cxDateEdit2.Date := Date;

  SetcxGrid(Self.tvEngineeringQuantity,0,g_Dir_Project_Quantity);
end;

procedure TfrmProjectSubcontract.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 27 then
  begin
    Self.RzToolButton80.Click;
  end;
end;

procedure TfrmProjectSubcontract.N2Click(Sender: TObject);
begin
  inherited;
  DM.BasePrinterLink1.Component := Self.Grid;
  DM.BasePrinterLink1.ReportTitleText := '项目名称：' + g_Projectname;
  DM.BasePrinter.Preview(True, nil);
end;

procedure TfrmProjectSubcontract.qryAfterOpen(DataSet: TDataSet);
begin
  inherited;
  TADOQuery(DataSet).Properties.Get_Item('Update Criteria').Value :=0;
end;

procedure TfrmProjectSubcontract.RzToolButton1Click(Sender: TObject);
begin
  inherited;
  {
  if Self.tvQuantityCol2.EditValue then
  begin
    with  do
    begin
      if State <> dsInactive then
      begin
        if (State <> dsEdit) and (State <> dsInsert) then
           Edit;
      end;
    end;

  end else
  begin
    Application.MessageBox( '冻结状态下不可以编辑！', '提示:', MB_OK + MB_ICONWARNING)
  end;
  }
  ADOIsEdit( Self.tvQuantityCol2.EditValue , Self.ADOQuantity)
end;

procedure TfrmProjectSubcontract.RzToolButton75Click(Sender: TObject);
begin

  with Self.ADOQuantity do
  begin

    if State = dsInactive then
    begin
      Application.MessageBox( '当前查询状态无效，单击项目管理在进行操作？', '提示:', MB_OKCANCEL + MB_ICONWARNING)
    end else
    begin
      First;
      Insert;
    //  Append;

      Self.tvEngineeringQuantityColumn18.FocusWithSelection;
      Self.Grid.SetFocus;
      keybd_event(VK_RETURN,0,0,0);

    end;

  end;

end;

procedure TfrmProjectSubcontract.RzToolButton76Click(Sender: TObject);
begin
  IsDeleteEmptyData(Self.ADOQuantity ,
                    g_Table_Project_EngineeringQuantity ,
                    Self.tvEngineeringQuantityColumn6.DataBinding.FieldName);
  IsSave := False;
end;

procedure TfrmProjectSubcontract.RzToolButton77Click(Sender: TObject);
var
  szName : string;
  szIndex: Integer;
  szNumbers : string;
  str:string;
  Id : string;
  dir: string;
  i : Integer;

begin
 //删除
  if  Application.MessageBox( '您要删除所有选中的记录吗？', '删除记录？', MB_OKCANCEL + MB_ICONWARNING) = IDOK then
  begin
      with Self.tvEngineeringQuantity.DataController.DataSource.DataSet do
      begin
        if not IsEmpty then
        begin
          for I := 0 to Self.tvEngineeringQuantity.Controller.SelectedRowCount -1 do
          begin
            szName := Self.tvEngineeringQuantityColumn1.DataBinding.FieldName;
            szIndex := Self.tvEngineeringQuantity.GetColumnByFieldName(szName).Index;
            Id := Self.tvEngineeringQuantity.Controller.SelectedRows[i].Values[szIndex];

            dir := ConcatEnclosure(g_Dir_Project_Quantity, id);
            if DirectoryExists(dir) then  DeleteDirectory(dir);
            if Length(str) = 0 then
              str := Id
            else
              str := str + '","' + Id;
          end;

          with DM.Qry do
          begin
            Close;
            SQL.Clear;
            SQL.Text := 'delete * from ' +  g_Table_Project_EngineeringQuantity + ' where ' + 'numbers' +' in("' + str + '")';
            ExecSQL;
            SQL.Clear;
            SQL.Text := 'delete * from ' +  g_Table_Project_EngineeringDetailed + ' where ' + 'parent' +' in("' + str + '")';
            ExecSQL;
          end;
          Self.ADOQuantity.Requery;
        end;

      end;

  end;





end;

procedure TfrmProjectSubcontract.RzToolButton78Click(Sender: TObject);
var
  Child : TfrmIsViewGrid;
  i : Integer;
begin
  inherited;
  Child := TfrmIsViewGrid.Create(Application);
  try
    Child.g_fromName := Self.Name + g_Dir_Project_Quantity;
    Child.ShowModal;
    SetcxGrid(Self.tvEngineeringQuantity,0,g_Dir_Project_Quantity);
  finally
    Child.Free;
  end;
end;

procedure TfrmProjectSubcontract.RzToolButton80Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmProjectSubcontract.RzToolButton81Click(Sender: TObject);
var
  sqltext : string;
  szColName : string;
begin
//范围查询
  inherited;
  szColName   := Self.tvEngineeringQuantityColumn2.DataBinding.FieldName;
  sqltext := 'select *from ' + g_Table_Project_EngineeringQuantity + ' where Code= "' + g_PostCode + ' " and ' + szColName +' between :t1 and :t2';
  SearchDateRange(Self.cxDateEdit1.Text,Self.cxDateEdit2.Text,sqltext,Self.ADOQuantity);
end;

procedure TfrmProjectSubcontract.RzToolButton82Click(Sender: TObject);
var
  s : string;
begin
  inherited;
  s := 'ColligateFile';
  CreateOpenDir(Handle,Concat(g_Resources , g_Dir_Project_Quantity + '\' + s),True);
end;

function TfrmProjectSubcontract.SetSumMoney(ACount , AUnitPrice : Variant):Boolean;
var
  szSumMoney : Variant;
  szColName : string;

begin
  if (ACount <> null) and (AUnitPrice <> null) then
  begin
    with Self.ADOQuantity do
    begin
      if State <> dsInactive then
      begin
        szColName := Self.tvEngineeringQuantityColumn11.DataBinding.FieldName;
        FieldByName(szColName).Value := ACount * AUnitPrice;

        szColName := Self.tvEngineeringQuantityColumn8.DataBinding.FieldName;
        FieldByName(szColName).Value:=ACount;
        szColName := Self.tvEngineeringQuantityColumn9.DataBinding.FieldName;
        FieldByName(szColName).Value:= AUnitPrice;

      end;
    end;
  end;

end;

procedure TfrmProjectSubcontract.RzToolButton83Click(Sender: TObject);
var
  szRowIndex : Integer;

  Child : TfrmCalcExpression;
  szTableName : string;
  szNumbers : string;
  szProjectName : string;
  szUnitPrice   : Currency;
  szRecordCount : Integer;

begin
  inherited;

  Child := TfrmCalcExpression.Create(Application);
  try
    Child.g_TableName := g_Table_Project_EngineeringDetailed;
    {
    szRowIndex := Self.tvEngineeringQuantity.Controller.FocusedRowIndex;
    with Self.ADOQuantity do
    begin
      if State <> dsInactive then
      begin
        if (State <> dsInsert) or (State <> dsEdit) then
        begin
          UpdateBatch(arAll);
        end;
      end;
    end;
    Self.tvEngineeringQuantity.Controller.FocusedRowIndex := szRowIndex;
    }
    with Self.tvEngineeringQuantity.DataController.DataSource.DataSet do
    begin

      if IsEmpty then
      begin
        ShowMessage('未选择工程量,无法查看或新增明细!');
      end else
      begin

        szTableName := Self.tvEngineeringQuantityColumn1.DataBinding.FieldName;
        szNumbers   := FieldByName(szTableName).AsString ;

        {
        with DM.Qry do
        begin
          Close;
          SQL.Clear;
          SQL.Text := 'Select *from ' + g_Table_Project_EngineeringQuantity + ' Where ' + szTableName + '="' + szNumbers + '"';
          Open;
          szRecordCount := RecordCount;
        end;

        if 0 = szRecordCount then
        begin
          ShowMessage(Self.tvEngineeringQuantityColumn1.Caption + ':' + szNumbers + ' 未保存，需要保存后才能新增编辑工程量明细');
        end else
        begin
        end;
        }
          szTableName   := Self.tvEngineeringQuantityColumn3.DataBinding.FieldName;
          szProjectName := FieldByName(szTableName).AsString;
          Child.g_PostCode := g_PostCode;
          Child.g_Prefix   := 'PMX-';
          Child.g_TableName    := g_Table_Project_EngineeringDetailed;
          Child.g_ParentNumber := szNumbers;
          Child.StatusBar1.Panels[0].Text := Self.tvEngineeringQuantityColumn1.Caption +  ':' + szNumbers;
          Child.StatusBar1.Panels[1].Text := Self.tvEngineeringQuantityColumn3.Caption +  ':' + szProjectName;
          szTableName := Self.tvEngineeringQuantityColumn9.DataBinding.FieldName;
          szUnitPrice := FieldByName(szTableName).AsCurrency;
          Child.ShowModal;
          if (State <> dsEdit) or (State <> dsInsert) then Edit;
          szTableName := Self.tvEngineeringQuantityColumn11.DataBinding.FieldName;
          FieldByName(szTableName).AsCurrency := StrToFloatDef(Child.g_CalcCount,0) * szUnitPrice;

          szTableName := Self.tvEngineeringQuantityColumn8.DataBinding.FieldName;
          FieldByName(szTableName).AsString  := Child.g_CalcCount;
          szTableName := Self.tvEngineeringQuantityColumn10.DataBinding.FieldName;
          FieldByName(szTableName).AsString := Child.g_Company;

          Self.ADODetailed.Requery();
          {
          if szUnitPrice = 0 then
          begin
            Self.tvEngineeringQuantityColumn7.FocusWithSelection;
          //  Self.tvEngineeringQuantityColumn9.Focused := True;
            Application.MessageBox( '请输入工程量单价，否则无法进行计算总价', '提示', MB_OKCANCEL + MB_ICONWARNING);
          end else
          begin

          end;
          }

      end;

    end;

  finally
    Child.Free;
  end;
end;

procedure TfrmProjectSubcontract.tvEngineeringQuantityColumn10GetPropertiesForEdit(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AProperties: TcxCustomEditProperties);
var
  List : TStringList;
  i : Integer;

begin
  inherited;
  with (AProperties AS TcxComboBoxProperties) do
  begin
    Items.Clear;
    List := TStringList.Create;
    try
      GetCompany(List,0);
      for I := 0 to List.Count-1 do Items.Add(List.Strings[i]);
    finally
      List.Free;
    end;

  end;
end;

procedure TfrmProjectSubcontract.tvEngineeringQuantityColumn16GetPropertiesForEdit(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AProperties: TcxCustomEditProperties);
begin
  inherited;
  GetCompanyInfo(AProperties);
end;

procedure TfrmProjectSubcontract.tvEngineeringQuantityColumn18GetPropertiesForEdit(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AProperties: TcxCustomEditProperties);
begin
  inherited;
  GetCompanyInfo(AProperties);
end;

procedure TfrmProjectSubcontract.tvEngineeringQuantityColumn4GetPropertiesForEdit(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AProperties: TcxCustomEditProperties);
begin
  inherited;
  DM.Qry.Close;
  DM.Qry.SQL.Clear;
  DM.Qry.SQL.Text := 'Select * from ' + g_Table_Maintain_WorkType;
  DM.Qry.Open;
  if DM.Qry.RecordCount <> 0 then
  begin
    with (AProperties AS TcxComboBoxProperties) do
    begin
      Items.Clear;
      while Not DM.Qry.Eof do
      begin
        Items.Add(DM.Qry.Fields[1].AsString);
        DM.Qry.Next;

      end;

    end;

  end;

end;

procedure TfrmProjectSubcontract.tvEngineeringQuantityColumn6PropertiesCloseUp(
  Sender: TObject);
var
  szCol1Name:Variant;
  szCol2Name:Variant;
  szCol3Name:Variant;
  szCol4Name:Variant;
  szCol5Name:Variant;
  szCol6Name:Variant;
  szCol7Name:Variant;
  szCol8Name:Variant;

  szRowIndex:Integer;
  szColName :string;
  szRecordCount : Integer;

begin
  inherited;

  with (Sender AS TcxLookupComboBox).Properties.Grid do
  begin
    szRowIndex := FocusedRowIndex;
    if szRowIndex >= 0 then
    begin
      szCol1Name:= DataController.Values[szRowIndex,0]; //名称
      szCol2Name:= DataController.Values[szRowIndex,1]; //简码
      szCol3Name:= DataController.Values[szRowIndex,2]; //单价
      szCol4Name:= DataController.Values[szRowIndex,3]; //录入日期
      szCol5Name:= DataController.Values[szRowIndex,4]; //分包内容
      szCol6Name:= DataController.Values[szRowIndex,5]; //施工工艺
      szCol7Name:= DataController.Values[szRowIndex,6]; //单位
      szCol8Name:= DataController.Values[szRowIndex,7]; //工种

      if szCol1Name <> null then
      begin
        with Self.ADOQuantity do
        begin
          if State <> dsInactive then
          begin
            if ( State = dsInsert ) or (State = dsEdit) then
            begin
              szColName := Self.tvEngineeringQuantityColumn6.DataBinding.FieldName; //名称
              FieldByName(szColName).Value := szCol1Name;

              szColName := Self.tvEngineeringQuantityColumn4.DataBinding.FieldName;//工种
              FieldByName(szColName).Value := szCol8Name;

              szColName := Self.tvEngineeringQuantityColumn13.DataBinding.FieldName;//分包内容
              FieldByName(szColName).Value := szCol5Name;

              szColName := Self.tvEngineeringQuantityColumn17.DataBinding.FieldName;//施工工艺
              FieldByName(szColName).Value := szCol6Name;

              szColName := Self.tvEngineeringQuantityColumn9.DataBinding.FieldName;//单价
              FieldByName(szColName).Value := szCol3Name;

              szColName := Self.tvEngineeringQuantityColumn10.DataBinding.FieldName;//单位
              FieldByName(szColName).Value := szCol7Name;

            end;
          end;

        end;

      end;

    end;

  end;


end;

procedure TfrmProjectSubcontract.tvEngineeringQuantityColumn6PropertiesInitPopup(
  Sender: TObject);
begin
  inherited;
  with Self.ADOQuantity do
  begin
    if State <> dsInactive then
    begin
      if (State <> dsInsert) or (State <> dsEdit) then
      begin
        Edit;
      end;
    end;  
  end;  
end;

procedure TfrmProjectSubcontract.tvEngineeringQuantityColumn8PropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
var
  szRowIndex : Integer;
  szUnitPrice: Variant;

begin
  inherited;
  szRowIndex := Self.tvEngineeringQuantity.Controller.FocusedRowIndex;
  szUnitPrice:= Self.tvEngineeringQuantity.DataController.Values[szRowIndex,Self.tvEngineeringQuantityColumn9.Index];

  if DisplayValue = 0 then
  begin
     Self.ADOQuantity.UpdateBatch(arAll);
     Self.RzToolButton83.Click;
  end else
  begin
    SetSumMoney(DisplayValue , szUnitPrice );
  end;
end;

procedure TfrmProjectSubcontract.tvEngineeringQuantityColumn9PropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
var
  szRowIndex : Integer;
  szCount: Variant;

begin
  inherited;
  szRowIndex := Self.tvEngineeringQuantity.Controller.FocusedRowIndex;
  szCount:= Self.tvEngineeringQuantity.DataController.Values[szRowIndex,Self.tvEngineeringQuantityColumn8.Index];
  SetSumMoney(szCount , DisplayValue );
end;

procedure TfrmProjectSubcontract.tvEngineeringQuantityColumnSizeChanged(
  Sender: TcxGridTableView; AColumn: TcxGridColumn);
begin
  SetcxGrid(Self.tvEngineeringQuantity,1,g_Dir_Project_Quantity);
end;

procedure TfrmProjectSubcontract.tvEngineeringQuantityDblClick(Sender: TObject);
var
  szDir : string;
begin
  inherited;
  szDir := Concat(g_Resources, '\'+ g_Dir_Project_Quantity);
  CreateEnclosure(Self.tvEngineeringQuantity.DataController.DataSource.DataSet,Handle,szDir);
end;

procedure TfrmProjectSubcontract.tvEngineeringQuantityEditing(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
  var AAllow: Boolean);
var
  szColName : string;
begin
  inherited;

  with Self.ADOQuantity do
  begin
    if State <> dsInactive then
    begin
      if (State <> dsEdit) and (State <> dsInsert) then
        AAllow := False
      else
        AAllow := True;
    end;
  end;

  szColName := Self.tvQuantityCol2.DataBinding.FieldName;
  if AItem.Index <> Self.tvQuantityCol2.Index then
  begin
  end else
  begin
    if AItem.Index = Self.tvQuantityCol1.Index then
    begin
      AAllow := False;
    end;
  end;

end;

procedure TfrmProjectSubcontract.tvEngineeringQuantityEditKeyDown(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
  AEdit: TcxCustomEdit; var Key: Word; Shift: TShiftState);
var
  szValue : Variant;

begin
  inherited;
  if (Key = 13) then
  begin

    if AItem.Index = Self.tvEngineeringQuantityColumn11.Index then
    begin

      if Self.tvEngineeringQuantity.Controller.FocusedRow.IsFirst and IsNewRow then
      begin
        //在最后一行新增
          with Self.ADOQuantity do
          begin
            if State <> dsInactive then
            begin
              First;
              Insert;
              Self.tvEngineeringQuantityColumn2.FocusWithSelection;
            end;
          end;

      end;


    end else
    if AItem.Index = Self.tvEngineeringQuantityColumn8.Index then
    begin
      if AEdit.EditValue = 0 then
      begin
         Self.RzToolButton83.Click;
      end;
    end;

  end;

end;

procedure TfrmProjectSubcontract.tvEngineeringQuantityStylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
var
  szIndex : Integer;
  Value : Variant;
begin
  inherited;
  Value := ARecord.Values[Self.tvQuantityCol2.Index];
  if Value = False then
  begin
    AStyle := DM.cxStyle222;
  end;
end;

procedure TfrmProjectSubcontract.tvEngineeringQuantityTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems1GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
var
  t : Currency;
  szCapital : string;

begin
  inherited;
  try
    szCapital := '';
    if AValue <> null then
    begin
      t := StrToCurr( VarToStr(AValue) );
      szCapital := MoneyConvert(t);
    end else
    begin
      szCapital := '';
    end;
    Self.tvEngineeringQuantityColumn12.Summary.FooterFormat := szCapital;
  except
  end;

end;

procedure TfrmProjectSubcontract.tvQuantityCol1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  AText := IntToStr(ARecord.Index + 1);
end;

procedure TfrmProjectSubcontract.tvQuantityCol2PropertiesChange(
  Sender: TObject);
var
  szRowIndex : Integer;

begin
  inherited;
  szRowIndex := Self.tvEngineeringQuantity.Controller.FocusedRowIndex;
  Self.ADOQuantity.UpdateBatch(arAll);
  Self.ADOQuantity.Requery();
  Self.tvEngineeringQuantity.Controller.FocusedRowIndex := szRowIndex;
end;

end.
