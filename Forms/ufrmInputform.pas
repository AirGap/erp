unit ufrmInputform;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, cxContainer, cxEdit, cxStyles, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator, Data.DB, cxDBData,
  cxTextEdit, cxCheckBox, cxDBLookupComboBox, cxDropDownEdit, cxCurrencyEdit,
  Vcl.ComCtrls, Vcl.DBCtrls, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid,
  Vcl.ExtCtrls, cxCalendar, cxDBEdit, cxMaskEdit, cxLookupEdit, cxDBLookupEdit,
  cxLabel, cxGroupBox, cxDBNavigator, RzButton, RzPanel, Datasnap.DBClient,
  Vcl.Menus, cxSpinEdit,ufrmBaseController, dxmdaset , UtilsTree, UnitADO,
  cxListBox, Vcl.StdCtrls, cxButtons, cxMemo, cxButtonEdit;

type
  TNavgator         = class(TDBNavigator);
  TCusDropDownEdit  = class(TcxCustomDropDownEdit);

  TfrmInputformBase = class(TfrmBaseController )
    RzToolbar4: TRzToolbar;
    RzSpacer26: TRzSpacer;
    RzToolButton23: TRzToolButton;
    RzSpacer31: TRzSpacer;
    RzToolButton24: TRzToolButton;
    RzSpacer32: TRzSpacer;
    RzSpacer33: TRzSpacer;
    RzSpacer1: TRzSpacer;
    RzToolButton1: TRzToolButton;
    RzSpacer2: TRzSpacer;
    RzToolButton2: TRzToolButton;
    RzSpacer3: TRzSpacer;
    RzToolButton3: TRzToolButton;
    RzToolButton4: TRzToolButton;
    RzSpacer4: TRzSpacer;
    RzToolButton5: TRzToolButton;
    RzToolButton6: TRzToolButton;
    RzSpacer5: TRzSpacer;
    RzSpacer6: TRzSpacer;
    RzToolButton7: TRzToolButton;
    RzSpacer7: TRzSpacer;
    RzToolButton8: TRzToolButton;
    cxDBNavigator1: TcxDBNavigator;
    cxGroupBox1: TcxGroupBox;
    cxLabel7: TcxLabel;
    cxLabel1: TcxLabel;
    cxLabel3: TcxLabel;
    cxLabel8: TcxLabel;
    cxLabel5: TcxLabel;
    cxLabel9: TcxLabel;
    cxDBComboBox1: TcxDBComboBox;
    BillNumber: TcxDBTextEdit;
    cxDBDateEdit1: TcxDBDateEdit;
    Bevel3: TBevel;
    cxGroupBox3: TcxGroupBox;
    cxLabel22: TcxLabel;
    cxDBComboBox7: TcxDBComboBox;
    cxLabel23: TcxLabel;
    cxDBTextEdit8: TcxDBTextEdit;
    cxLabel24: TcxLabel;
    cxDBTextEdit9: TcxDBTextEdit;
    cxLabel25: TcxLabel;
    cxDBTextEdit10: TcxDBTextEdit;
    cxLabel26: TcxLabel;
    cxDBTextEdit11: TcxDBTextEdit;
    cxLabel27: TcxLabel;
    cxDBDateEdit4: TcxDBDateEdit;
    cxDBComboBox8: TcxDBComboBox;
    cxLabel28: TcxLabel;
    cxLabel29: TcxLabel;
    cxLabel30: TcxLabel;
    Timer1: TTimer;
    pm: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    Master: TClientDataSet;
    MasterSource: TDataSource;
    DetailedSource: TDataSource;
    Detailed: TClientDataSet;
    N8: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    N11: TMenuItem;
    CompanySource: TDataSource;
    Company: TClientDataSet;
    Print: TPopupMenu;
    N12: TMenuItem;
    cxDBLookupComboBox4: TcxDBLookupComboBox;
    cxDBLookupComboBox5: TcxDBLookupComboBox;
    cxDBButtonEdit1: TcxDBButtonEdit;
    RzPanel5: TRzPanel;
    RzPanel6: TRzPanel;
    cxDBTextEdit7: TcxDBTextEdit;
    RzPanel7: TRzPanel;
    RzPanel8: TRzPanel;
    cxDBCurrencyEdit1: TcxDBCurrencyEdit;
    cxLabel2: TcxLabel;
    cxLabel4: TcxLabel;
    cxLabel6: TcxLabel;
    cxLabel10: TcxLabel;
    cxLabel11: TcxLabel;
    cxLabel12: TcxLabel;
    cxLabel16: TcxLabel;
    dsRuning: TClientDataSet;
    DataSource1: TDataSource;
    Panel2: TPanel;
    cxGroupBox2: TcxGroupBox;
    Grid: TcxGrid;
    tView: TcxGridDBTableView;
    tViewCol1: TcxGridDBColumn;
    tViewCol2: TcxGridDBColumn;
    tViewCol3: TcxGridDBColumn;
    tViewCol4: TcxGridDBColumn;
    tViewCol5: TcxGridDBColumn;
    tViewCol6: TcxGridDBColumn;
    tViewColumn3: TcxGridDBColumn;
    tViewColumn4: TcxGridDBColumn;
    tViewColumn6: TcxGridDBColumn;
    tViewColumn7: TcxGridDBColumn;
    tViewColumn1: TcxGridDBColumn;
    tViewColumn2: TcxGridDBColumn;
    tViewColumn5: TcxGridDBColumn;
    tViewColumn12: TcxGridDBColumn;
    tViewColumn13: TcxGridDBColumn;
    tViewCol7: TcxGridDBColumn;
    tViewColumn10: TcxGridDBColumn;
    tViewCol9: TcxGridDBColumn;
    tViewCol8: TcxGridDBColumn;
    tViewCol15: TcxGridDBColumn;
    tViewCol16: TcxGridDBColumn;
    tViewCol10: TcxGridDBColumn;
    tViewColumn14: TcxGridDBColumn;
    tViewCol11: TcxGridDBColumn;
    tViewCol12: TcxGridDBColumn;
    tViewCol13: TcxGridDBColumn;
    tViewCol14: TcxGridDBColumn;
    tRuning: TcxGridDBTableView;
    tRuningCol9: TcxGridDBColumn;
    tRuningCol1: TcxGridDBColumn;
    tRuningCol12: TcxGridDBColumn;
    tRuningCol2: TcxGridDBColumn;
    tRuningCol3: TcxGridDBColumn;
    tRuningCol4: TcxGridDBColumn;
    tRuningCol11: TcxGridDBColumn;
    tRuningCol5: TcxGridDBColumn;
    tRuningCol6: TcxGridDBColumn;
    tRuningCol7: TcxGridDBColumn;
    tRuningCol8: TcxGridDBColumn;
    tRuningCol10: TcxGridDBColumn;
    Lv: TcxGridLevel;
    Lv2: TcxGridLevel;
    Navigator: TDBNavigator;
    cxDBCheckBox1: TcxDBCheckBox;
    cxDBTextEdit1: TcxDBTextEdit;
    cxDBTextEdit2: TcxDBTextEdit;
    tViewColumn8: TcxGridDBColumn;
    cxDBButtonEdit2: TcxDBButtonEdit;
    N13: TMenuItem;
    N14: TMenuItem;
    dsGroupUnitPrice: TClientDataSet;
    DataGroupUnitPrice: TDataSource;
    Lv1: TcxGridLevel;
    tGroup: TcxGridDBTableView;
    tGroupColumn1: TcxGridDBColumn;
    tGroupColumn2: TcxGridDBColumn;
    tGroupColumn3: TcxGridDBColumn;
    N15: TMenuItem;
    tGroupColumn4: TcxGridDBColumn;
    tViewColumn9: TcxGridDBColumn;
    tViewColumn11: TcxGridDBColumn;
    DBCompany: TcxDBButtonEdit;
    N16: TMenuItem;
    RzPanel1: TRzPanel;
    RzPanel2: TRzPanel;
    cxTextEdit1: TcxTextEdit;
    tViewColumn15: TcxGridDBColumn;
    RzSpacer8: TRzSpacer;
    RzToolButton9: TRzToolButton;
    procedure RzToolButton1Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Timer1Timer(Sender: TObject);
    procedure RzToolButton8Click(Sender: TObject);
    procedure MasterAfterInsert(DataSet: TDataSet);
    procedure RzToolButton7Click(Sender: TObject);
    procedure RzToolButton3Click(Sender: TObject);
    procedure RzToolButton4Click(Sender: TObject);
    procedure RzToolButton5Click(Sender: TObject);
    procedure tViewColumnSizeChanged(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
    procedure tViewEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure tViewEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure DetailedAfterInsert(DataSet: TDataSet);
    procedure N11Click(Sender: TObject);
    procedure tvClick(Sender: TObject);
    procedure cxDBPopupEdit1PropertiesInitPopup(Sender: TObject);
    procedure RzToolButton6Click(Sender: TObject);
    procedure cxDBButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure DBCompanyPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure cxDBComboBox1PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure cxDBLookupComboBox1PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure cxDBButtonEdit1PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure cxDBComboBox7PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure cxDBTextEdit8PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure cxDBTextEdit9PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure cxDBTextEdit10PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure cxDBTextEdit11PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure cxDBDateEdit4PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure cxDBComboBox8PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure cxDBLookupComboBox4PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure cxDBLookupComboBox5PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure tViewCol1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure cxDBComboBox1Enter(Sender: TObject);
    procedure cxDBDateEdit1Enter(Sender: TObject);
    procedure RzToolButton2Click(Sender: TObject);
    procedure RzToolButton23Click(Sender: TObject);
    procedure RzToolButton24Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure tViewCol8PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure tViewCol15PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure tViewCol16PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure tViewCol10PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure tViewColumn10PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure tViewTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure tViewDblClick(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure N6Click(Sender: TObject);
    procedure N7Click(Sender: TObject);
    procedure cxDBComboBox1PropertiesCloseUp(Sender: TObject);
    procedure cxDBButtonEdit2PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure tViewCol4PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure tViewCol6PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure tViewCol7PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure N14Click(Sender: TObject);
    procedure dsGroupUnitPriceAfterInsert(DataSet: TDataSet);
    procedure N15Click(Sender: TObject);
    procedure tGroupEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure DetailedCalcFields(DataSet: TDataSet);
    procedure tViewCol7PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure DBCompanyPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure BarCodeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure N16Click(Sender: TObject);
    procedure cxTextEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RzToolButton9Click(Sender: TObject);
  private
    { Private declarations }
    FOldWndProc:  TWndMethod;
    dwVar_CompanyName  : Variant;
    dwVar_BillCategory : Variant;
    dwVar_DeliverCompany:Variant;
    dwVar_ReceiveCompany:Variant;
    dwVar_AccountsStatus:Variant;
    dwVar_Handman       : Variant;
    dwVar_AuditSignature:Variant;
    dwVar_Recipient:Variant;
    dwVar_Consignor:Variant;
    dwVar_DateSigning:Variant;
    dwVar_BillStatus:Variant;
    dwVar_PayStatus:Variant;
    dwVar_Exacct:Variant;
    dwADO : TADO;
    Radix : Integer;
    procedure SetcxGrid(lpGridView : TcxGridDBTableView ; index : Byte ;lpSuffix:string);
    function IsFormEmpty():Boolean;
    procedure PopupKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
    procedure GetSumMoney(); //Count , UnitPrice , Weight : Variant
    function UpdateInfo():Boolean;
    function GetRadix(lpRadix : Integer):BOOL;
    function RuningAccount(lpFormType : Integer):Boolean;
    procedure WndProc(var message:TMessage);override; //重载窗口消息过程
    procedure ADetailDataControllerCollapsing(ADataController: TcxCustomDataController;
              ARecordIndex:Integer;var AAllow:Boolean);
    procedure  ADetailDataControllerExpanding(
    ADataController:   TcxCustomDataController;   ARecordIndex:   Integer;
    var   AAllow:   Boolean);
    procedure GetUnitPriceMoney(lpCount , lpWeight , lpUnitPrice :  string);
    procedure CalculatData(Sender: TObject; s: string);
    Procedure OnMouseWheel(Var Msg: TMsg; var Handled: Boolean);
    procedure RePinert(lpReportName : string ; lpIndex : Byte = 0);
  public
    { Public declarations }
   // dwModule : Integer;
    KeyList : string;
    odt:integer;
    keys:integer;
    dwDetailSQLText : string;
    dwDetailedTable : string;
    dwDataType : Integer;
    dwNodeText : string;
    dwNodeId   : Integer;
    dwTreeCode : string;
    dwIsEdit   : Boolean;
    dwBillNumber : Variant;
    dwIsContrct: Boolean;
    dwGroupUnitPriceSQLText : string;
  end;

var
  frmInputformBase: TfrmInputformBase;

implementation

uses
   Printers,
   ufrmSerialPort,
   ufrmCompanyInfo,
   ufrmCalcTool,
   ufrmContrctList,
   ufrmContrctGoodsList,
   ufrmGroupUnitPrice,
   ufrmMakings,
   System.Math,
   global,
   ufrmIsViewGrid,
   uDataModule,
   FillThrdTree,
   ufrmStorageTreeView,
   ufrmReport,
   ufrmCompanyManage;

{$R *.dfm}

{
h:=CreateThread(nil,0,@myfunction,nil,0,hID);
if WaitForSingleObject(h,6000) <> WAIT_TIMEOUT then
begin

end;
}

//小票开始
const
  // 末尾走纸几行
  c_run_paper_lines = 6;
  // ESC指令 开钱箱
  c_OpenMoneyBoxCommand = CHR(27) + CHR(112) + CHR(0) + CHR(17) + CHR(8);
  // ESC指令 自动切纸
  c_cut_paper = CHR(29) + CHR(86) + CHR(66) + CHR(0);

type // usb接口票打用
  TOutBufPassThrough = record
  nDataLen: word;
  sEscData: array [0 .. 1024] of AnsiChar;
  end;

// usb接口开钱箱
procedure OpenUSBMoneyBox;
var
  prt: TPrinter;
  esc: TOutBufPassThrough;
begin
  try
    prt := Printers.Printer;
    prt.beginDoc;
    esc.nDataLen := Length(c_OpenMoneyBoxCommand);
    strpCopy(esc.sEscData, c_OpenMoneyBoxCommand);
    ExtEscape(prt.Handle, PASSTHROUGH, sizeOf(esc), @esc, 0, nil);
    prt.endDoc;
  except
  end;
end;

// usb接口切纸
procedure usbCutPaper;
var
  prt: TPrinter;
  esc: TOutBufPassThrough;
begin
  try
    prt := Printers.Printer;
    prt.beginDoc;
    esc.nDataLen := Length(c_cut_paper);
    strpCopy(esc.sEscData, c_cut_paper);
    ExtEscape(prt.Handle, PASSTHROUGH, sizeOf(esc), @esc, 0, nil);
    prt.endDoc;
  except

  end;
end;
//小票结束

procedure TfrmInputformBase.ADetailDataControllerCollapsing(ADataController: TcxCustomDataController;
ARecordIndex:Integer;var AAllow:Boolean);
var
  I:   Integer;
  C:   Integer;
begin
  AAllow   :=   False;
  C   :=   0;
  for   I   :=   0   to   ADataController.RecordCount   -   1   do
  begin
    if   ADataController.GetDetailExpanding(I)   then
        Inc(C);
    if   C   >   1   then
        AAllow   :=   True;
  end;
end;

procedure TfrmInputformBase.ADetailDataControllerExpanding(
    ADataController:   TcxCustomDataController;   ARecordIndex:   Integer;
    var   AAllow:   Boolean);
begin
  ADataController.CollapseDetails;
end;

procedure TfrmInputformBase.BarCodeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = 13 then
  begin
    ShowMessage( (Sender as TEdit).Text );
  end;
end;

procedure TfrmInputformBase.OnMouseWheel(var Msg: TMsg; var Handled: Boolean);
begin
   if Msg.message = WM_MOUSEWHEEL then
   begin
     Handled := ActiveControl = cxDBComboBox1.InnerControl;
   end;
end;

procedure TfrmInputformBase.WndProc(var Message: TMessage);
var
  pMsg : PMakings;

begin
  case Message.Msg of
    WM_LisView :
      begin
        pMsg := PMakings(message.LParam);
        if Assigned(pMsg) then
        begin
          tViewCol4.EditValue    := pMsg.dwMakingCaption;
          tViewCol5.EditValue    := pMsg.dwModelIndex;
          tViewCol6.EditValue    := pMsg.dwSpec;
          tViewColumn4.EditValue := pMsg.dwBrand;
          tViewColumn3.EditValue := pMsg.dwManufactor; //厂家
          tViewColumn6.EditValue := pMsg.dwNoColour;   //颜色
          tViewColumn7.EditValue := pMsg.dwTexture;
          tViewColumn1.EditValue := pMsg.dwNoType;
          tViewColumn2.EditValue := pMsg.dwNoNumber;

          if not dwIsContrct then
          begin
            if (dwDataType = 0) or (dwDataType = 4) then
               tViewCol8.EditValue  := pMsg.dwBuyingPrice;  //单价

            if (dwDataType = 1) or (dwDataType = 2) then
              tViewCol8.EditValue    := pMsg.dwUnitPrice;  //单价

            tViewColumn11.EditValue:= pMsg.dwUnitPrice;
            tViewCol16.EditValue   := pMsg.dwUnitPrice;

          end else
          begin
          //  if (dwDataType = 0) or (dwDataType = 4) then
             tViewCol8.EditValue  := pMsg.dwUnitPrice;  //单价
          end;

          //ShowMessage( CurrToStr(pMsg.dwUnitPrice) + ' dwDataType:' +
          //IntToStr(dwDataType)  + ' Is:' + BoolToStr(dwIsContrct,True));
          //入库和退货的那个单价对接进价
          //出库和退还对接售价

        end;
      end;
    WM_ViewNum :
      begin
        pMsg := PMakings(message.LParam);
        if Assigned(pMsg) then
        begin

        end;
      end;
  end;
      // 一定要加上这句，否则编译通不过。因为绝大部分消息没人处理了
  inherited WndProc(Message); // 会一路向上调用，直到TControl.WndProc调用Dispatch来寻找消息处理函数
end;

function IsDetailed(lpDataSet : TClientDataSet):Boolean;
begin
  with lpDataSet do
  begin
    if IsEmpty then
    begin
      Append ;
    end else
    begin
      First;
      Insert;
    end;
  end;
end;

function TfrmInputformBase.GetRadix(lpRadix : Integer):BOOL;
begin
  case lpRadix of
    0: Radix := -1;
    1: Radix := -2;
    2: Radix := -3;
    3: Radix := -4;
    4: Radix := -5;
    5: Radix := -6;
    6: Radix := -7;
    7: Radix := -8;
  end;

end;

procedure TfrmInputformBase.GetSumMoney();
var
  szSumMoney : Currency;
  szColName : string;
  szValue : Currency;
  s : string;
  szDisCount  : Double;
  szDisPrice  : Currency;
  szCount     : Double;
  szUnitPrice : Currency;
  szWeight    : Double;
begin
  inherited;
  szDisCount  := StrToCurrDef( VarToStr( tViewCol15.EditValue )   , 0); //折扣
  szCount     := StrToCurrDef( VarToStr( tViewCol7.EditValue )    , 0 );
  szUnitPrice := StrToCurrDef( VarToStr( tViewCol8.EditValue )    , 0 );
  szWeight    := StrToCurrDef( VarToStr( tViewColumn10.EditValue) , 0);

  szDisPrice  := szDisCount * szUnitPrice; //折扣后单价
  tViewCol16.EditValue := szDisPrice;
  {
  ShowMessage(' Count:' + FloatToStr(szCount) +
              ' UnitPrice:'+ FloatToStr(szUnitPrice) +
              ' Weight:' + FloatToStr(szWeight)  +
              ' DisCount:'+FloatToStr(szDisCount)+
              ' DisPrice:'+FloatToStr(szDisPrice)
              );
  }

  {
  if self.Master.State in [dsEdit,dsInsert] then Self.Master.Edit;
  szSumMoney := StrToCurrDef(  VarToStr( UnitPrice ),0) *
                StrToFloatDef( VarToStr( Count ) ,0 ) *
                StrToFloatDef( VarToStr( Weight ) ,1 );
  }
  szSumMoney := szDisPrice * szCount * szWeight;
  Self.tViewCol10.EditValue := szSumMoney;
  Self.cxDBCurrencyEdit1.Clear;
  Self.cxDBTextEdit7.Clear;
  {
  s := VarToStr(Self.tView.DataController.Summary.FooterSummaryValues[0]);
  szValue := StrToCurrDef(s,0);
  }
  Self.tViewColumn14.EditValue := MoneyConvert( szSumMoney );
end;

function TfrmInputformBase.UpdateInfo():Boolean;
var
  i : Integer;
  s : Variant;
begin
  inherited;
  //更新明细主要信息
  with Self.Detailed do
  begin
    First;
    while Not Eof do
    begin
      Edit;
        FieldByName(Self.cxDBDateEdit1.DataBinding.DataField).Value    := Self.cxDBDateEdit1.EditingValue;
        FieldByName(Self.cxDBButtonEdit2.DataBinding.DataField).Value  := Self.cxDBButtonEdit2.EditingValue;
        FieldByName(Self.cxDBButtonEdit1.DataBinding.DataField).Value  := Self.cxDBButtonEdit1.EditingValue;
        FieldByName(Self.cxDBComboBox7.DataBinding.DataField).Value    := Self.cxDBComboBox7.EditingValue;
        FieldByName(Self.tViewCol3.DataBinding.FieldName).Value        := Self.BillNumber.EditingValue;
        FieldByName(Self.cxDBTextEdit1.DataBinding.DataField).Value    := Self.cxDBTextEdit1.EditingValue;
        FieldByName(Self.cxDBTextEdit2.DataBinding.DataField).Value    := Self.cxDBTextEdit2.EditingValue;
      Post;
      Next;
    end;
  end;
end;

procedure TfrmInputformBase.PopupKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = 27 then //如果按下“ESC”键
    TcxCustomEditPopupWindow(sender).ClosePopup;//关闭弹出窗
end;

procedure TfrmInputformBase.SetcxGrid(lpGridView : TcxGridDBTableView ; index : Byte ;
lpSuffix:string);
var
  i : Integer;
begin
  for I := 0 to lpGridView.ColumnCount - 1 do
  begin
    Application.ProcessMessages;
    SetGrid(lpGridView.Columns[i],index, lpSuffix);
  end;
end;

procedure TfrmInputformBase.tGroupEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  inherited;
  AAllow := False;
end;

procedure TfrmInputformBase.cxButton4Click(Sender: TObject);
begin
  inherited;
  Self.tViewCol7.Editing := False;
end;

procedure TfrmInputformBase.cxButton5Click(Sender: TObject);
begin
  inherited;
  Self.tViewCol7.Editing := False;
end;

procedure TfrmInputformBase.cxDBButtonEdit1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  szParent : Integer;
  lvMsg : string;

begin
  inherited;
  szParent := 0;
  with Self.Company do
  begin
    if State <> dsInactive then
    begin
      if Locate('SignName',DBCompany.Text,[]) then
      begin
        szParent := FieldByName('parent').Value;
      end;
    end;
  end;
  if (dwDataType = 0) or (dwDataType = 2) then
  begin
    //入库  退还
    lvMsg :=  Copy( cxLabel7.Caption , 0 , Length(cxLabel7.Caption)-1);
  end;

  if (dwDataType = 4) or (dwDataType = 1) then
  begin
    //出库 退货
    lvMsg :=  Copy( cxLabel1.Caption , 0 , Length(cxLabel1.Caption)-1);
  end;

  if szParent <> 0 then
  begin
    Application.CreateForm(TfrmStorageTreeView,frmStorageTreeView);
    try
      frmStorageTreeView.dwTableName := g_Table_Company_StorageTree;
      frmStorageTreeView.dwParent    := szParent;
      frmStorageTreeView.dwTreeName  := lvMsg;
      frmStorageTreeView.ShowModal;

      cxDBButtonEdit1.EditValue := frmStorageTreeView.dwStorageName;
      cxDBButtonEdit1.PostEditValue;

      dwNodeId   := frmStorageTreeView.dwNodeId;
      dwTreeCode := frmStorageTreeView.dwTreeCode;
      Self.cxDBTextEdit2.EditValue := dwTreeCode;
      Self.cxDBTextEdit2.PostEditValue;
      Self.cxDBTextEdit1.EditValue := dwNodeId;
      Self.cxDBTextEdit1.PostEditValue;
    finally
      frmStorageTreeView.Free;
    end;
  end else
  begin
    lvMsg := '重新选择' + lvMsg;
    Application.MessageBox(PWideChar( lvMsg ), 'Error', MB_OKCANCEL + MB_ICONWARNING);
  end;

end;

procedure TfrmInputformBase.cxDBButtonEdit1PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
begin
  inherited;
  dwVar_ReceiveCompany := DisplayValue;
end;

procedure TfrmInputformBase.cxDBButtonEdit2PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  Child: TfrmCompanyManage;
  s : Variant;
begin
  inherited;
  if not dwIsContrct then
  begin
    Child := TfrmCompanyManage.Create(nil);
    try
      Child.dwIsReadonly := True;
      Child.ShowModal;
      s := Child.dwSignName;
      if s <> null then
      begin
        (Sender as TcxDBButtonEdit).EditValue := s;
        (Sender as TcxDBButtonEdit).PostEditValue;
      end;
    finally
      Child.Free;
    end;
  end else
  begin
    Application.CreateForm(TfrmContrctList,frmContrctList);
    try
      frmContrctList.dwModuleIndex := dwDataType;
      frmContrctList.ShowModal;
      s := frmContrctList.dwCompanyName;
      if s <> null then
      begin
        (Sender as TcxDBButtonEdit).EditValue := s;
        (Sender as TcxDBButtonEdit).PostEditValue;
      end;
    finally
      frmContrctList.Free;
    end;
  end;
end;

procedure TfrmInputformBase.cxDBComboBox1Enter(Sender: TObject);
begin
  inherited;
  (Sender as TcxDBComboBox).DroppedDown := True;
end;

procedure TfrmInputformBase.cxDBComboBox1PropertiesCloseUp(Sender: TObject);
var
  szIndex : Integer;
begin
  inherited;
  szIndex := (Sender as TcxDBComboBox).ItemIndex;
  Self.cxDBComboBox7.ItemIndex := szIndex; //交易状态
  if szIndex = 1 then
  begin
    Self.cxDBLookupComboBox4.Enabled := True;
    Self.cxDBLookupComboBox5.Enabled := True;
    if (dwDataType = 0) or (dwDataType = 1) then
       Self.Grid.RootLevelOptions.DetailTabsPosition := dtpTop;
  end else
  begin
    Self.cxDBLookupComboBox4.Enabled := False;
    Self.cxDBLookupComboBox5.Enabled := False;
    Self.Grid.RootLevelOptions.DetailTabsPosition := dtpNone;
  end;
end;

procedure TfrmInputformBase.cxDBComboBox1PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
begin
  inherited;
  dwVar_BillCategory := DisplayValue;
end;

procedure TfrmInputformBase.cxDBComboBox7PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
begin
  inherited;
  dwVar_AccountsStatus := DisplayValue;
end;

procedure TfrmInputformBase.cxDBComboBox8PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
begin
  inherited;
  dwVar_BillStatus := DisplayValue;
end;

procedure TfrmInputformBase.cxDBDateEdit1Enter(Sender: TObject);
begin
  inherited;
  (Sender AS TcxDBDateEdit).DroppedDown := True;
end;

procedure TfrmInputformBase.cxDBDateEdit4PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
begin
  inherited;
  dwVar_DateSigning := DisplayValue;
end;

procedure TfrmInputformBase.cxDBLookupComboBox1PropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  inherited;
  dwVar_DeliverCompany := DisplayValue;
end;

procedure TfrmInputformBase.cxDBLookupComboBox4PropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  inherited;
  dwVar_PayStatus := DisplayValue;
end;

procedure TfrmInputformBase.cxDBLookupComboBox5PropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  inherited;
  dwVar_Exacct := DisplayValue;
end;

procedure TfrmInputformBase.cxDBPopupEdit1PropertiesInitPopup(Sender: TObject);
begin
  inherited;
  TCusDropDownEdit(sender).PopupWindow.KeyPreview := true;//令弹出窗按键事件有效
  TCusDropDownEdit(sender).PopupWindow.OnKeyDown  := PopupKeyDown;//关联按键事件给弹出窗
end;

procedure TfrmInputformBase.cxDBTextEdit10PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
begin
  inherited;
  dwVar_Recipient := DisplayValue;
end;

procedure TfrmInputformBase.cxDBTextEdit11PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
begin
  inherited;
  dwVar_Consignor := DisplayValue;
end;

procedure TfrmInputformBase.cxDBTextEdit8PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
begin
  inherited;
  dwVar_Handman := DisplayValue;
end;

procedure TfrmInputformBase.cxDBTextEdit9PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
begin
  inherited;
  dwVar_AuditSignature := DisplayValue;
end;

procedure TfrmInputformBase.cxTextEdit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);

  //修改表现值
  procedure SetGridValue(lpGrid : TcxGridDBTableView ;
                         lpBarCode,
                         lpMakingCaption ,
                         lpModelIndex ,
                         lpSpec ,
                         lpBrand,
                         lpManufactor ,
                         lpNoColour ,
                         lpUnitPrice : Variant );
  var
    i:Integer;
    lvCountFieldName : string;
    lvCount:Variant;
    lvBarCode : Variant;
    lvIsExists: Boolean;
  begin
    lvCountFieldName := tViewCol7.DataBinding.FieldName;
    with Self.Detailed do
    begin
      lvIsExists := False;
      if State <> dsInactive then
      begin
        First;
        while not Eof do
        begin
          lvIsExists := False;
          if FieldByName(tViewColumn15.DataBinding.FieldName).Value = lpBarCode then
          begin
            Edit;
            lvCount := FieldByName(lvCountFieldName).Value;
            Inc(lvCount);
            FieldByName(lvCountFieldName).Value := lvCount;
            lvIsExists := True;
            Post;
            GetSumMoney(); //计算总价
            Last;
            Break;
          end;
          Next;
        end;

      end;

      if not lvIsExists then
      begin
        Append;

        FieldByName(tViewCol4.DataBinding.FieldName).Value := lpMakingCaption;
        FieldByName(tViewCol5.DataBinding.FieldName).Value := lpModelIndex;
        FieldByName(tViewCol6.DataBinding.FieldName).Value := lpSpec;
        FieldByName(tViewCol7.DataBinding.FieldName).Value := 1;
        FieldByName(tViewCol8.DataBinding.FieldName).Value := lpUnitPrice; //单价

        FieldByName(tViewColumn3.DataBinding.FieldName).Value := lpManufactor; //厂家
        FieldByName(tViewColumn4.DataBinding.FieldName).Value := lpBrand; //品牌
        FieldByName(tViewColumn6.DataBinding.FieldName).Value := lpNoColour; //颜色
        FieldByName(tViewColumn15.DataBinding.FieldName).Value:= lpBarCode;//条码

        GetSumMoney(); //计算总价
        Post;
      end;
    end;
    
  end;

var
  s : string;
  i : Integer;
begin
  inherited;
  s := (Sender as TcxTextEdit).Text;
  if (Key = 13) and (Length(s) = 13) then
  begin

    if Not IsFormEmpty then
    begin

      with DM.Qry do
      begin
        Close;
        SQL.Text := 'Select * from ' + g_Table_MakingsList + ' WHERE BarCode="'+ s +'"';
        Open;
        if RecordCount <> 0 then
        begin
          SetGridValue(Self.tView,
          FieldByName('BarCode').Value,
          FieldByName('MakingCaption').Value,
          FieldByName('ModelIndex').Value,
          FieldByName('spec').Value,
          FieldByName('Brand').Value,
          FieldByName('Manufactor').Value,
          FieldByName('NoColour').Value,
        //  FieldByName('BuyingPrice').Value,
          FieldByName('UnitPrice').Value
           )
          //FieldByName('MakingCaption').AsString
          //ModelIndex  //型号
          //spec        //规格
          //Brand       //品牌
          //Manufactor  //厂家
          //NoColour    //颜色
          //BuyingPrice //进价
          //UnitPrice   //售价
        end else
        begin
          Application.MessageBox( '您输入的条形码没有查到记录！', '条形码：', MB_OKCANCEL + MB_ICONWARNING);
        end;
      end;

    end;
    (Sender as TcxTextEdit).Text := '';
  end else
  begin
    if Length(s) > 13 then
    begin
       Application.MessageBox('不是13位条形码','',mb_ok);
      (Sender as TcxTextEdit).Text := '';
    end;

  end;

end;

procedure TfrmInputformBase.DBCompanyPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
var
  szCompanyName : string;

begin
  inherited;
  Application.CreateForm(TfrmCompanyInfo,frmCompanyInfo);
  try
    frmCompanyInfo.dwTableName := g_Table_Company_StorageTree;
    frmCompanyInfo.ShowModal;
    szCompanyName := frmCompanyInfo.dwCompanyName;
    if szCompanyName <> '' then
    begin
      (Sender as TcxDBButtonEdit).Text := frmCompanyInfo.dwCompanyName;
      (Sender as TcxDBButtonEdit).PostEditValue;
      Self.cxDBDateEdit1.Enabled   := True;
      Self.cxDBDateEdit4.Enabled   := True;
      Self.cxDBTextEdit8.Enabled   := True;
      Self.cxDBTextEdit9.Enabled   := True;
      Self.cxDBTextEdit10.Enabled  := True;
      Self.cxDBTextEdit11.Enabled  := True;
      Self.cxDBComboBox1.Enabled   := True;
      Self.cxDBComboBox8.Enabled   := True;
      Self.cxDBButtonEdit2.Enabled := True;
      Self.cxDBButtonEdit1.Enabled := True;
      //读取公司
      dwADO.OpenSQL(Self.Company, 'Select * from ' + g_Table_Company_StorageTree + ' Where PID=0'); //读取公司
    end;

  finally
    frmCompanyInfo.Free;
  end;
end;

procedure TfrmInputformBase.DBCompanyPropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
begin
  inherited;
  dwVar_CompanyName := VarToStr(DisplayValue);
end;

procedure TfrmInputformBase.DetailedAfterInsert(DataSet: TDataSet);
var
  Suffix : string;
  szConName : string;
  szCode : string;

begin
  Suffix := '000000';
  szConName := tViewColumn8.DataBinding.FieldName;
  szCode := GetRowCode(g_Table_Company_StorageDetailed,szConName,Suffix,970002);
  with DataSet do
  begin
    FieldByName('IsContrct').Value := dwIsContrct;
    FieldByName(szConName).Value:= szCode;
    FieldByName('DataType').Value  := dwDataType;
    FieldByName(cxDBTextEdit2.DataBinding.DataField).Value := cxDBTextEdit2.EditValue;
    FieldByName(Self.tViewCol2.DataBinding.FieldName).Value:= Self.cxDBCheckBox1.EditValue;
    FieldByName(tViewCol10.DataBinding.FieldName).Value := 0;
    FieldByName(tViewCol7.DataBinding.FieldName).Value  := 0;   //数量
    FieldByName(tViewCol8.DataBinding.FieldName).Value  := 0;   //单价
    FieldByName(tViewCol15.DataBinding.FieldName).Value := 1;
    FieldByName(tViewCol16.DataBinding.FieldName).Value := 0;
    FieldByName(tViewColumn9.DataBinding.FieldName).Value  := 0;
    FieldByName(tViewColumn11.DataBinding.FieldName).Value := 0;
    FieldByName(tViewColumn13.DataBinding.FieldName).Value := 1;
    FieldByName(tViewColumn10.DataBinding.FieldName).Value := 1;
  end;
end;

procedure TfrmInputformBase.DetailedCalcFields(DataSet: TDataSet);
begin
  inherited;
//  Inc(Calc);
//  DataSet[tViewCol10.DataBinding.FieldName] := DataSet[tViewCol7.DataBinding.FieldName] * DataSet[tViewCol8.DataBinding.FieldName];
  {
  if DataSet.State = dsInternalCalc then
  begin
    Inc(InternalCalc);
    DataSet['MUL'] := DataSet['num1'] * DataSet['num2'];
  end;
  }
end;

procedure TfrmInputformBase.dsGroupUnitPriceAfterInsert(DataSet: TDataSet);
var
  szCode : string;
  szColName : string;
begin
  inherited;
  with DataSet do
  begin
    szColName := Self.tGroupColumn4.DataBinding.FieldName;
    szCode := GetRowCode(g_Table_Company_Storage_GroupUnitPrice,szColName,'',90009);

    FieldByName('ParentId').Value := Self.tViewColumn8.EditValue;
    FieldByName('BillNumber').Value := BillNumber.EditingValue;
    FieldByName('Code').Value := dwTreeCode;

    Self.tGroupColumn4.EditValue := szCode;
  end;
end;

procedure TfrmInputformBase.FormCreate(Sender: TObject);
begin
  inherited;
  Application.OnMessage := OnMouseWheel;
end;

procedure TfrmInputformBase.FormDestroy(Sender: TObject);
begin
  inherited;
  Application.OnMessage := nil;
end;

procedure TfrmInputformBase.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 113 then  //F2 打印小票
  begin
    Self.RzToolButton9.Click;
  end;

  if Key = 27 then Close;

  if (Key = 48) or
     (Key = 49) or
     (Key = 50) or
     (Key = 51) or
     (Key = 52) or
     (Key = 53) or
     (Key = 54) or
     (Key = 55) or
     (Key = 56) or
     (Key = 57)   then
  begin
    KeyList := KeyList + Char(Key);
  //  Self.mmo1.Lines.Add(Char(Key));
    {
    inc(keys);
    if keys = 2 then
    begin
      keys := 0;
      if gettickcount-odt < 100 then //0.1秒以上为键盘,自己改一下
      //   Self.mmo1.Lines.Add('其它输入:' + IntToStr(Key));
    end else
      odt:=gettickcount;
    }
  end else
  if Key = 13 then
  begin
  //  Self.cxTextEdit1.SetFocus;
  end;

end;

procedure TfrmInputformBase.MasterAfterInsert(DataSet: TDataSet);
var
  szConName : string;
  Prefix : string;
  Suffix : string;
  szCode : string;
  szSignName : Variant;
begin
  inherited;
//  Prefix := 'ST-';
  Suffix := '000000';
  szConName := Self.BillNumber.DataBinding.DataField;
  szCode := Prefix + GetRowCode(g_Table_Company_StorageBillList,szConName,Suffix,970001);
  with DataSet do
  begin
    FieldByName(Self.BillNumber.DataBinding.DataField).Value := szCode;
    FieldByName(Self.cxDBDateEdit1.DataBinding.DataField).Value := Date;
    FieldByName(Self.cxDBCheckBox1.DataBinding.DataField).Value := True;
    FieldByName('IsContrct').Value := dwIsContrct;
    FieldByName('DataType').Value  := dwDataType;
  end;
  Self.DBCompany.Enabled := True;
end;

procedure TfrmInputformBase.N11Click(Sender: TObject);
var
  szDir , szNumbers : string;
begin
  inherited;
  szNumbers := Self.tViewCol3.EditValue;
  szDir := Concat(g_Resources, '\'+ g_DirBuildStorage);
  CreateOpenDir(Handle,Concat(szDir,'\' + szNumbers),True);
//  CreateEnclosure(Self.tv1.DataController.DataSource.DataSet,Handle,szDir);
end;

procedure TfrmInputformBase.N14Click(Sender: TObject);
begin
  inherited;
  IsDetailed(Self.Detailed);
  Self.Grid.SetFocus;
  Self.tViewCol4.Editing := True;
end;

procedure TfrmInputformBase.N15Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton7.Click;
end;

procedure TfrmInputformBase.N16Click(Sender: TObject);
begin
  inherited;
  if not IsFormEmpty then
  begin
    {
    with Self.Detailed do
    begin
      if State in [dsEdit , dsInsert] then else begin
        Append;
        Post;
      end;
    end;
    }
    Self.cxTextEdit1.SetFocus;
  end;
end;

procedure TfrmInputformBase.N1Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton1.Click;
end;

procedure TfrmInputformBase.N2Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton6.Click;
end;

procedure TfrmInputformBase.N3Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton2.Click;
end;

procedure TfrmInputformBase.N4Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton23.Click;
end;

procedure TfrmInputformBase.N6Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton4.Click;
end;

procedure TfrmInputformBase.N7Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton5.Click;
end;

function TfrmInputformBase.IsFormEmpty():Boolean;
var
  s : PWideChar;
  s1: PWideChar;
  s2: PWideChar;
  lvMsg : string;
  lvC : string;

begin
  Result := False;
  s := '新增商品记录？';
  if Self.BillNumber.EditValue = null then
  begin
    //单号不能为空
    Result := True;
    Application.MessageBox( '单号不能为空！', s, MB_OKCANCEL + MB_ICONWARNING);
    Exit;
  end else
  if Self.DBCompany.EditingValue = null then
  begin
    //公司名不能为空
    Result := True;
    lvMsg  := Copy(cxLabel3.Caption,0,Length(cxLabel3.Caption) - 1) + '不能为空！' ;
    Application.MessageBox(PWideChar( lvmsg ), s, MB_OKCANCEL + MB_ICONWARNING);
    Exit;
  end else
  if Self.cxDBDateEdit1.EditingValue = null then
  begin
    //开票日期不能为空
    Result := True;
    Application.MessageBox( '开票日期不能为空！', s, MB_OKCANCEL + MB_ICONWARNING);
    Self.cxDBDateEdit1.DroppedDown := True;
    Exit;
  end else
  if Self.cxDBComboBox1.EditingValue = null then
  begin
    //结算状态不能为空
    Result := True;
    Application.MessageBox( '请选择票据类别！', s, MB_OKCANCEL + MB_ICONWARNING);
    Self.cxDBComboBox1.DroppedDown := True;
    Exit;
  end else
  if Not Self.cxDBCheckBox1.Checked then
  begin
    Result := True;
    Application.MessageBox( '冻结状态，不能编辑和新增商品！', s, MB_OKCANCEL + MB_ICONWARNING);
    Exit;
  end;
  lvMsg := '不能为空!';
  if (dwDataType = 0) or (dwDataType = 2) then
  begin
    //入库  退还
    s1 := PWideChar( Copy( cxLabel1.Caption , 0 , Length(cxLabel1.Caption)-1) + lvMsg );
    s2 := PWideChar( Copy( cxLabel7.Caption , 0 , Length(cxLabel7.Caption)-1) + lvMsg );
  end;

  if (dwDataType = 4) or (dwDataType = 1) then
  begin
    //出库 退货
    s1 := PWideChar( Copy( cxLabel7.Caption , 0 , Length(cxLabel7.Caption)-1) + lvMsg );
    s2 := PWideChar( Copy( cxLabel1.Caption , 0 , Length(cxLabel1.Caption)-1) + lvMsg );
  end;

  {
  0 = 入库
  4 = 退货
  1 = 出库
  2 = 退还


  case dwDataType of
    0:
    begin
      //入库
      s1 := '发货单位不能为空！';
      s2 := '收货仓库不能为空！';
    end;
    1..2:
    begin
      s1 := '收货单位不能为空！';
      s2 := '发货单位不能为空！';
    end;  
  end;
  }
  if Self.cxDBButtonEdit2.Text = '' then
  begin
    //发货单位不能为空
    Result := True;
    Application.MessageBox(s1, s, MB_OKCANCEL + MB_ICONWARNING);
    Exit;
  end else
  if Self.cxDBButtonEdit1.Text = '' then
  begin
    //收货单位不能为空
    Result := True;
    Application.MessageBox(s2, s, MB_OKCANCEL + MB_ICONWARNING);
    Exit;
  end;

  if Self.cxDBComboBox7.ItemIndex = 1 then //交易状态
  begin
    //现金入库
    if Length(Self.cxDBLookupComboBox4.Text) = 0 then
    begin
      Result := True;
      Application.MessageBox( '请选择一个交易方式', '现金保存', MB_OKCANCEL + MB_ICONWARNING);
      Self.cxDBLookupComboBox4.DroppedDown := True;
      Exit;
    end else
    if Length(Self.cxDBLookupComboBox5.Text) = 0 then
    begin
      Result := True;
      Application.MessageBox( '请选择一个费用科目', '现金保存', MB_OKCANCEL + MB_ICONWARNING);
      Self.cxDBLookupComboBox5.DroppedDown := True;
    end;

  end;

end;

procedure TfrmInputformBase.RzToolButton1Click(Sender: TObject);
begin
  if dwIsEdit then
  begin
    IsDetailed(Self.Detailed);
    Self.Grid.SetFocus;
    Self.tViewCol4.Editing := True;
  end else
  begin
    if Navigator.DataSource.State in [ dsEdit , dsInsert] then
    begin
       IsDetailed(Self.Detailed);
       Self.Grid.SetFocus;
       Self.tViewCol4.Editing := True;
    end else
    begin
      TNavgator(Navigator).Buttons[nbInsert].Click;
      Self.DBCompany.SetFocus;
    end;
  end;
end;

procedure TfrmInputformBase.RzToolButton23Click(Sender: TObject);
var
  ADetailDC: TcxGridDataController;
  AView: TcxCustomGridTableView;
  s : string;
  szSQLText : string;
  szRecordCount : Integer;
  szCount , ErrorCount : Integer;

begin
  inherited;
  szCount := Self.tView.Controller.SelectedRowCount;
  if szCount > 0 then
  begin
    if Application.MessageBox(PWideChar( '您确定要删除所有选中的记录吗？' ), '删除记录？', MB_OKCANCEL + MB_ICONWARNING) = IDOK then
    begin
      //主表删除
      s := GetTableSelectedId(Self.tView,tViewColumn8.Index);
      DM.ADOconn.BeginTrans; //开始事务
      try
        if not dwIsEdit then
        begin
          if dwIsContrct then
          begin
            with tView.DataController do
            begin
              ADetailDC := TcxGridDataController(GetDetailDataController(FocusedRecordIndex, 0));
              ADetailDC.SelectAll;
              ADetailDC.DeleteSelection;
            end;
          end;
          Self.tView.Controller.DeleteSelection;
        end else
        begin
          szSQLText := 'delete * from ' +  g_Table_Company_Storage_GroupUnitPrice  +' where ParentId in("' + s + '")';
          dwADO.ExecSQL(szSQLText);
          szSQLText := 'delete * from ' +  g_Table_Company_StorageDetailed +
             ' where '+ tViewColumn8.DataBinding.FieldName +
             ' in("' + s + '")';
          dwADO.ExecSQL(szSQLText);
          Self.Timer1.Enabled := True;
        end;
        DM.ADOconn.Committrans; //提交事务
      except
        on E:Exception do
        begin
          DM.ADOconn.RollbackTrans;           // 事务回滚
        //  err:=E.Message;
        //  ShowMessage(err);
        end;
      end;
    end;

  end;

end;

procedure TfrmInputformBase.RePinert(lpReportName : string ; lpIndex : Byte = 0);
var
  lvReport : TfrmReport;
  s : string;
begin
  inherited;
  s := Self.cxDBComboBox1.Text;
  lvReport := TfrmReport.Create(Application);
  try
    with lvReport.dwDictionary do
    begin
      Clear;
      Add('名称'    , Self.DBCompany.Text);

      Add('经手人'  , Self.cxDBTextEdit8.Text);
      Add('签收人'  , Self.cxDBTextEdit10.Text);
      Add('发货人'  , Self.cxDBTextEdit11.Text);
      Add('单据编号', Self.BillNumber.Text);
      Add('票据状态', Self.cxDBComboBox8.Text);
      Add('票据类别', Self.cxDBComboBox1.Text);
      Add('交易方式', Self.cxDBLookupComboBox4.Text);

      Add(Self.cxDBButtonEdit2.Hint, Self.cxDBButtonEdit2.Text);
      Add(Self.cxDBButtonEdit1.Hint, Self.cxDBButtonEdit1.Text);

    //  ShowMessage(Self.cxDBButtonEdit2.Hint + ' - ' + Self.cxDBButtonEdit2.Text);
    //  ShowMessage(Self.cxDBButtonEdit1.Hint + ' - ' + Self.cxDBButtonEdit1.Text);

      Add('开票日期', Self.cxDBDateEdit1.Text);
      Add('审核签字', Self.cxDBTextEdit9.Text);
      Add('签收日期', Self.cxDBDateEdit4.Text);
      Add('Title',s);
    end;
    lvReport.frxDBDataset.DataSource := Self.DetailedSource;
    lvReport.frxDBDataset.UserName   := '进销管理';
    lvReport.dwIsReportTitle := True; //是否输出标题
    lvReport.dwReportName    := lpReportName;
    lvReport.dwRecordCount   := Self.Detailed.RecordCount;
  //  Report.dwReportFile := '进销管理-标准模板'; //报表文件
  //  lvReport.dwReportTitle := s; //报表标题

    if lpIndex = 1 then
    begin
      lvReport.Show;
      lvReport.Hide;
      lvReport.dwIsReportTitle := False;
      lvReport.cxButton1.Click;
      lvReport.Close;
    end else
    begin
      lvReport.ShowModal;
    end;
  finally
  end;
end;

procedure TfrmInputformBase.RzToolButton24Click(Sender: TObject);
{
var
  Report : TfrmReport;
  s : string;
}
begin
  inherited;
  RePinert('标准模板');
  {
  s := Self.cxDBComboBox1.Text;
  Report := TfrmReport.Create(Application);
  try
    with Report.dwDictionary do
    begin
      Clear;
      Add('名称'    , Self.DBCompany.Text);

      Add('经手人'  , Self.cxDBTextEdit8.Text);
      Add('签收人'  , Self.cxDBTextEdit10.Text);
      Add('发货人'  , Self.cxDBTextEdit11.Text);
      Add('单据编号', Self.BillNumber.Text);
      Add('票据状态', Self.cxDBComboBox8.Text);
      Add('交易方式', Self.cxDBLookupComboBox4.Text);
      Add(Self.cxDBButtonEdit2.Hint, Self.cxDBButtonEdit2.Text);
      Add(Self.cxDBButtonEdit1.Hint, Self.cxDBButtonEdit1.Text);

      Add('开票日期', Self.cxDBDateEdit1.Text);
      Add('审核签字', Self.cxDBTextEdit9.Text);
      Add('签收日期', Self.cxDBDateEdit4.Text);
    end;
    Report.frxDBDataset.DataSource := Self.DetailedSource;
    Report.frxDBDataset.UserName   := '进销管理';
    Report.dwIsReportTitle := True; //是否输出标题
    Report.dwReportName    := '标准模板';
    Report.dwRecordCount   := Self.Detailed.RecordCount;
  //  Report.dwReportFile := '进销管理-标准模板'; //报表文件
    Report.dwReportTitle := s; //报表标题
    Report.Show;
  finally
  end;
  }
end;

function TfrmInputformBase.RuningAccount(lpFormType : Integer):Boolean;
var
  s : string;
  szItemsIndex : Integer;
begin
  // numbers      编号
  // Receivables  收款单位
  // TradeType    交易方式
  // PaymentType  付款单位
  // ProjectName  工程/名称
  // CategoryName 费用科目
  // Payee        收款人名称
  // Drawee       付款人名称
  // SumMoney     金额
  // CapitalMoney 大写金额
  // Remarks      备注
  Result := False;
  szItemsIndex := Self.cxDBComboBox7.ItemIndex;  //交易状态
  if szItemsIndex = 1 then
  begin
    //现金入库
    case dwDataType of
      0:
      begin
        //入库
        //g_Table_RuningAccount //现金收款
        //g_RunningIndex := 2;
        lpFormType := 2;
        if Self.cxDBTextEdit8.EditingValue = null then
        begin
          //经手人
        end;

      end;
      1:
      begin
        //出库
        //g_RunningIndex := 3;
        //g_Table_RuningAccount //现金支款
        lpFormType := 3;
      end;
    end;

    if Self.cxDBLookupComboBox4.EditingValue = null then
    begin
      //交易方式
    end;

    if Self.cxDBLookupComboBox5.EditingValue = null then
    begin
      //费用科目
    end;

    with Self.dsRuning do
    begin
      if State <> dsInactive then
      begin
        if Locate(Self.tRuningCol1.DataBinding.FieldName,BillNumber.EditingValue,[]) then
          Edit
        else
          Append;

        FieldByName(Self.tRuningCol1.DataBinding.FieldName).Value := BillNumber.EditingValue;
        case dwDataType of
          0:
          begin
            s := '现金支款';
            FieldByName(Self.tRuningCol2.DataBinding.FieldName).Value := cxDBButtonEdit2.Text;
            FieldByName(Self.tRuningCol3.DataBinding.FieldName).Value := DBCompany.Text;  //经手人
          end;
          1:
          begin
            FieldByName(Self.tRuningCol3.DataBinding.FieldName).Value := cxDBButtonEdit2.Text;
            s := '现金收款';
          end;
        end;
        FieldByName(Self.tRuningCol11.DataBinding.FieldName).Value     := s;
        FieldByName(Self.tRuningCol5.DataBinding.FieldName).Value := cxDBLookupComboBox4.Text;
        FieldByName(Self.tRuningCol4.DataBinding.FieldName).Value := cxDBButtonEdit1.Text;
        FieldByName(Self.tRuningCol6.DataBinding.FieldName).Value := cxDBLookupComboBox5.Text;
        FieldByName(Self.tRuningCol7.DataBinding.FieldName).Value := cxDBCurrencyEdit1.EditingValue;
        FieldByName(Self.tRuningCol8.DataBinding.FieldName).Value := cxDBTextEdit7.EditingValue;
        FieldByName(Self.tRuningCol10.DataBinding.FieldName).Value:= lpFormType;
        FieldByName(Self.tRuningCol9.DataBinding.FieldName).Value := True;
        FieldByName(Self.tRuningCol12.DataBinding.FieldName).Value:= cxDBDateEdit1.EditingValue;
        Post;
      end;
    end;

  end;

end;

procedure TfrmInputformBase.RzToolButton2Click(Sender: TObject);
VAR
  ErrorCount : Integer;
  err:string;
  szItemsIndex : Integer;
  szPayStatus : string;
  szExacct : string;
  szSQLText : string;

begin
  inherited;
  if Application.MessageBox(PWideChar( '是否需要印小票？' ), '小票:', MB_OKCANCEL + MB_ICONWARNING) = IDOK then
  begin
    Self.RzToolButton9.Click;
  end;

  if IsFormEmpty = False then
  begin
    DM.ADOconn.BeginTrans; //开始事务
    try
      //删除空行
      dwADO.DeleteRowEmpty(Detailed,tViewCol4.DataBinding.FieldName);
      //更新信息
      UpdateInfo;
      //更新现金
      RuningAccount(0);
      //开始保存
      dwADO.UpdateTableData(g_Table_RuningAccount,tRuningCol1.DataBinding.FieldName,0,Self.dsRuning); //现金保存
      szSQLText := 'Select * from ' + g_Table_Company_Storage_GroupUnitPrice;
      if dwIsContrct then
      begin
        if not dwADO.UpdateTableData(szSQLText,Self.tGroupColumn4.DataBinding.FieldName,1,Self.dsGroupUnitPrice) then
        begin
          ShowMessage('组保存失败!');
          Exit;
        end;
      end;

      if dwADO.UpdateTableData(g_Table_Company_StorageBillList,
                               Self.BillNumber.DataBinding.DataField,0,Self.Master) and
         dwADO.UpdateTableData(g_Table_Company_StorageDetailed,Self.tViewCol3.DataBinding.FieldName,0,Self.Detailed)  then
      begin

        ShowMessage('保存成功!');
        Timer1Timer(Sender);
      end else
      begin
        ShowMessage('保存失败!!');
      end;

      DM.ADOconn.Committrans; //提交事务
    except
      on E:Exception do
      begin
        DM.ADOconn.RollbackTrans;           // 事务回滚
        err:=E.Message;
        ShowMessage(err);
      end;
    end;
  end;
end;

procedure TfrmInputformBase.RzToolButton3Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmInputformBase.RzToolButton4Click(Sender: TObject);
begin
  TNavgator(Navigator).Buttons[nbPrior].Click;
end;

procedure TfrmInputformBase.RzToolButton5Click(Sender: TObject);
begin
  TNavgator(Navigator).Buttons[nbNext].Click;
end;

procedure TfrmInputformBase.RzToolButton6Click(Sender: TObject);
begin
  inherited;

  if Not IsFormEmpty then
  begin
    //删除空行
    dwADO.DeleteRowEmpty(Detailed,tViewCol4.DataBinding.FieldName);
    //更新现金
    RuningAccount(0);
    //更新信息
    UpdateInfo;
    //提交PoST
    with Self.Navigator do
    begin
      if DataSource.State in [dsEdit , dsInsert] then
      begin
        TNavgator(Navigator).Buttons[nbPost].Click;
      end;
    end;

    with Self.dsGroupUnitPrice do
    begin
      if State in [dsEdit,dsInsert] then Post;
    end;

  end;
end;

procedure TfrmInputformBase.RzToolButton7Click(Sender: TObject);
begin
  if Self.cxDBCheckBox1.EditValue = False then
  begin
    ShowMessage('主合同冻结状态商品明细禁止编辑!');
    Exit;
  end;
///  if dwIsEdit then
  dwADO.ADOIsEdit(Self.Master);
  dwADO.ADOIsEdit(Self.Detailed);
end;

procedure TfrmInputformBase.RzToolButton8Click(Sender: TObject);
var
  Child : TfrmIsViewGrid;
  i : Integer;  //表格设置
begin
  inherited;
  //http://www.a-hospital.com/
  Child := TfrmIsViewGrid.Create(Application);
  try
    Child.g_fromName := dwDetailedTable;
    Child.ShowModal;
    SetcxGrid(Self.tView,0, dwDetailedTable);
  finally
    Child.Free;
  end;
end;

procedure TfrmInputformBase.RzToolButton9Click(Sender: TObject);
begin
  inherited;
  RePinert('标准小票',1);
end;

procedure TfrmInputformBase.Timer1Timer(Sender: TObject);
var
  i : Integer;
  s : string;
begin
  Timer1.Enabled := False;

  dwDetailedTable := 'CompanyDetailed';
  //****************************************************************************
  
  if dwIsContrct then
  begin
    //进销合同管理
    if dwIsEdit then
    begin
      s := ' Where ' + Self.tViewCol3.DataBinding.FieldName + '="' + dwBillNumber + '"';
    //  dwADO.OpenSQL(ClientMakings,'Select * from ' + g_Table_MakingsList); //读取合同材料
    end else
    begin
      s := ' Where 1=2';
    end;

    dwADO.OpenSQL(Self.Master, 'Select * from ' + g_Table_Company_StorageBillList  + s  );

    dwDetailSQLText := 'Select * from ' + g_Table_Company_StorageDetailed  + s;
    dwADO.OpenSQL(Self.Detailed,dwDetailSQLText);

    dwGroupUnitPriceSQLText := 'Select * from ' + g_Table_Company_Storage_GroupUnitPrice;
    dwADO.OpenSQL(Self.dsGroupUnitPrice,dwGroupUnitPriceSQLText);

  end else
  begin
    //现金进销管理

    if dwIsEdit then
      s := ' Where ' + Self.tViewCol3.DataBinding.FieldName + '="' + dwBillNumber + '"'
    else
      s := ' Where 1=2';

    dwADO.OpenSQL(Self.Master, 'Select * from ' + g_Table_Company_StorageBillList  + s  );
    dwDetailSQLText := 'Select * from ' + g_Table_Company_StorageDetailed  + s;
    dwADO.OpenSQL(Self.Detailed,dwDetailSQLText);

  end;

  if (dwDataType = 0) or (dwDataType = 1) then
  begin
    //现金流水帐
    if dwIsEdit then
      s := ' Where '+ Self.tRuningCol1.DataBinding.FieldName + '="' + dwBillNumber + '"'
    else
      s := ' Where 1=2';
    dwADO.OpenSQL(Self.dsRuning, 'Select * from ' + g_Table_RuningAccount + s );
  end;

  if dwIsEdit then
     Self.Master.Edit;

  SetcxGrid(Self.tView,0, dwDetailedTable);

end;

procedure TfrmInputformBase.tvClick(Sender: TObject);
var
  Node : TTreeNode;
  PData: PNodeData;
  szNodeId : Integer;
  szTreeCode:string;
  szNodeText:string;
begin
  Node := (Sender AS TTreeView).Selected;
  if Assigned(Node) then
  begin
    if Node.Level >= 2 then
    begin
      PData := PNodeData(Node.Data);
      if Assigned(PData) then
      begin
        with Self.Master do
        begin
          if State in [dsInactive,dsEdit,dsInsert] then
          begin
            FieldByName('TreeId').Value   := pData^.Index;
            FieldByName('DataType').Value := dwDataType;
            FieldByName('Code').Value     := pData^.Code;
          end;
        end;
      end;
    end;
  end;
end;

procedure TfrmInputformBase.tViewCol10PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  s : string;

begin
  inherited;
  {
  if DisplayValue <> null then
  begin
    s := VarToStr(DisplayValue);
    s := MoneyConvert( StrToFloatDef(s,0) );
    Self.tViewColumn14.EditValue := s;
  end;
  }
end;

procedure TfrmInputformBase.tViewCol15PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  dwUnitPrice : Currency;
  szDisPrice  : Currency;
  dwDisCount : Currency;
  dwValue : Currency;
  Count : Variant;
  szWeight  : Variant;

begin
  inherited;
  {
  dwUnitPrice := StrToCurrDef( VarToStr( Self.tViewCol8.EditValue ) ,0);
  dwDisCount  := StrToCurrDef( VarToStr( DisplayValue ) ,0); //折扣

  szDisPrice := dwDisCount * dwUnitPrice;

  Self.tViewCol16.EditValue := szDisPrice;
  }
  Self.tViewCol15.EditValue := DisplayValue;
//  Count := Self.tViewCol7.EditValue; //数量
//  szWeight := Self.tViewColumn10.EditValue; //重量
  GetSumMoney( );
end;

procedure TfrmInputformBase.tViewCol16PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  Count , szWeight : Variant;
begin
  inherited;
  Self.tViewCol16.EditValue := DisplayValue;
  {
  Count := Self.tViewCol7.EditValue; //数量
  szWeight := Self.tViewColumn10.EditValue; //重量
  }
  GetSumMoney();
end;

procedure TfrmInputformBase.tViewCol1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  inherited;
  AText := IntToStr(ARecord.Index + 1);
end;

procedure TfrmInputformBase.tViewCol4PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
var
  Child : TfrmMakings;
  szSignName : Variant;

begin
  inherited;
  if not dwIsContrct then
  begin
    Child := TfrmMakings.Create(nil);
    try
      Child.dwParentHandle := Handle;
      Child.dwIsReadonly   := True;
      Child.ShowModal;
    finally
      Child.Free;
    end;
  end else
  begin
    Application.CreateForm(tfrmContrctGoodsList,frmContrctGoodsList);
    szSignName := Self.cxDBButtonEdit2.EditValue;
    if szSignName <> null then
    begin
      try
        frmContrctGoodsList.dwParentHandle:= Handle;
        frmContrctGoodsList.dwModuleIndex := dwDataType;
        frmContrctGoodsList.dwSignName    := szSignName;
        frmContrctGoodsList.ShowModal;
      finally
        frmContrctGoodsList.Free;
      end;
    end else
    begin
      Application.MessageBox(PWideChar( '未选择' + Self.cxDBButtonEdit2.Hint ), '记录？', MB_OKCANCEL + MB_ICONWARNING)
    end;
  end;
end;

procedure TfrmInputformBase.tViewCol6PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
var
  szNoNumbers : Variant;
  szSignName  : Variant;
  szUnitPrice : Variant;
  szGroupPrice: Variant;
begin
  inherited;
  if dwIsContrct then
  begin
    szSignName  := Self.cxDBButtonEdit2.EditingValue;
    if szSignName = null then
    begin
      Application.MessageBox(PWideChar( '未选择' + Self.cxDBButtonEdit2.Hint ), '记录？', MB_OKCANCEL + MB_ICONWARNING)
    end else
    begin
      szNoNumbers := Self.tViewColumn8.EditValue;
      if szNoNumbers <> null then
      begin
        Application.CreateForm(TfrmGroupUnitPrice,frmGroupUnitPrice);
        try
          szUnitPrice := Self.tViewColumn11.EditValue;

          Self.tView.ViewData.Collapse(True);
          frmGroupUnitPrice.dwModuleIndex := dwDataType;
          frmGroupUnitPrice.dwSignName := szSignName;
          frmGroupUnitPrice.ds.DataSet := Self.dsGroupUnitPrice;
          Self.dsGroupUnitPrice.Filter   := 'ParentId=' + VarToStr(Self.tViewColumn8.EditValue) + '';
          Self.dsGroupUnitPrice.Filtered := True;
          frmGroupUnitPrice.dwParentId := szNoNumbers;
          frmGroupUnitPrice.ShowModal;

          szGroupPrice := frmGroupUnitPrice.dwSumMoney;

          Self.tViewCol8.EditValue    := szGroupPrice + szUnitPrice;
          Self.tViewCol16.EditValue   := Self.tViewCol8.EditValue;
          Self.tViewColumn9.EditValue := szGroupPrice;
          Self.dsGroupUnitPrice.Filtered := False;
          GetSumMoney;
        //  Self.tView.Controller.FocusedRow.Expand(True);
        finally
          frmGroupUnitPrice.Free;
        end;

      end;
    end;
  end;
end;

procedure TfrmInputformBase.tViewCol7PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
var
  Count,UnitPrice , szWeight : Variant;
begin
  inherited;
  Application.CreateForm(TfrmCalcTool,frmCalcTool);
  try
    frmCalcTool.CountTotal.EditValue    := Self.tViewCol7.EditValue;
    frmCalcTool.cxMemo1.EditValue       := Self.tViewColumn12.EditValue;
    frmCalcTool.VariableCount.EditValue := Self.tViewColumn13.EditValue;

    frmCalcTool.dwParentHandle := Handle;
    frmCalcTool.ShowModal;

    Self.tViewCol7.EditValue     := frmCalcTool.CountTotal.EditingValue;
    Self.tViewColumn12.EditValue := frmCalcTool.cxMemo1.EditValue;
    Self.tViewColumn13.EditValue := frmCalcTool.VariableCount.EditValue;

  //Self.tvViewColumn8.Index //数量
    {
    UnitPrice  := Self.tViewCol8.EditValue;
    Count := Self.tViewCol7.EditValue; //数量
    szWeight := Self.tViewColumn10.EditValue; //重量
    }
    GetSumMoney();//Count,UnitPrice , szWeight

  finally
    frmCalcTool.Free;
  end;
end;

procedure TfrmInputformBase.GetUnitPriceMoney(lpCount , lpWeight , lpUnitPrice : string);
var
  Count,szWeight : Double;
  UnitPrice : Currency;

  szSumMoney : Currency;
  szColName : string;
  szValue : Currency;
  s : string;

begin
  if self.Master.State in [dsEdit,dsInsert] then Self.Master.Edit;
  szSumMoney := StrToFloatDef( lpUnitPrice ,0 ) * StrToFloatDef(  lpCount , 0) * StrToFloatDef(  lpWeight , 1);
  Self.tViewCol10.EditValue := szSumMoney;
  {
  Self.cxDBCurrencyEdit1.Clear;
  Self.cxDBTextEdit7.Clear;
  }
  {
  s := VarToStr(Self.tView.DataController.Summary.FooterSummaryValues[0]);
  szValue := StrToCurrDef(s,0);
  }
  Self.tViewColumn14.EditValue := MoneyConvert( szSumMoney );
end;

procedure TfrmInputformBase.CalculatData(Sender: TObject; s: string);
begin
  tView.DataController.PostEditingData;
  with TcxCustomTextEdit(Sender) do
  begin
    {
    with Self.Detailed do
    begin
      if State in [dsEdit,dsInsert] then
      begin
        FieldByName(Self.tViewCol10.DataBinding.FieldName).Value := StrToFloatDef(Text, 0) * StrToFloatDef(s, 0);
      end;
    end;
    }
    try
      tViewCol10.EditValue := StrToFloatDef(Text, 0) * StrToFloatDef(s, 0)
    except
      tViewCol10.EditValue := 0;
    end;
    SelStart := length(Text);
    SelLength := 0;
  end;

end;

procedure TfrmInputformBase.tViewCol7PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  UnitPrice , szWeight: Variant;
begin
  inherited;
  Self.tViewCol7.EditValue := DisplayValue;
  {
  UnitPrice  := Self.tViewCol8.EditValue;
  szWeight := Self.tViewColumn10.EditValue; //重量
  }
  GetSumMoney();
end;

procedure TfrmInputformBase.tViewCol8PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  Count , szWeight : Variant;
begin
  inherited;
  Self.tViewCol8.EditValue  := DisplayValue;
  {
  Count    := Self.tViewCol7.EditValue; //数量
  szWeight := Self.tViewColumn10.EditValue; //重量
  }
  GetSumMoney( );
end;

procedure TfrmInputformBase.tViewColumn10PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  s : string;
  Count : Variant;
begin
  inherited;
  if DisplayValue <> null then
  begin
    {
    s := VarToStr(DisplayValue);
    Count := Self.tViewCol7.EditValue; //数量
    Self.tViewColumn10.EditValue  := DisplayValue;
    }
    GetSumMoney(); //Count, Self.tViewCol8.EditValue , DisplayValue
  end;

end;

procedure TfrmInputformBase.tViewColumnSizeChanged(Sender: TcxGridTableView;
  AColumn: TcxGridColumn);
begin
   SetcxGrid(Self.tView,1, dwDetailedTable);
end;

procedure TfrmInputformBase.tViewDblClick(Sender: TObject);
begin
  inherited;
  Self.RzToolButton7.Click;
end;

procedure TfrmInputformBase.tViewEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  dwADO.ADOIsEditing(Self.Detailed,AAllow);
end;

procedure TfrmInputformBase.tViewEditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
var
  v:Variant;
begin
  if (Key = 13) then
  begin

    if (AItem.Index = Self.tViewCol10.Index) and (Self.tView.Controller.FocusedRow.IsLast) then
    begin
      if (IsNewRow ) then
      begin
        dwADO.ADOAppend(Self.Detailed);
        Self.tViewCol1.FocusWithSelection;
        Self.tViewCol1.Editing := True;
      end;

    end;

  end;
end;

procedure TfrmInputformBase.tViewTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
begin
  inherited;
  if AValue <> null then
  begin
    Self.cxDBCurrencyEdit1.EditValue := AValue;
    Self.cxDBCurrencyEdit1.PostEditValue;

    Self.cxDBTextEdit7.EditValue := MoneyConvert( Self.cxDBCurrencyEdit1.EditValue );
    Self.cxDBTextEdit7.PostEditValue;
  end else
  begin
    Self.cxDBCurrencyEdit1.EditValue := 0;
    Self.cxDBCurrencyEdit1.PostEditValue;

    Self.cxDBTextEdit7.EditValue := MoneyConvert( Self.cxDBCurrencyEdit1.EditValue );
    Self.cxDBTextEdit7.PostEditValue;
  end;
end;


end.
