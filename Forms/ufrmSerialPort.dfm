object frmSerialPort: TfrmSerialPort
  Left = 0
  Top = 0
  Caption = #20018#21475
  ClientHeight = 443
  ClientWidth = 782
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object grpComSetting: TGroupBox
    Left = 0
    Top = 0
    Width = 782
    Height = 105
    Align = alTop
    Caption = #31471#21475#35774#32622
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object Label1: TLabel
      Left = 38
      Top = 25
      Width = 67
      Height = 16
      AutoSize = False
      Caption = #20018#21475
    end
    object Label2: TLabel
      Left = 186
      Top = 25
      Width = 71
      Height = 16
      AutoSize = False
      Caption = #27874#29305#29575
    end
    object Label3: TLabel
      Left = 440
      Top = 25
      Width = 57
      Height = 16
      AutoSize = False
      Caption = #25968#25454#20301
    end
    object Label4: TLabel
      Left = 552
      Top = 25
      Width = 56
      Height = 16
      AutoSize = False
      Caption = #20572#27490#20301
    end
    object Label5: TLabel
      Left = 311
      Top = 25
      Width = 74
      Height = 16
      AutoSize = False
      Caption = #26657#39564
    end
    object cmbCommName: TComboBox
      Left = 38
      Top = 47
      Width = 91
      Height = 24
      TabOrder = 0
      Text = 'COM1:'
    end
    object cmbBaudrate: TComboBox
      Left = 186
      Top = 47
      Width = 71
      Height = 24
      ItemIndex = 4
      TabOrder = 1
      Text = '  9600'
      Items.Strings = (
        '   600'
        '  1200'
        '  2400'
        '  4800'
        '  9600'
        ' 19200'
        ' 38400'
        ' 57600'
        '115200')
    end
    object cmbDatabits: TComboBox
      Left = 440
      Top = 47
      Width = 57
      Height = 24
      ItemIndex = 3
      TabOrder = 3
      Text = '8'
      Items.Strings = (
        '5'
        '6'
        '7'
        '8')
    end
    object cmbStopbits: TComboBox
      Left = 552
      Top = 47
      Width = 56
      Height = 24
      ItemIndex = 0
      TabOrder = 4
      Text = '1'
      Items.Strings = (
        '1'
        '1.5'
        '2')
    end
    object cmbParity: TComboBox
      Left = 311
      Top = 47
      Width = 74
      Height = 24
      ItemIndex = 0
      TabOrder = 2
      Text = #26080#26657#39564
      Items.Strings = (
        #26080#26657#39564
        #22855#26657#39564
        #20598#26657#39564)
    end
  end
  object grpError: TGroupBox
    Left = 0
    Top = 105
    Width = 782
    Height = 71
    Align = alClient
    Caption = #38169#35823#20449#24687#21306
    TabOrder = 1
    ExplicitTop = 80
    ExplicitWidth = 659
    ExplicitHeight = 69
    object MmoError: TMemo
      Left = 2
      Top = 15
      Width = 778
      Height = 54
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Lines.Strings = (
        'mmoError')
      ParentFont = False
      TabOrder = 0
      ExplicitWidth = 655
      ExplicitHeight = 52
    end
  end
  object grpReceived: TGroupBox
    Left = 0
    Top = 176
    Width = 782
    Height = 121
    Align = alBottom
    Caption = #25509#25910#25968#25454#21306
    TabOrder = 2
    ExplicitTop = 149
    ExplicitWidth = 659
    object mmoReceived: TMemo
      Left = 2
      Top = 15
      Width = 778
      Height = 104
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Lines.Strings = (
        'mmoReceived')
      ParentFont = False
      TabOrder = 0
      ExplicitWidth = 655
    end
  end
  object grpSend: TGroupBox
    Left = 0
    Top = 297
    Width = 782
    Height = 105
    Align = alBottom
    Caption = #21457#36865#25968#25454#21306
    TabOrder = 3
    ExplicitTop = 270
    ExplicitWidth = 659
    object mmoSend: TMemo
      Left = 2
      Top = 15
      Width = 778
      Height = 88
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Lines.Strings = (
        'mmoSend')
      ParentFont = False
      TabOrder = 0
      ExplicitWidth = 655
    end
  end
  object pnlCommand: TPanel
    Left = 0
    Top = 402
    Width = 782
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 4
    ExplicitTop = 375
    ExplicitWidth = 659
    object btnOpen: TButton
      Left = 30
      Top = 6
      Width = 75
      Height = 25
      Caption = 'Open'
      TabOrder = 0
      OnClick = btnOpenClick
    end
    object btnClose: TButton
      Left = 144
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Close'
      Enabled = False
      TabOrder = 1
    end
    object btnSend: TButton
      Left = 272
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Send'
      Enabled = False
      TabOrder = 2
    end
  end
end
