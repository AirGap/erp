unit ufrmConstructPost;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  cxTextEdit, cxMemo, RzButton, cxMaskEdit, cxDropDownEdit, cxCalendar, cxDBEdit,
  ComCtrls, dxSkinBlueprint, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle,
  dxSkinHighContrast, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinSevenClassic, dxSkinSharpPlus,
  dxSkinTheAsphaltWorld, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint;

type
  TfrmConstructPost = class(TForm)
    GroupBox2: TGroupBox;
    cxTextEdit1: TcxTextEdit;
    cxMemo1: TcxMemo;
    RzBitBtn3: TRzBitBtn;
    RzBitBtn1: TRzBitBtn;
    RzBitBtn2: TRzBitBtn;
    Label1: TLabel;
    lbl: TLabel;
    Label3: TLabel;
    DateTimePicker1: TDateTimePicker;
    procedure RzBitBtn3Click(Sender: TObject);
    procedure RzBitBtn1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure RzBitBtn2Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    g_TableName : string;
    g_PostCode  : string;
    g_PostType  : string;
    g_PostNumbers : string;
    g_PostParent  : string;
    g_ProjectDir  : string;
    
  end;

var
  frmConstructPost: TfrmConstructPost;

  
implementation

uses
   uDataModule,global;

{$R *.dfm}

procedure TfrmConstructPost.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    13:
    begin
      Self.RzBitBtn3.Click;
    end;
    27:
    begin
      Close;
    end;
  end;
end;

procedure TfrmConstructPost.FormShow(Sender: TObject);
begin
  Self.DateTimePicker1.Date := Now;
end;

procedure TfrmConstructPost.RzBitBtn3Click(Sender: TObject);
var
  szName : string;
  szDate : TDateTime;
  szRemarks : string;
  s : string;

begin
  szName := Self.cxTextEdit1.Text;
  if Length(szName ) <> 0 then
  begin
      szDate := Self.DateTimePicker1.Date;
      szRemarks := Self.cxMemo1.Text;
      if DM.IsNumbers(g_TableName,g_PostNumbers) and (g_PostNumbers <> '') then
      begin
        //编辑
         s := Format('Update '+ g_TableName +' set ' +
                                             'chcaption="%s",'  +
                                             'chDate="%s",' +
                                             'chremarks="%s"' +
                                             ' where Numbers="%s"',[
                                                         szName,
                                                         FormatDateTime('yyyy-MM-dd',szDate),
                                                         szRemarks,
                                                         g_PostNumbers
                                                         ]);

      end
      else
      begin
        //新增
        s := Format( 'Insert into '+ g_TableName +' (code,' +
                                                     'numbers,'   +
                                                     'Parent,'    +
                                                     'chCaption,' +
                                                     'chDate,'    +
                                                     'chRemarks' +
                                                     ') values("%s","%s","%s","%s","%s","%s")',[
                                                          g_PostCode,
                                                          g_PostNumbers,
                                                          g_PostParent ,
                                                          szName, 
                                                          FormatDateTime('yyyy-MM-dd',szDate),
                                                          szRemarks
                                                          ]);
      end;

      with DM.Qry do
      begin
        Close;
        SQL.Clear;
        SQL.Text := s;
        if ExecSQL <> 0 then
        begin
          g_PostNumbers := DM.getDataMaxDate(g_TableName);
          Application.MessageBox('执行成功!','',MB_OK + MB_ICONQUESTION);
        end else
        begin
          Application.MessageBox('执行失败!','',MB_OK + MB_ICONQUESTION)
        end;

      end;


  end else
  begin

  end;    

end;

procedure TfrmConstructPost.RzBitBtn1Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmConstructPost.RzBitBtn2Click(Sender: TObject);
var
  s : string;
begin
  //打开目录
  s := Concat(g_ProjectDir,'\' + g_PostNumbers);
  CreateOpenDir(Handle,s,True);
end;

end.
