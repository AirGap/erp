unit ufrmBranchInput;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, jpeg, ExtCtrls, RzPanel, RzRadGrp, Mask, RzEdit, ComCtrls, StdCtrls,
  RzButton, RzCmboBx, RzDBCmbo, RzTabs, DB, ADODB, ExtDlgs, RzBckgnd;

type
  TfrmBranchlnput = class(TForm)
    Qry: TADOQuery;
    RzPageControl1: TRzPageControl;
    TabSheet1: TRzTabSheet;
    Panel9: TPanel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label10: TLabel;
    ComboBox2: TComboBox;
    RzEdit12: TRzEdit;
    RzBitBtn1: TRzBitBtn;
    RzBitBtn17: TRzBitBtn;
    TabSheet2: TRzTabSheet;
    RzBitBtn6: TRzBitBtn;
    RzEdit3: TRzEdit;
    Label21: TLabel;
    RzEdit2: TRzEdit;
    Panel2: TPanel;
    RzBitBtn2: TRzBitBtn;
    RzBitBtn3: TRzBitBtn;
    RzBitBtn4: TRzBitBtn;
    RzEdit16: TRzEdit;
    Label1: TLabel;
    Label9: TLabel;
    RzEdit5: TRzEdit;
    Label2: TLabel;
    Label3: TLabel;
    RzEdit8: TRzEdit;
    RzEdit10: TRzEdit;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    RzEdit13: TRzEdit;
    Label7: TLabel;
    Label8: TLabel;
    RzEdit14: TRzEdit;
    Label11: TLabel;
    ComboBox1: TComboBox;
    Label23: TLabel;
    RzEdit15: TRzEdit;
    Label12: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    RzEdit4: TRzEdit;
    RzEdit11: TRzEdit;
    AddEndDate: TDateTimePicker;
    AddStartDate: TDateTimePicker;
    ModifyStartDate: TDateTimePicker;
    ModifyEndDate: TDateTimePicker;
    RzMemo2: TRzMemo;
    RzMemo1: TRzMemo;
    RzPanel3: TRzPanel;
    Label19: TLabel;
    RzEdit1: TRzEdit;
    Label20: TLabel;
    RzEdit6: TRzEdit;
    procedure RzBitBtn17Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure RzBitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RzEdit2KeyPress(Sender: TObject; var Key: Char);
    procedure RzBitBtn3Click(Sender: TObject);
    procedure RzEdit5Change(Sender: TObject);
    procedure RzBitBtn6Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    g_PostCode : string;
    g_ProjectDir : string;
    g_PostNumbers: string;
    g_Handte     : THandle;
  //  procedure WMMove(var Message: TWMMove); message WM_MOVE;
  end;

var
  frmBranchlnput: TfrmBranchlnput;
  g_TableName : string = 'expenditure';
  
implementation

uses
   uDataModule,global,ShellAPI,ufrmManage;

{$R *.dfm}


procedure TfrmBranchlnput.RzBitBtn17Click(Sender: TObject);
var
  s : string;
  szCode : string;
  szPayee: string;
  
  szCaption: string;
  szContent : string;
  szPrice: Currency;
  szNum  : Single;
  szRemarks: string;
  szUnit   : string;
  szPic    : string;
  szPhoto  : string;
  szPayType: Integer;
  szTruck  : Integer;
  szDate   : TDateTime;
  i : Integer;
  pData : PPaytype;
  parent: string;
  
begin
  szCode := g_PostCode;
  if Length(szCode) = 0 then
  begin
    Application.MessageBox('请选择合同',m_title,MB_OK + MB_ICONHAND);
    Close;
  end
  else
  begin
  //  szNumber := StrToIntDef( Self.RzEdit2.Text,0 );
    if g_PostNumbers = '' then
    begin
      Self.RzEdit2.SetFocus;
      Application.MessageBox('请输入单号',m_title,MB_OK + MB_ICONHAND);
    end
    else
    begin
      s := 'Select * from '+ g_TableName +' where numbers="'+ g_PostNumbers+'"';
      OutputLog(s);
      with dm.Qry do
      begin
        Close;
        SQL.Clear;
        SQL.Text := s;
        Open;
        if 0<>RecordCount then
        begin
          Application.MessageBox('单号已存在',m_title,MB_OK + MB_ICONHAND);
          Exit;
        end;

      end;

      szUnit := Self.ComboBox2.Text;
      if Length(szUnit) = 0 then
      begin
        Application.MessageBox('请选择单位',m_title,MB_OK + MB_ICONHAND);
        Self.ComboBox2.DroppedDown := True;
      end
      else
      begin
        szPrice := StrToFloatDef( Self.RzEdit5.Text ,0) ;
           szNum   := StrToFloatDef(Self.RzEdit4.Text,0);
           if szNum = 0 then
           begin
             Application.MessageBox('请输入数量',m_title,MB_OK + MB_ICONHAND);
           end
           else
           begin
               szCaption := Self.RzEdit3.Text;  //名称
               szContent := Self.RzEdit12.Text; //内容
               szRemarks := Self.RzMemo2.Text;  //备注

               szTruck   := StrToIntDef(Self.RzEdit16.Text,1);
               szDate    := AddStartDate.DateTime;
               parent    := '';// Self.RzEdit18.Text; //主合同
               s := Format( 'Insert into '+ g_TableName  +' (code,'+
                                              'parent,'    +
                                              'numbers,'   +
                                              'caption,'   +
                                              'content,'   +
                                              'pay_time,'  +
                                              'End_time,'  +
                                              'truck,'     +
                                              'Price,'     +
                                              'num,'       +
                                              'Units,'     +
                                              'PurchaseCompany,'   +
                                              'pacttype,'  +
                                              'remarks'    +
                                              ') values("%s","%s","%s","%s","%s","%s","%s","%d",''%m'',''%f'',"%s","%s","%d","%s")',[
                                                                      szCode,
                                                                      parent,
                                                                      g_PostNumbers,
                                                                      szCaption,
                                                                      szContent,
                                                                      FormatDateTime('yyyy-MM-dd',AddEndDate.DateTime),
                                                                      FormatDateTime('yyyy-MM-dd',Now),
                                                                      szTruck,
                                                                      szPrice,
                                                                      szNum,
                                                                      szUnit,
                                                                      self.RzEdit1.Text,
                                                                      g_pacttype,
                                                                      szRemarks
                                                                      ] );
               with DM.Qry do
               begin
                 Close;
                 SQL.Clear;
                 SQL.Text := s;
                 if 0 <> ExecSQL then
                 begin
                   Self.FormActivate(Sender);
                   m_IsModify := True;
                   SendMessage(g_Handte,WM_LisView,0,0);
                   Application.MessageBox('添加成功',m_title,MB_OK + MB_ICONQUESTION );
                 end else
                 begin
                   Application.MessageBox('添加失败',m_title,MB_OK + MB_ICONQUESTION );
                 end;

               end;

           end;

      end;

    end;

  end;

end;

procedure TfrmBranchlnput.RzBitBtn1Click(Sender: TObject);
var
  s : string;
begin
  //打开目录
  s := Concat(g_ProjectDir,'\' + g_PostNumbers);
  CreateOpenDir(Handle,s,True);
end;

procedure TfrmBranchlnput.RzBitBtn3Click(Sender: TObject);
var
  s : string;
  szCode : string;
  szPayee: string;
  szCaption: string;
  szContent : string;
  szPrice: Currency;
  szNum  : Single;
  szRemarks: string;
  szUnit   : string;
  szPic    : string;
  szPhoto  : string;
  szPayType: Integer;
  sztruck  : Integer;
  i : Integer;
  pData : PPaytype;
  szDate : TDateTime;
  
begin

  szCode := g_PostCode;
  if Length(szCode) = 0 then
  begin
    Application.MessageBox('请选择合同',m_title,MB_OK + MB_ICONHAND);
    Close;
  end
  else
  begin
  //  szNumber := StrToIntDef( Self.RzEdit14.Text ,0);
    with DM.Qry do
    begin
      Close;
      SQL.Clear;
      SQL.Text := 'Select *from '+ g_TableName +' where numbers="' + g_PostNumbers+'"';
      Open;
      if RecordCount = 0 then
      begin
        Application.MessageBox('修改单号不存在',m_title,MB_OK + MB_ICONHAND);
        Exit;
      end;
    end;
    
    if g_PostNumbers = '' then
    begin
      Self.RzEdit2.SetFocus;
      Application.MessageBox('请输入单号',m_title,MB_OK + MB_ICONHAND);
    end
    else
    begin

      if Self.ComboBox1.ItemIndex < 0 then
      begin
        Application.MessageBox('请选择单位',m_title,MB_OK + MB_ICONHAND);
        Self.ComboBox1.DroppedDown := True;
      end
      else
      begin
        szUnit := Self.ComboBox1.Text;
        szPrice := StrToFloatDef( Self.RzEdit13.Text ,0);
           szNum := StrToFloatDef(Self.RzEdit11.Text,0);
           if szNum = 0 then
           begin
             Application.MessageBox('请输入数量',m_title,MB_OK + MB_ICONHAND);
           end
           else
           begin
             sztruck   := StrToIntDef( Self.RzEdit15.Text,1);
             szCaption := Self.RzEdit10.Text;  //名称
             szContent := Self.RzEdit8.Text; //内容
             szRemarks := Self.RzMemo1.Text;  //备注
             szDate    := ModifyStartDate.DateTime;
             szPic    :=  '';

             s := Format('Update '+ g_TableName +' set caption="%s",'  +
                                       'content="%s",'  +
                                       'pay_time="%s",' +
                                       'End_time="%s",' +
                                       'Price=''%m'','  +
                                       'num=''%f'','    +
                                       'truck=''%d'','  +
                                       'Units="%s",'    +
                                       'remarks="%s",'  +
                                       'PurchaseCompany="%s"'   +
                                        ' where 编号=%d',[
                                                     szCaption,
                                                     szContent,
                                                     FormatDateTime('yyyy-MM-dd',ModifyStartDate.DateTime),
                                                     FormatDateTime('yyyy-MM-dd',Now),
                                                     szPrice,
                                                     szNum,
                                                     sztruck,
                                                     szUnit,
                                                     szRemarks,
                                                     self.RzEdit6.Text,
                                                     g_code
                                                     ]);

              with DM.Qry do
              begin
                Close;
                SQL.Clear;
                SQL.Text := s;
                if 0 <> ExecSQL then
                begin
                  m_IsModify := True;
                  SendMessage(g_Handte,WM_LisView,0,0);
                  Application.MessageBox('修改成功',m_title,MB_OK + MB_ICONQUESTION);
                end else
                begin
                  Application.MessageBox('修改失败',m_title,MB_OK + MB_ICONQUESTION);
                end;
              end;

           end;

      end;

    end;
       
  end;

  
end;

procedure TfrmBranchlnput.RzBitBtn6Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmBranchlnput.RzEdit2KeyPress(Sender: TObject; var Key: Char);
begin
if (key<>#46) and ((key < #48) or (key > #57)) and (key <> #8) and (Key = #77) then//如果输入不是数字或小数点（#46代表小数点）
  begin
    key:=#0; //取消输入的内容（#0代表空值）
  end;
end;

procedure TfrmBranchlnput.RzEdit5Change(Sender: TObject);
begin
  if Sender = RzEdit5 then
  begin
    Self.Label9.Caption := MoneyConvert( StrToFloatDef( Self.RzEdit5.Text,0 ) );
  end;

  if Sender = RzEdit13 then
  begin
    Self.Label12.Caption := MoneyConvert( StrToFloatDef( Self.RzEdit13.Text,0 ) );
  end;
end;

procedure TfrmBranchlnput.FormActivate(Sender: TObject);
var
  i : Integer;
begin
  case Self.RzPageControl1.ActivePageIndex of
    0:
    begin
      g_PostNumbers := DM.getDataMaxDate(g_TableName);
      Self.RzEdit2.Text := g_PostNumbers;
    end;
    1:
    begin
      Self.RzEdit14.Text := g_numbers ; //单号
      g_PostNumbers := g_numbers;

      Self.RzEdit13.Text := FloatToStr(g_Price);//单价
      Self.RzEdit11.Text := FloatToStr( g_num ) ;//数量
      Self.RzEdit15.Text := IntToStr( g_truck);//车
      Self.RzEdit10.Text := g_caption; //名称
      Self.RzEdit8.Text  := g_content;
      Self.RzEdit6.Text  := g_PurchaseCompany;
      g_PostCode := g_global_code;
      ModifyStartDate.DateTime  := g_pay_time;
      ModifyEndDate.DateTime := g_End_time;

      Self.RzMemo1.Text  := g_remarks;
      i := Self.ComboBox1.Items.IndexOf( g_Units);
      if i >= 0 then Self.ComboBox1.ItemIndex := i;

    end;  
  end;

end;

procedure TfrmBranchlnput.FormCreate(Sender: TObject);
var
  i : Integer;
  
begin
  frmBranchlnput := Self;
  
  for i := 0 to Self.RzPageControl1.PageCount - 1 do Self.RzPageControl1.Pages[i].TabVisible := False;//隐藏
  AddStartDate.Date := Date;
  AddEndDate.Date   := Date;
  
  ModifyStartDate.Date := Date;
  ModifyEndDate.Date   := Date;
  
end;

procedure TfrmBranchlnput.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    13:
    begin
      case Self.RzPageControl1.ActivePageIndex of
        0:
        begin
          Self.RzBitBtn17.Click;
        end;
        1:
        begin
          Self.RzBitBtn3.Click;
        end;
      end;
    end;
    27:
    begin
      Close;
    end;
  end;
end;

procedure TfrmBranchlnput.FormShow(Sender: TObject);
var
  s : string;
begin
  with DM.Qry do
  begin
    Close;
    SQL.Clear;
    SQL.Text := 'Select * from unit where chType=0';
    Open;

    if 0 <> RecordCount then
     begin
       Self.ComboBox1.Clear;
       Self.ComboBox2.Clear;

       while not Eof do
       begin
         s := FieldByName('name').AsString;
         Self.ComboBox1.Items.Add( s );
         Self.ComboBox2.Items.Add( s );
         Next;
       end;
       Self.ComboBox1.ItemIndex := 0;
       Self.ComboBox2.ItemIndex := 0;
     end;

  end;
end;

end.
