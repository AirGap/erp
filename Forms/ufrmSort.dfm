object frmSort: TfrmSort
  Left = 0
  Top = 0
  Caption = #20844#21496#20449#24687
  ClientHeight = 447
  ClientWidth = 953
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object RzPanel1: TRzPanel
    Left = 0
    Top = 0
    Width = 274
    Height = 447
    Align = alLeft
    BorderInner = fsFlat
    BorderOuter = fsNone
    BorderSides = [sdRight]
    TabOrder = 0
    object RzToolbar3: TRzToolbar
      Left = 0
      Top = 0
      Width = 273
      AutoStyle = False
      Images = DM.cxImageList1
      RowHeight = 28
      BorderInner = fsNone
      BorderOuter = fsNone
      BorderSides = [sdTop]
      BorderWidth = 0
      GradientColorStyle = gcsCustom
      TabOrder = 0
      VisualStyle = vsGradient
      ToolbarControls = (
        RzSpacer16
        RzToolButton1
        RzSpacer17
        RzToolButton2
        RzSpacer18
        RzToolButton3
        RzSpacer1
        RzToolButton4
        RzSpacer2
        RzToolButton5)
      object RzSpacer16: TRzSpacer
        Left = 4
        Top = 4
      end
      object RzToolButton1: TRzToolButton
        Left = 12
        Top = 4
        Width = 24
        GradientColorStyle = gcsCustom
        ImageIndex = 37
        ShowCaption = False
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        UseToolbarVisualStyle = False
        VisualStyle = vsGradient
        Caption = #26032#24314#20844#21496
        OnClick = RzToolButton1Click
      end
      object RzSpacer17: TRzSpacer
        Left = 36
        Top = 4
      end
      object RzToolButton2: TRzToolButton
        Left = 44
        Top = 4
        Width = 24
        ImageIndex = 0
        ShowCaption = False
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #32534#36753
        OnClick = RzToolButton1Click
      end
      object RzSpacer18: TRzSpacer
        Left = 68
        Top = 4
      end
      object RzToolButton3: TRzToolButton
        Left = 76
        Top = 4
        Width = 24
        ImageIndex = 2
        ShowCaption = False
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #21024#38500
        OnClick = RzToolButton1Click
      end
      object RzSpacer1: TRzSpacer
        Left = 100
        Top = 4
      end
      object RzToolButton4: TRzToolButton
        Left = 108
        Top = 4
        ImageIndex = 29
        OnClick = RzToolButton4Click
      end
      object RzSpacer2: TRzSpacer
        Left = 133
        Top = 4
      end
      object RzToolButton5: TRzToolButton
        Left = 141
        Top = 4
        ImageIndex = 31
        OnClick = RzToolButton5Click
      end
    end
    object tv: TTreeView
      Left = 0
      Top = 32
      Width = 273
      Height = 415
      Align = alClient
      BevelInner = bvNone
      BevelOuter = bvNone
      DragMode = dmAutomatic
      HideSelection = False
      Images = DM.TreeImageList
      Indent = 19
      RowSelect = True
      SortType = stText
      TabOrder = 1
      OnCancelEdit = tvCancelEdit
      OnChange = tvChange
      OnDragDrop = tvDragDrop
      OnDragOver = tvDragOver
      OnEdited = tvEdited
      OnKeyDown = tvKeyDown
    end
  end
  object RzPanel2: TRzPanel
    Left = 282
    Top = 0
    Width = 671
    Height = 447
    Align = alClient
    BorderInner = fsFlat
    BorderOuter = fsNone
    BorderSides = [sdLeft]
    TabOrder = 1
    object RzToolbar2: TRzToolbar
      Left = 1
      Top = 0
      Width = 670
      AutoStyle = False
      Images = DM.cxImageList1
      RowHeight = 28
      BorderInner = fsNone
      BorderOuter = fsNone
      BorderSides = [sdTop]
      BorderWidth = 0
      GradientColorStyle = gcsCustom
      TabOrder = 0
      VisualStyle = vsGradient
      ToolbarControls = (
        RzSpacer5
        RzToolButton7
        RzSpacer15
        RzToolButton6
        RzSpacer3
        RzToolButton8
        RzSpacer9
        RzToolButton9
        RzSpacer10
        RzToolButton10
        RzSpacer4
        RzToolButton12)
      object RzSpacer5: TRzSpacer
        Left = 4
        Top = 4
      end
      object RzToolButton7: TRzToolButton
        Left = 12
        Top = 4
        Width = 60
        ImageIndex = 37
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #26032#22686
        OnClick = RzToolButton7Click
      end
      object RzToolButton8: TRzToolButton
        Left = 150
        Top = 4
        Width = 60
        ImageIndex = 3
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #20445#23384
        OnClick = RzToolButton7Click
      end
      object RzSpacer9: TRzSpacer
        Left = 210
        Top = 4
      end
      object RzToolButton9: TRzToolButton
        Left = 218
        Top = 4
        Width = 60
        ImageIndex = 2
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #21024#38500
        OnClick = RzToolButton7Click
      end
      object RzSpacer10: TRzSpacer
        Left = 278
        Top = 4
      end
      object RzToolButton12: TRzToolButton
        Left = 370
        Top = 4
        Width = 60
        ImageIndex = 8
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #36864#20986
        OnClick = RzToolButton12Click
      end
      object RzSpacer15: TRzSpacer
        Left = 72
        Top = 4
      end
      object RzSpacer3: TRzSpacer
        Left = 142
        Top = 4
      end
      object RzToolButton6: TRzToolButton
        Left = 80
        Top = 4
        Width = 62
        ImageIndex = 0
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #32534#36753
        OnClick = RzToolButton6Click
      end
      object RzSpacer4: TRzSpacer
        Left = 362
        Top = 4
      end
      object RzToolButton10: TRzToolButton
        Left = 286
        Top = 4
        Width = 76
        DropDownMenu = Print
        ImageIndex = 5
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        ToolStyle = tsDropDown
        Caption = #25253#34920
      end
    end
    object Grid: TcxGrid
      Left = 1
      Top = 32
      Width = 670
      Height = 415
      Align = alClient
      TabOrder = 1
      object tvTable: TcxGridDBTableView
        PopupMenu = pm
        OnDblClick = tvTableDblClick
        Navigator.Buttons.CustomButtons = <>
        OnEditing = tvTableEditing
        OnEditKeyDown = tvTableEditKeyDown
        DataController.DataSource = ds
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        FilterRow.Visible = True
        OptionsBehavior.FocusCellOnTab = True
        OptionsBehavior.FocusFirstCellOnNewRecord = True
        OptionsBehavior.GoToNextCellOnEnter = True
        OptionsBehavior.IncSearch = True
        OptionsBehavior.FocusCellOnCycle = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsCustomize.GroupBySorting = True
        OptionsData.CancelOnExit = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsSelection.InvertSelect = False
        OptionsSelection.MultiSelect = True
        OptionsSelection.CellMultiSelect = True
        OptionsView.DataRowHeight = 23
        OptionsView.Footer = True
        OptionsView.GroupByBox = False
        OptionsView.HeaderHeight = 20
        OptionsView.Indicator = True
        OptionsView.IndicatorWidth = 20
        Styles.OnGetContentStyle = tvTableStylesGetContentStyle
        object tvCol1: TcxGridDBColumn
          Caption = #24207#21495
          PropertiesClassName = 'TcxTextEditProperties'
          Properties.Alignment.Horz = taCenter
          OnGetDisplayText = tvCol1GetDisplayText
          HeaderAlignmentHorz = taCenter
          Options.Moving = False
          Width = 40
        end
        object tvTableColumn5: TcxGridDBColumn
          Caption = #29366#24577
          DataBinding.FieldName = 'status'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.OnChange = tvTableColumn5PropertiesChange
          HeaderAlignmentHorz = taCenter
          Options.Moving = False
          Width = 40
        end
        object tvTableColumn4: TcxGridDBColumn
          Caption = #32534#21495
          DataBinding.FieldName = 'Numbers'
          Visible = False
          HeaderAlignmentHorz = taCenter
          Options.Moving = False
          Width = 111
        end
        object tvTableColumn2: TcxGridDBColumn
          Caption = #24037#21495
          DataBinding.FieldName = 'PayNum'
          HeaderAlignmentHorz = taCenter
          Options.Moving = False
          Width = 80
        end
        object tvCol2: TcxGridDBColumn
          Caption = #22995#21517
          DataBinding.FieldName = 'Contacts'
          PropertiesClassName = 'TcxLookupComboBoxProperties'
          Properties.DropDownWidth = 200
          Properties.ImmediateDropDownWhenActivated = True
          Properties.KeyFieldNames = 'SignName'
          Properties.ListColumns = <
            item
              FieldName = 'SignName'
            end
            item
              FieldName = 'PYCode'
            end>
          Properties.ListOptions.ShowHeader = False
          Properties.ListSource = DM.DataSignName
          Properties.OnCloseUp = tvCol2PropertiesCloseUp
          HeaderAlignmentHorz = taCenter
          Options.Moving = False
          Width = 114
        end
        object tvTableColumn1: TcxGridDBColumn
          Caption = #24615#21035
          DataBinding.FieldName = 'Sex'
          PropertiesClassName = 'TcxComboBoxProperties'
          Properties.ImmediateDropDownWhenActivated = True
          Properties.Items.Strings = (
            #30007
            #22899)
          OnGetPropertiesForEdit = tvTableColumn1GetPropertiesForEdit
          HeaderAlignmentHorz = taCenter
          Options.Moving = False
          Width = 40
        end
        object tvTableColumn3: TcxGridDBColumn
          Caption = #37096#38376
          DataBinding.FieldName = 'sectione'
          PropertiesClassName = 'TcxComboBoxProperties'
          Properties.ImmediateDropDownWhenActivated = True
          RepositoryItem = DM.section
          OnGetPropertiesForEdit = tvTableColumn3GetPropertiesForEdit
          HeaderAlignmentHorz = taCenter
          Options.Moving = False
          Width = 60
        end
        object tvCol3: TcxGridDBColumn
          Caption = #32844#21153
          DataBinding.FieldName = 'duties'
          RepositoryItem = DM.JobBox
          HeaderAlignmentHorz = taCenter
          Options.Moving = False
          Width = 81
        end
        object tvTableColumn6: TcxGridDBColumn
          Caption = #24037#36164#31867#22411
          DataBinding.FieldName = 'wagestype'
          PropertiesClassName = 'TcxComboBoxProperties'
          Properties.ImmediateDropDownWhenActivated = True
          Properties.Items.Strings = (
            #24180#24037#36164
            #26376#24037#36164
            #26085#24037#36164)
          Options.Moving = False
          Width = 60
        end
        object tvTableColumn9: TcxGridDBColumn
          Caption = #26368#23569#22825#25968'('#26376')'
          DataBinding.FieldName = 'LeastWork'
          Options.Moving = False
          Width = 77
        end
        object tvTableColumn7: TcxGridDBColumn
          Caption = #21333#20215
          DataBinding.FieldName = 'UnitPrice'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Options.Moving = False
          Width = 84
        end
        object tvTableColumn8: TcxGridDBColumn
          Caption = #21152#29677#21333#20215
          DataBinding.FieldName = 'OverPrice'
          PropertiesClassName = 'TcxCurrencyEditProperties'
          Options.Moving = False
          Width = 60
        end
        object tvCol5: TcxGridDBColumn
          Caption = #25163#26426#21495#30721
          DataBinding.FieldName = 'Mobile'
          PropertiesClassName = 'TcxTextEditProperties'
          HeaderAlignmentHorz = taCenter
          Options.Moving = False
          Width = 94
        end
        object tvCol4: TcxGridDBColumn
          Caption = #22266#23450#30005#35805
          DataBinding.FieldName = 'fixedTel'
          PropertiesClassName = 'TcxTextEditProperties'
          HeaderAlignmentHorz = taCenter
          Options.Moving = False
          Width = 96
        end
        object tvCol7: TcxGridDBColumn
          Caption = #22320#22336
          DataBinding.FieldName = 'address'
          PropertiesClassName = 'TcxTextEditProperties'
          Options.Moving = False
          Width = 100
        end
        object tvCol6: TcxGridDBColumn
          Caption = #37038#31665
          DataBinding.FieldName = 'Mailbox'
          PropertiesClassName = 'TcxTextEditProperties'
          Options.Moving = False
          Width = 99
        end
        object tvCol8: TcxGridDBColumn
          Caption = #22791#27880
          DataBinding.FieldName = 'Remarks'
          PropertiesClassName = 'TcxMemoProperties'
          Options.Moving = False
          Width = 130
        end
      end
      object Lv: TcxGridLevel
        GridView = tvTable
      end
    end
  end
  object cxSplitter3: TcxSplitter
    Left = 274
    Top = 0
    Width = 8
    Height = 447
    HotZoneClassName = 'TcxXPTaskBarStyle'
    HotZone.SizePercent = 61
    AutoSnap = True
    ResizeUpdate = True
  end
  object qry: TADOQuery
    Connection = DM.ADOconn
    AfterOpen = qryAfterOpen
    AfterInsert = qryAfterInsert
    Parameters = <>
    Left = 240
    Top = 80
  end
  object ds: TDataSource
    DataSet = qry
    Left = 240
    Top = 136
  end
  object ds1: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 320
    Top = 136
  end
  object Print: TPopupMenu
    Images = DM.cxImageList1
    Left = 416
    Top = 136
    object Excel1: TMenuItem
      Caption = #36755#20986'Excel'
      OnClick = Excel1Click
    end
    object N2: TMenuItem
      Caption = #25171#21360#25253#34920
      ImageIndex = 6
      OnClick = N2Click
    end
  end
  object pm: TPopupMenu
    Images = DM.cxImageList1
    Left = 498
    Top = 136
    object N1: TMenuItem
      Caption = #26032#22686
      ImageIndex = 37
      OnClick = N1Click
    end
    object N3: TMenuItem
      Caption = #32534#36753
      ImageIndex = 0
      OnClick = N3Click
    end
    object N4: TMenuItem
      Caption = #20445#23384
      ImageIndex = 3
      OnClick = N4Click
    end
    object N5: TMenuItem
      Caption = #21024#38500
      ImageIndex = 51
      OnClick = N5Click
    end
    object N6: TMenuItem
      Caption = #36864#20986
      ImageIndex = 8
      OnClick = N6Click
    end
  end
end
