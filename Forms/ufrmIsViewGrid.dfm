object frmIsViewGrid: TfrmIsViewGrid
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = #34920#26684#35774#32622
  ClientHeight = 482
  ClientWidth = 321
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid2: TcxGrid
    Left = 8
    Top = 8
    Width = 305
    Height = 466
    TabOrder = 0
    object cxGrid2DBTableView1: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = DataSource2
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Inserting = False
      OptionsView.NoDataToDisplayInfoText = '<'#26242#26080#25968#25454#26174#31034'>'
      OptionsView.GroupByBox = False
      OptionsView.HeaderHeight = 28
      OptionsView.Indicator = True
      object cxGrid2DBTableView1Column2: TcxGridDBColumn
        Caption = #24207#21495
        OnGetDisplayText = cxGrid2DBTableView1Column2GetDisplayText
        HeaderAlignmentHorz = taCenter
        Options.Filtering = False
        Options.Focusing = False
        Options.Sorting = False
        Width = 50
      end
      object cxGrid2DBTableView1Column1: TcxGridDBColumn
        Caption = #21015#21517
        DataBinding.FieldName = 'Caption'
        HeaderAlignmentHorz = taCenter
        Options.Filtering = False
        Options.FilteringAddValueItems = False
        Options.FilteringFilteredItemsList = False
        Options.FilteringMRUItemsList = False
        Options.FilteringPopup = False
        Options.FilteringPopupMultiSelect = False
        Options.FilteringWithFindPanel = False
        Width = 154
      end
      object cxGrid2DBTableView1Column3: TcxGridDBColumn
        Caption = #26159#21542#26174#31034
        DataBinding.FieldName = 'IsView'
        HeaderAlignmentHorz = taCenter
        Options.Filtering = False
        Options.FilteringAddValueItems = False
        Options.FilteringFilteredItemsList = False
        Options.FilteringMRUItemsList = False
        Options.FilteringPopup = False
        Options.FilteringPopupMultiSelect = False
        Options.FilteringWithFindPanel = False
        Width = 59
      end
    end
    object cxGrid2Level1: TcxGridLevel
      GridView = cxGrid2DBTableView1
    end
  end
  object ADOQuery2: TADOQuery
    Connection = DM.ADOconn
    Parameters = <>
    Left = 41
    Top = 56
  end
  object DataSource2: TDataSource
    DataSet = ADOQuery2
    Left = 113
    Top = 56
  end
end
