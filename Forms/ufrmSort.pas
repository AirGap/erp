unit ufrmSort;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, TreeUtils,RzTabs, ExtCtrls, ComCtrls, RzButton, StdCtrls, ActnList, Menus,
  ImgList, Grids, DBGrids, Mask, RzEdit, RzSpnEdt, Buttons, DB, ADODB,
  System.ImageList, RzPanel, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, dxSkinsCore, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, cxSplitter, cxStyles, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, cxDBData, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxTextEdit, cxMemo,FillThrdTree,UtilsTree,ufrmBaseController,
  cxCheckBox, cxDropDownEdit, cxCurrencyEdit,ufunctions, cxDBLookupComboBox,
  Datasnap.DBClient;

type
  TfrmSort = class(TfrmBaseController)
    RzPanel1: TRzPanel;
    RzToolbar3: TRzToolbar;
    RzSpacer16: TRzSpacer;
    RzToolButton1: TRzToolButton;
    RzSpacer17: TRzSpacer;
    RzToolButton2: TRzToolButton;
    RzSpacer18: TRzSpacer;
    RzToolButton3: TRzToolButton;
    RzPanel2: TRzPanel;
    RzToolbar2: TRzToolbar;
    RzSpacer5: TRzSpacer;
    RzToolButton7: TRzToolButton;
    RzToolButton8: TRzToolButton;
    RzSpacer9: TRzSpacer;
    RzToolButton9: TRzToolButton;
    RzSpacer10: TRzSpacer;
    RzToolButton12: TRzToolButton;
    RzSpacer15: TRzSpacer;
    cxSplitter3: TcxSplitter;
    tvTable: TcxGridDBTableView;
    Lv: TcxGridLevel;
    Grid: TcxGrid;
    tvCol1: TcxGridDBColumn;
    tvCol2: TcxGridDBColumn;
    tvCol4: TcxGridDBColumn;
    tvCol5: TcxGridDBColumn;
    tvCol7: TcxGridDBColumn;
    tvCol3: TcxGridDBColumn;
    tvCol6: TcxGridDBColumn;
    tvCol8: TcxGridDBColumn;
    tv: TTreeView;
    RzSpacer1: TRzSpacer;
    RzToolButton4: TRzToolButton;
    RzSpacer2: TRzSpacer;
    RzToolButton5: TRzToolButton;
    qry: TADOQuery;
    ds: TDataSource;
    tvTableColumn1: TcxGridDBColumn;
    tvTableColumn2: TcxGridDBColumn;
    tvTableColumn3: TcxGridDBColumn;
    tvTableColumn4: TcxGridDBColumn;
    tvTableColumn5: TcxGridDBColumn;
    tvTableColumn6: TcxGridDBColumn;
    tvTableColumn7: TcxGridDBColumn;
    tvTableColumn8: TcxGridDBColumn;
    tvTableColumn9: TcxGridDBColumn;
    RzSpacer3: TRzSpacer;
    RzToolButton6: TRzToolButton;
    ds1: TClientDataSet;
    RzSpacer4: TRzSpacer;
    RzToolButton10: TRzToolButton;
    Print: TPopupMenu;
    Excel1: TMenuItem;
    N2: TMenuItem;
    pm: TPopupMenu;
    N1: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    procedure tvCol1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure RzToolButton1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure RzToolButton12Click(Sender: TObject);
    procedure tvChange(Sender: TObject; Node: TTreeNode);
    procedure tvCancelEdit(Sender: TObject; Node: TTreeNode);
    procedure tvEdited(Sender: TObject; Node: TTreeNode; var S: string);
    procedure RzToolButton4Click(Sender: TObject);
    procedure RzToolButton5Click(Sender: TObject);
    procedure tvDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure tvDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure RzToolButton7Click(Sender: TObject);
    procedure qryAfterInsert(DataSet: TDataSet);
    procedure tvTableEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure tvTableEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure tvTableDblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure tvTableStylesGetContentStyle(Sender: TcxCustomGridTableView;
      ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
      var AStyle: TcxStyle);
    procedure tvTableColumn5PropertiesChange(Sender: TObject);
    procedure tvTableColumn1GetPropertiesForEdit(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AProperties: TcxCustomEditProperties);
    procedure tvTableColumn3GetPropertiesForEdit(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AProperties: TcxCustomEditProperties);
    procedure tvKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure qryAfterOpen(DataSet: TDataSet);
    procedure tvCol2PropertiesCloseUp(Sender: TObject);
    procedure RzToolButton6Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure Excel1Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure N5Click(Sender: TObject);
    procedure N6Click(Sender: TObject);
  private
    { Private declarations }
    g_TreeView : TNewUtilsTree;
    g_SelectNodeId : Integer;
    Prefix : string;
    Suffix : string;

    procedure TreeAddName(Tv : TTreeView); //树增加名称
    procedure UpDateIndex(lpNode : TTreeNode);
    function GetTreeIndexList(Anode: TTreeNode;var s : string):string;stdcall;
    function IstreeEdit(S : string ; Node: TTreeNode):Boolean;
    function DeleteTableFile(AtableList : array of TTableList ; lpSignName : string ;
                             IsFullDelete : Boolean):Boolean;
    function IsExistName(lpName : string):Boolean;
  public
    { Public declarations }
  end;

var
  frmSort: TfrmSort;
  g_DirSort : string = 'Sort';

implementation

uses
   global,uDataModule;

{$R *.dfm}

function ExecSQLText(lpSql : string):Integer;
begin
  Result := 0;
  with DM.Qry do
  begin
    Close;
    SQL.Text := lpSql;
    Result := ExecSQL;
  end;
end;
//删除表中数据时清除所有关联附件与表中数据
function TfrmSort.DeleteTableFile(AtableList : array of TTableList ; lpSignName : string ;
IsFullDelete : Boolean):Boolean;
var
  s : Integer;
  i : Integer;
  szTableName : string;
  szDirectory : string;
  szPath : string;
  sqlstr : string;

begin
   Result := True;
   for I := 0 to High(AtableList) do
   begin
     szTableName := AtableList[i].ATableName;
     szDirectory := AtableList[i].ADirectory;
     if szTableName <> '' then
     begin
         if IsFullDelete then
         begin
           //附件全删除
           if szDirectory <> '' then
           begin
             szPath := Concat(g_Resources,'\' + szDirectory );
             if DirectoryExists(szPath) then DeleteDirectory(szPath);
           end;
           sqlstr := 'delete * from '+ szTableName +' where TreeId in('+ lpSignName  +')';
           if ExecSQLText(sqlstr) <> 0 then
         end else
         begin
           if (Length(szTableName ) <> 0) and (Length(szDirectory) <> 0) then
           begin
             //查询附件路径
             sqlstr := 'select * from ' + szTableName +' where SignName in ("' + lpSignName +'")';
              with DM.Qry do
              begin
                Close;
                SQL.Clear;
                SQL.Text := sqlstr;
                Open;

                if 0 <> RecordCount then
                begin

                  while not Eof do
                  begin
                    szPath := Concat(g_Resources,'\' + szDirectory + '\' + FieldByName('numbers').AsString );
                    if DirectoryExists(szPath) then
                    begin
                      DeleteDirectory(szPath);
                    end;
                    Next;
                  end;

                end;

              end;

           end;

           //清理数据
           sqlstr := 'delete * from '+ szTableName +' where SignName in('''+ lpSignName  +''')';
           if ExecSQLText(sqlstr) <> 0 then

         end;
     end;

   end;

end;

procedure TfrmSort.TreeAddName(Tv : TTreeView); //树增加名称
var
  Node , ChildNode : TTreeNode;
  s : string;

begin
  with Tv do
  begin
    Node := Selected;
    if Assigned(Node) then
    begin
    //  s := GetNewBHStr(g_Table_CompanyTree,'ID','SIGN',10);
      if Node.Level = 0 then
      begin
        g_TreeView.AddChildNode(s,'',Date,m_OutText);
      end else
      begin
        ShowMessage('只能建立一个顶级公司!!');
      end;

    end;

  end;

end;

procedure TfrmSort.FormCreate(Sender: TObject);
begin
  inherited;
  Prefix := 'YG-';
  Suffix := '01001';
end;

procedure TfrmSort.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = 27  then  Close;
end;

function TfrmSort.GetTreeIndexList(Anode: TTreeNode;var s : string):string;stdcall;
var
  i: integer;
  node: TTreeNode;
  szData : PNodeData;
  szCodeList : string;

begin
  if Assigned(Anode) then
  begin
    szData := PNodeData(ANode.Data);
    s := IntToStr( szData.Index );
    if Anode.Count <> 0 then
    begin
      getSontreeIndex(Anode,szCodeList);
      Result := szCodeList;
    end
    else
    begin
      Result := s;
    end;

    s := Result;
  end;

end;

procedure TfrmSort.Excel1Click(Sender: TObject);
begin
  inherited;
  CxGridToExcel(Self.Grid,'员工明细表');
end;

procedure TfrmSort.FormActivate(Sender: TObject);
begin

//公司信息：序号 状态  交易号  姓名  性别  部门  工种  电话   邮箱  地址  备注

//这些是显示的都有附件功能

  g_TreeView := TNewUtilsTree.Create(Self.tv,
                                     DM.ADOconn,
                                     g_Table_Maintain_CompanyInfoTree,
                                     '公司名称');
  g_TreeView.GetTreeTable(0);
end;

procedure TfrmSort.RzToolButton12Click(Sender: TObject);
begin
  Close;
end;

function InDeleteData(ATable,code : string):Integer;stdcall;
var
  s : string;
begin

  if Length(ATable) <>  0 then
  begin
    s := 'delete * from ' + ATable + ' where TreeId =' + Code  + '';
    with  DM.Qry do
    begin
      Close;
      SQL.Clear;
      SQL.Text := s;
      Result   := ExecSQL;
    end;

  end;

end;

procedure TfrmSort.RzToolButton1Click(Sender: TObject);
var
  Node : TTreeNode;
  szCodeList : string;
  szColName  : string;
  szTableList : array[0..5] of TTableList;

begin
  if sender  = Self.RzToolButton1 then
  begin
    //添加
    TreeAddName(Self.tv);
  end else
  if Sender = Self.RzToolButton2 then
  begin
    //编辑
    Self.tv.Selected.EditText;
  end else
  if Sender = Self.RzToolButton3 then
  begin
    //删除
    if MessageBox(handle, '是否删除公司节点？', '提示', MB_ICONQUESTION
    + MB_YESNO) = IDYES then
    begin
      Node := Self.Tv.Selected;
      if Assigned(Node) then
      begin
        GetTreeIndexList(Node,szCodeList);
        //删除考勤
        szTableList[0].ATableName := g_Table_Company_Staff_CheckWork;
        szTableList[0].ADirectory := '';

        szTableList[1].ATableName := g_Table_Company_Staff_Over ;
        szTableList[1].ADirectory := '';

        szTableList[2].ATableName := g_Table_Company_Prize;
        szTableList[2].ADirectory := g_Dir_Company_Prize ;

        szTableList[3].ATableName := g_Table_Company_buckle;
        szTableList[3].ADirectory := g_Dir_Company_buckle;

        szTableList[4].ATableName := g_Table_Company_BlendProjectRecord;
        szTableList[4].ADirectory := '';

        szTableList[5].ATableName := g_Table_Company_BlendStaff;
        szTableList[5].ADirectory := '';

        DeleteTableFile(szTableList,szCodeList,true);
        InDeleteData(g_Table_Maintain_CompanyInfoList,szCodeList);
        g_TreeView.DeleteTree(Node);
        Self.qry.Requery();

      end;
    end;

  end;

end;

procedure TfrmSort.RzToolButton4Click(Sender: TObject);
var
  PrevNode , NextNode , SourceNode , TargetNode : TTreeNode;
begin
  inherited;
  PrevNode   := Tv.Selected.getPrevSibling;
  NextNode   := Tv.Selected.getNextSibling;

  SourceNode := Tv.Selected;
  if (PrevNode.Index <> -1) then
  begin
    if NextNode <> nil then
      PrevNode.MoveTo(NextNode, naInsert)
    else
      PrevNode.MoveTo(SourceNode, naAdd);
  end;

  SourceNode := Tv.Selected; //中
  UpDateIndex(SourceNode);
//  PrevNode   := Tv.Selected.getPrevSibling;//上
//  UpDateIndex(PrevNode);
  NextNode   := Tv.Selected.getNextSibling;//下
  UpDateIndex(NextNode);
end;

procedure TfrmSort.RzToolButton5Click(Sender: TObject);
var
  OldNode , Node , SourceNode ,PrevNode: TTreeNode;
begin
  inherited;
  Node := Self.Tv.Selected;
  OldNode := Node.getNextSibling;
  if OldNode.Index <> -1 then
  begin

    OldNode.MoveTo(Node, naInsert);

    SourceNode := Tv.Selected; //中
    UpDateIndex(SourceNode);
    PrevNode   := Tv.Selected.getPrevSibling;//上
    UpDateIndex(PrevNode);
  //  NextNode   := Tv.Selected.getNextSibling;//下
  //  UpDateIndex(NextNode);
  end;
end;

procedure TfrmSort.RzToolButton6Click(Sender: TObject);
begin
  inherited;
  ADOIsEdit(Self.tvTableColumn5.EditValue,Self.qry);
end;

procedure TfrmSort.RzToolButton7Click(Sender: TObject);
var
  szColName : string;
  Code : string;
  I: Integer;
  szCount : Integer;
  s : Variant;
  szTableList : array[0..5] of TTableList;
begin
  inherited;
  if Sender = Self.RzToolButton7 then
  begin
    with Self.qry do
    begin
      if State <> dsInactive then
      begin
        Append;
        Self.tvTable.Columns[4].Focused := True;
        Self.Grid.SetFocus;
        keybd_event(VK_RETURN,0,0,0);
      end else
      begin
        Application.MessageBox( '当前查询状态无效，先选择一个分类在进行操作？', '提示:', MB_OKCANCEL + MB_ICONWARNING)
      end;
    end;

  end else
  if Sender = Self.RzToolButton8 then
  begin
    IsDeleteEmptyData(Self.qry ,
                      g_Table_Maintain_CompanyInfoList ,
                      Self.tvCol2.DataBinding.FieldName);

  end else
  if Sender = Self.RzToolButton9 then
  begin
    //删除考勤
    if Application.MessageBox( '是否需要要彻底删除数据，删除后无法恢复？', '提示:',
    MB_OKCANCEL + MB_ICONWARNING) = IDOK then
    begin

      szTableList[0].ATableName := g_Table_Company_Staff_CheckWork;
      szTableList[0].ADirectory := '';

      szTableList[1].ATableName := g_Table_Company_Staff_Over ;
      szTableList[1].ADirectory := '';

      szTableList[2].ATableName := g_Table_Company_Prize;
      szTableList[2].ADirectory := g_Dir_Company_Prize ;

      szTableList[3].ATableName := g_Table_Company_buckle;
      szTableList[3].ADirectory := g_Dir_Company_buckle;

      szTableList[4].ATableName := g_Table_Company_BlendStaff;
      szTableList[4].ADirectory := '';

      szTableList[5].ATableName := g_Table_Company_BlendProjectRecord;
      szTableList[5].ADirectory := '';

      with Self.tvTable do
      begin
        szCount := Self.tvTable.Controller.SelectedRecordCount;
        for I := szCount -1 downto 0 do
        begin
          s := Controller.SelectedRows[i].Values[Self.tvCol2.Index];
          if s <> null then
          begin
            DeleteTableFile(szTableList,VarToStr( s ),False);
          end;

        end;

      end;
      DeleteSelection(Self.tvTable,'',g_Table_Maintain_CompanyInfoList,'');

    end;
    {
    szColName := Self.tvTableColumn4.DataBinding.FieldName;
    DM.ADOconn.BeginTrans;
    try

      with Self.tvTable.Controller do
      begin
        szCount := SelectedRowCount;
        for I := szCount -1 downto 0 do
        begin
        
          s := SelectedRows[i].Values[Self.tvTableColumn4.Index];
          Code := VarToStr(s);
          if Length( Code ) <> 0 then
          begin
            if Length( Code ) = 0 then
              s := Code
            else
              Code := s + '","' +Code;
          end;

        end;
      
      end;

      if Code <> '' then
      begin
        with DM.Qry do
        begin
          Close;
          SQL.Text :=  'delete * from '+ g_Table_Company_Staff_CheckWork +' where '+ szColName +' in('''+ Code  +''')';
          ExecSQL;
          SQL.Text :=  'delete * from '+ g_Table_Company_Staff_Over +' where '+ szColName +' in('''+ Code  +''')';
          ExecSQL;
          SQL.Text :=  'delete * from '+ g_Table_Company_Prize +' where '+ szColName +' in('''+ Code  +''')';
          ExecSQL;
          SQL.Text :=  'delete * from '+ g_Table_Company_buckle +' where '+ szColName +' in('''+ Code  +''')';
          ExecSQL;
        end;
        

      end else
      begin
        ShowMessage('员工ID为空删除失败');
      end;   
      DM.ADOconn.CommitTrans;
    except
      DM.ADOconn.RollbackTrans;
    end;
    }
    Self.qry.Requery();
  end;
end;

procedure TfrmSort.UpDateIndex(lpNode : TTreeNode);
var
  s : string;
begin
  if lpNode <> nil then
  begin
    S := 'UPDATE ' + g_Table_Maintain_CompanyInfoTree + ' SET TreeIndex=''' + IntToStr(lpNode.Index) + ''' WHERE SignName="' + lpNode.Text + '"';
    with DM.Qry do
    begin
      Close;
      SQL.Clear;
      SQL.Text := s;
      ExecSQL;
    end;
  end;
end;

function TfrmSort.IstreeEdit(S : string ; Node: TTreeNode):Boolean;
var
  szCodeList : string;
  PrevNode , NextNode : TTreeNode;

begin
  if s = m_OutText then
  begin
    treeCldnode(Node,szCodeList);
    g_TreeView.DeleteTree(Node);
  end else
  begin
    g_TreeView.ModifyNodeSignName(s,Node);
    UpDateIndex(Node);
    PrevNode   := Tv.Selected.getPrevSibling;//上
    if PrevNode.Index <> -1 then UpDateIndex(PrevNode);
    NextNode   := Tv.Selected.getNextSibling;//下
    if NextNode.Index <> -1 then UpDateIndex(NextNode);
  end;
end;

procedure TfrmSort.N1Click(Sender: TObject);
begin
  inherited;
  RzToolButton7.Click;
end;

procedure TfrmSort.N2Click(Sender: TObject);
begin
  inherited;
  DM.BasePrinterLink1.Component := Self.Grid;
  DM.BasePrinterLink1.ReportTitleText := '';
  DM.BasePrinter.Preview(True, nil);
end;

procedure TfrmSort.N3Click(Sender: TObject);
begin
  inherited;
  RzToolButton6.Click;
end;

procedure TfrmSort.N4Click(Sender: TObject);
begin
  inherited;
  RzToolButton8.Click;
end;

procedure TfrmSort.N5Click(Sender: TObject);
begin
  inherited;
  RzToolButton9.Click;
end;

procedure TfrmSort.N6Click(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TfrmSort.qryAfterInsert(DataSet: TDataSet);
var
  szCode : string;
  szColName : string;
begin
  inherited;
  with DataSet do
  begin
    if State <> dsInactive then
    begin
      FieldByName('TreeId').Value := g_SelectNodeId;
      szCode := Prefix + GetRowCode( g_Table_Maintain_CompanyInfoList ,
                                     Self.tvTableColumn4.DataBinding.FieldName ,
                                     Suffix ,
                                     01001);
      szColName := Self.tvTableColumn4.DataBinding.FieldName;
      FieldByName(szColName).Value := szCode;

      szCode := GetUserCode(g_Table_Maintain_CompanyInfoList);
      szColName := Self.tvTableColumn2.DataBinding.FieldName;
      FieldByName(szColName).Value := szCode;
      szColName := Self.tvTableColumn5.DataBinding.FieldName;
      FieldByName(szColName).Value := True;

    end;
  end;
end;

procedure TfrmSort.qryAfterOpen(DataSet: TDataSet);
begin
  inherited;
  TADOQuery(DataSet).Properties.Get_Item('Update Criteria').Value :=0;
end;

procedure TfrmSort.tvCancelEdit(Sender: TObject; Node: TTreeNode);
begin
//  IstreeEdit(Node.Text,Node);
end;

procedure TfrmSort.tvChange(Sender: TObject; Node: TTreeNode);
var
  s : string;
  PData: PNodeData;
  szRowCount : Integer;
begin
  inherited;
  if Node.Level >= 1 then
  begin
    PData := PNodeData(Node.Data);
    if Assigned(PData) then
    begin
      g_SelectNodeId := PData^.Index;

      with Self.qry do
      begin
        Close;
        SQL.Clear;
        SQL.Text := 'Select * from '+
                    g_Table_Maintain_CompanyInfoList +
                    ' WHERE TreeId=' + IntToStr(g_SelectNodeId);
        Open;
      end;

    end;

  end;

end;


procedure TfrmSort.tvDragDrop(Sender, Source: TObject; X, Y: Integer);
var
  TargetNode, SourceNode: TTreeNode;
begin
  inherited;
  TargetNode := tv.DropTarget;
  SourceNode := tv.Selected;
  if MessageBox(handle, '您确认要移动合同吗？', '提示', MB_ICONQUESTION
    + MB_YESNO) = IDYES then
    if g_TreeView.ModifyNodePID(PNodeData(TargetNode.Data)^.Index, SourceNode) then
       g_TreeView.GetTreeTable(0);
end;

procedure TfrmSort.tvDragOver(Sender, Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
begin
  inherited;
  if (Source = Self.Tv) then
     Accept := True
  else
     Accept := False;
end;

procedure TfrmSort.tvEdited(Sender: TObject; Node: TTreeNode; var S: string);
begin
  inherited;
  IstreeEdit(s,Node);
end;

procedure TfrmSort.tvKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  case Key of
    13:
    begin
      Self.tv.Selected.Parent.Selected := True;
      Self.RzToolButton1.Click;
    end;
    46:
    begin
      Self.RzToolButton3.Click;
    end;
  end;
end;

procedure TfrmSort.tvCol1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  AText := IntToStr(ARecord.Index + 1);
end;

function TfrmSort.IsExistName(lpName : string):Boolean;
var
  DD1 : string;
  i : Integer;
  szIsExist:Boolean;

begin
  szIsExist  := False;
  with Self.tvTable do
  begin

    for I := 0 to ViewData.RowCount-1 do
    begin
      DD1 := VarToStr( ViewData.Rows[i].Values[Self.tvCol2.Index] ); //姓名
      if ( DD1 = lpName) and (i <> Controller.FocusedRowIndex) then
      begin
        szIsExist := True;
        Self.tvCol2.EditValue := null;
        Self.tvCol2.Editing;
        Application.MessageBox(PWideChar(lpName + ' 已经存在' ), '？', MB_OKCANCEL + MB_ICONWARNING);
        Break;
      end;
    end;

  end;
  Result := szIsExist;
end;

procedure TfrmSort.tvCol2PropertiesCloseUp(Sender: TObject);
var
  szRowIndex : Integer;
  szCol1Name : string;
begin
  inherited;
  with (Sender as TcxLookupComboBox).Properties.Grid do
  begin
    szRowIndex := FocusedRowIndex;
    if szRowIndex >= 0 then
    begin
      szCol1Name := VarToStr( DataController.Values[szRowIndex, 0] ); //姓名
      ////////////////////////////////////////////////////////////////////////
      IsExistName(szCol1Name);
    end;
  end;
end;

procedure TfrmSort.tvTableColumn1GetPropertiesForEdit(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AProperties: TcxCustomEditProperties);
begin
  inherited;
  {
  with (AProperties AS TcxComboBoxProperties) do
  begin
    Items.Clear;
    Items.Add('男');
    Items.Add('女')
  end;
  }
end;

procedure TfrmSort.tvTableColumn3GetPropertiesForEdit(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AProperties: TcxCustomEditProperties);
begin
  inherited;
  {
  DM.Qry.Close;
  DM.Qry.SQL.Clear;
  DM.Qry.SQL.Text := 'Select * from ' + g_Table_Maintain_section;
  DM.Qry.Open;
  if DM.Qry.RecordCount <> 0 then
  begin
    with (AProperties AS TcxComboBoxProperties) do
    begin
      Items.Clear;
      while Not DM.Qry.Eof do
      begin
        Items.Add(DM.Qry.Fields[1].AsString);
        DM.Qry.Next;

      end;

    end;

  end;
  }
end;

procedure TfrmSort.tvTableColumn5PropertiesChange(Sender: TObject);
var
  szRowIndex : Integer;

begin
  inherited;
  szRowIndex := Self.tvTable.Controller.FocusedRowIndex;
  Self.qry.UpdateBatch(arAll);
  Self.qry.Requery();
  Self.tvTable.DataController.FocusedRowIndex := szRowIndex;
end;

procedure TfrmSort.tvTableDblClick(Sender: TObject);
var
  szDir : string;
begin
  inherited;
  szDir := Concat(g_Resources,'\'+ g_DirSort);
  CreateEnclosure(Self.tvTable.DataController.DataSource.DataSet,Handle,szDir);
end;

procedure TfrmSort.tvTableEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
var
  szColName : string;
begin
  inherited;
  AAllow := ADOBanEditing(Self.qry);
  {
  with Self.qry do
  begin
    if State <> dsInactive then
      begin
        if FieldByName(szColName).Value = False then
        begin
          AAllow := False;
        end else
        begin
          AAllow := True;
        end;

      end;
  end;
  }
  {
  szColName := Self.tvTableColumn5.DataBinding.FieldName;
  if AItem.Index <> Self.tvTableColumn5.Index then
  begin

  end;
  }

    if (AItem.Index = Self.tvCol1.Index) or
     (AItem.Index = Self.tvTableColumn4.Index) then
  begin
    AAllow := False;
  end;
end;

procedure TfrmSort.tvTableEditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = 13 then
  begin
    if AItem.Index = Self.tvCol7.Index then
    begin
      if Self.tvTable.Controller.FocusedRow.IsLast then
      begin
        with Self.qry do
        begin
          if State <> dsInactive then
          begin
            Append;
            Self.tvTable.Columns[3].Focused := True;
          end;
        end;
      end;
    end;
  end;

end;

procedure TfrmSort.tvTableStylesGetContentStyle(Sender: TcxCustomGridTableView;
  ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
  var AStyle: TcxStyle);
var
  szIndex : Integer;
  Value : Variant;
begin
  inherited;
  Value := ARecord.Values[Self.tvTableColumn5.Index];
  if Value = False then
  begin
    AStyle := DM.cxStyle222;
  end;
end;

end.
