﻿unit ufrmCalcTool;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, Vcl.Menus, Vcl.StdCtrls,
  cxButtons, cxLabel, cxDBEdit, cxMemo, cxDropDownEdit, cxTextEdit, cxMaskEdit,
  cxSpinEdit, cxGroupBox, Vcl.ExtCtrls, RzPanel,ufrmBaseController, Data.DB,
  dxmdaset;

type
  TfrmCalcTool = class(TfrmBaseController)
    RzPanel3: TRzPanel;
    Bevel1: TBevel;
    cxGroupBox4: TcxGroupBox;
    Label23: TLabel;
    Label24: TLabel;
    Label26: TLabel;
    Label3: TLabel;
    cxSpinEdit1: TcxSpinEdit;
    cxSpinEdit2: TcxSpinEdit;
    cxSpinEdit5: TcxSpinEdit;
    cxComboBox1: TcxComboBox;
    cxGroupBox5: TcxGroupBox;
    Label4: TLabel;
    Label2: TLabel;
    Label6: TLabel;
    cxLabel13: TcxLabel;
    cxLabel14: TcxLabel;
    cxComboBox2: TcxComboBox;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    CountTotal: TcxSpinEdit;
    cxMemo1: TcxMemo;
    VariableCount: TcxSpinEdit;
    NumericalValue: TcxSpinEdit;
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure cxButton5Click(Sender: TObject);
    procedure cxDBSpinEdit1PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure cxComboBox1PropertiesCloseUp(Sender: TObject);
    procedure cxSpinEdit1PropertiesChange(Sender: TObject);
    procedure cxDBSpinEdit2PropertiesChange(Sender: TObject);
    procedure cxDBSpinEdit3PropertiesChange(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure cxMemo1PropertiesChange(Sender: TObject);
    procedure cxComboBox2PropertiesCloseUp(Sender: TObject);
  private
    { Private declarations }
    Radix : Integer;
    function GetRadix(lpRadix : Integer):BOOL;
    procedure CalcValue();
  public
    { Public declarations }
    dwParentHandle : HWND;
  end;

var
  frmCalcTool: TfrmCalcTool;

implementation

uses
   global,System.Math;

{$R *.dfm}

procedure TfrmCalcTool.CalcValue();
var
  szWeight    : Double;   //重量
  szMeter     : Double;   //米数
  szRootCount : Double;   //根数
  szTheoryWeight : Double; //理论重量
  szPieceCount: Double;   //件数

begin
  inherited;
  szTheoryWeight:= Self.VariableCount.Value; //变量数
  szMeter       := Self.cxSpinEdit2.Value;
  szRootCount   := Self.cxSpinEdit5.Value;
  szPieceCount  := Self.cxSpinEdit1.Value;

  if (szRootCount <> 0) then
  begin
    szWeight := (szMeter * (szRootCount * szPieceCount) );
    szWeight := RoundTo( szWeight * szTheoryWeight ,  Radix );

    Self.NumericalValue.EditValue := szWeight; //计算数值编辑框
    Self.NumericalValue.PostEditValue;

    Self.CountTotal.EditValue := szWeight;// szWeight;
    Self.CountTotal.PostEditValue;
  end;

end;

function TfrmCalcTool.GetRadix(lpRadix : Integer):BOOL;
begin
  case lpRadix of
    0: Radix := -1;
    1: Radix := -2;
    2: Radix := -3;
    3: Radix := -4;
    4: Radix := -5;
    5: Radix := -6;
    6: Radix := -7;
    7: Radix := -8;
  end;

end;

procedure TfrmCalcTool.cxButton4Click(Sender: TObject);
//var
//  pMsg : TMakings;
begin
  inherited;
  Close;
  {
  pMsg.dwGoodsNo       := '';
  pMsg.dwMakingCaption := DBCol4.EditValue;;
  pMsg.dwModelIndex    := DBCol5.EditValue;
  pMsg.dwSpec := Self.DBCol6.EditValue;
  pMsg.dwBrand:= Self.DBCol15.EditValue;
  }
//  SendMessage(dwParentHandle, WM_LisView, 0, LPARAM(@pMsg));  // 发消息
end;

procedure TfrmCalcTool.cxButton5Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmCalcTool.cxComboBox1PropertiesCloseUp(Sender: TObject);
var
  i : Integer;
begin
  inherited;
  i := Self.cxComboBox1.ItemIndex;
  if i >= 0 then
  begin
    Self.VariableCount.EditValue := RebarList[i].Weight;// Format('0.#######;-0.#######',[RebarList[i].Weight] );
    Self.VariableCount.PostEditValue;
    CalcValue;
  end;
end;

procedure TfrmCalcTool.cxComboBox2PropertiesCloseUp(Sender: TObject);
begin
  inherited;
  GetRadix(Self.cxComboBox2.ItemIndex);
end;

procedure TfrmCalcTool.cxDBSpinEdit1PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  s : string;

begin
  inherited;
  if DisplayValue <> null then
  begin
    s := VarToStr(DisplayValue);
    Self.cxSpinEdit1.EditValue := RoundTo( StrToFloatDef(s,0) , Radix );
  // Self.cxSpinEdit1.PostEditValue;
  end;
end;

procedure TfrmCalcTool.cxDBSpinEdit2PropertiesChange(Sender: TObject);
var
  szValue : Variant;
  s : string;

begin
  inherited;
  szValue := Self.NumericalValue.EditingValue;

  if szValue <> null then
  begin
    s := VarToStr(szValue);

    Self.CountTotal.EditValue := RoundTo( StrToFloatDef(s,0)  * szValue , Radix );
  //  Self.cxSpinEdit1.PostEditValue;
  end;
end;

procedure TfrmCalcTool.cxDBSpinEdit3PropertiesChange(Sender: TObject);
var
  szValue : Variant;
  s : string;

begin
  inherited;
  szValue := Self.NumericalValue.EditingValue;
  if szValue <> null then
  begin
    s := VarToStr(szValue);
    Self.CountTotal.EditValue := RoundTo( StrToFloatDef(s,0) , Radix ) ;
  //  Self.cxSpinEdit1.PostEditValue;
  end;
end;

procedure TfrmCalcTool.cxMemo1PropertiesChange(Sender: TObject);
var
  s : Variant;
  szValue : string;
  i : Integer;
  f : Double;
begin
  inherited;
  s := (Sender as TcxMemo).EditingValue;
  if s <> null then
  begin
    szValue := FunExpCalc(s,2);

    if TryStrToInt(szValue,i) or TryStrToFloat(szValue, f) then
    begin
      Self.CountTotal.EditValue := RoundTo( StrToFloatDef( szValue , 0 ) , Radix );
      Self.NumericalValue.EditValue := szValue;
    end;
  end;
end;


procedure TfrmCalcTool.cxSpinEdit1PropertiesChange(Sender: TObject);
begin
  CalcValue();
end;

procedure TfrmCalcTool.FormActivate(Sender: TObject);
var
  i : Integer;
begin
  inherited;
  for I := 0 to High(RebarList) do Self.cxComboBox1.Properties.Items.Add( RebarList[i].Model );
  GetRadix(Self.cxComboBox2.ItemIndex);
  CountTotal.SetFocus;
end;

procedure TfrmCalcTool.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 27 then Close;
end;

end.
