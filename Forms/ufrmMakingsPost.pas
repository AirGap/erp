unit ufrmMakingsPost;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, cxLabel, cxTextEdit, cxDBEdit,
  cxCurrencyEdit, Vcl.Menus, Vcl.StdCtrls, cxButtons, Vcl.ExtCtrls, RzPanel,
  RzButton, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxNavigator, Data.DB, cxDBData, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, Datasnap.DBClient,UnitADO;

type
  TfrmMakingsPost = class(TForm)
    RzToolbar1: TRzToolbar;
    RzSpacer1: TRzSpacer;
    RzToolButton1: TRzToolButton;
    RzSpacer2: TRzSpacer;
    RzToolButton2: TRzToolButton;
    RzSpacer3: TRzSpacer;
    RzToolButton4: TRzToolButton;
    grp1: TGroupBox;
    cxLabel1: TcxLabel;
    cxDBTextEdit1: TcxDBTextEdit;
    cxLabel2: TcxLabel;
    cxDBTextEdit2: TcxDBTextEdit;
    cxLabel3: TcxLabel;
    cxDBTextEdit3: TcxDBTextEdit;
    cxLabel4: TcxLabel;
    cxDBTextEdit4: TcxDBTextEdit;
    cxLabel5: TcxLabel;
    cxDBTextEdit5: TcxDBTextEdit;
    cxLabel6: TcxLabel;
    cxDBTextEdit6: TcxDBTextEdit;
    cxLabel7: TcxLabel;
    cxLabel8: TcxLabel;
    cxLabel9: TcxLabel;
    cxLabel10: TcxLabel;
    cxDBTextEdit7: TcxDBTextEdit;
    cxDBTextEdit8: TcxDBTextEdit;
    dsMaster: TClientDataSet;
    DataSource: TDataSource;
    cxDBCurrencyEdit1: TcxDBCurrencyEdit;
    cxDBCurrencyEdit2: TcxDBCurrencyEdit;
    procedure FormActivate(Sender: TObject);
    procedure RzToolButton1Click(Sender: TObject);
    procedure RzToolButton4Click(Sender: TObject);
    procedure RzToolButton5Click(Sender: TObject);
    procedure RzToolButton2Click(Sender: TObject);
    procedure dsMasterAfterInsert(DataSet: TDataSet);
    procedure FormDestroy(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }

    dwADO : TADO;
    dwSQLText : string;
    function GetRowId(lpRowCount : Integer):string;
  public
    { Public declarations }
    dwTableName:string;
    dwNodeId : Integer;
    dwIsEdit : Boolean;
    dwGoodsNo: string;
  end;

var
  frmMakingsPost: TfrmMakingsPost;

implementation

{$R *.dfm}

uses
   uDataModule,global;

//查找行数
function GetRowCount(ATabName , AFieldName , APrefix : string):Integer;stdcall;
var
  i : Integer;
  s : string;
begin
  Result := 0;
  s := 'SELECT Count(*) FROM ' + ATabName + ' Where Sign_Id=' + APrefix + ' AND '+ AFieldName +' <> ""'  ;
  OutputLog(s);
  with DM.Qry do
  begin
    Close;
    SQL.Clear;
    SQL.Text := s;
    Open;
  //  Sort := AFieldName + ' ASC';
    if RecordCount <> 0 then
    begin
      Result := Fields[0].AsInteger
    end;
  end;
  Inc(result);
end;

function GetLastLine(ATabName , AFieldName , APrefix : string):string;
var
  i : Integer;
  s : string;
begin
  Result := '0';
  s := 'SELECT '+ AFieldName +' FROM ' +  ATabName + ' Where Sign_Id=' + APrefix + ' AND '+ AFieldName +' <> ""'  ;
  OutputLog(s);
  with DM.Qry do
  begin
    Close;
    SQL.Text := s;
    Open;
    Sort := AFieldName + ' ASC';
    Last;
    if RecordCount <> 0 then
      i := FieldByName(AFieldName).AsInteger
  end;
  Inc(i);
  Result := IntToStr(i);
end;

function TfrmMakingsPost.GetRowId(lpRowCount : Integer):string;
var
  szRowCount : Integer;
  I: Integer;
  s: string;
begin
  //跟距行数计算ID
  szRowCount := lpRowCount;
  for I := 0 to 6 - Length( IntToStr(szRowCount) )do
      s := s + '0';
  Result :=  IntToStr(dwNodeId) + s + IntToStr(szRowCount);
end;

function IsIDExists(ATabName , AFieldName , APrefix ,AID : string):Boolean;
var
  s : string;
begin
  s := 'SELECT '+ AFieldName +' FROM ' +  ATabName + ' Where Sign_Id=' + APrefix + ' AND '+ AFieldName +'="' + AID + '"';
  OutputLog(s);
  with DM.Qry do
  begin
    Close;
    SQL.Text := s;
    Open;
    Result := RecordCount <> 0;
  end;
end;

procedure TfrmMakingsPost.dsMasterAfterInsert(DataSet: TDataSet);
var
  DD2 : string;
  s : string;
  szId : string;
  szBarCode : string;
  i : Integer;
  szFieldName : string;
begin
  inherited;
  with DataSet do
  begin
    szFieldName := Self.cxDBTextEdit4.DataBinding.DataField;
    DD2 := GetRowId( GetRowCount(g_Table_MakingsList,szFieldName,IntToStr(dwNodeId) ) ) ;
    if IsIDExists(g_Table_MakingsList,szFieldName,IntToStr(dwNodeId),DD2) then
    begin
      DD2 := GetLastLine(  g_Table_MakingsList,szFieldName,IntToStr(dwNodeId)  );
    end;

    s := '';
    for I := 0 to 9 - Length( DD2 ) do
        s := s + '0';

    szId := '69' + s + DD2 ;
    FieldByName('Sign_Id').Value   := dwNodeId;
    FieldByName(szFieldName).Value := DD2;
    FieldByName('InputDate').Value := Date;
    if DD2 <> '' then
    begin
      szBarCode := EAN13BarCode(szId);
      FieldByName(cxDBTextEdit2.DataBinding.DataField).Value := szBarCode;
    end;
  end;
end;



procedure TfrmMakingsPost.FormActivate(Sender: TObject);
begin
  if not dwIsEdit then
  begin
    dwSQLText := 'Select * from ' + dwTableName + ' WHERE 1=2';
  end else
  begin
    dwSQLText := 'Select * from ' + dwTableName + ' WHERE GoodsNo="'+ dwGoodsNo +'"';
  end;
  dwADO.OpenSQL(Self.dsMaster,dwSQLText);
end;

procedure TfrmMakingsPost.FormDestroy(Sender: TObject);
begin
  Self.dsMaster.Close;
end;

procedure TfrmMakingsPost.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 27 then
  begin
    Close;
  end;
end;

procedure TfrmMakingsPost.RzToolButton1Click(Sender: TObject);
begin
  dwADO.ADOAppend(Self.dsMaster);
end;

procedure TfrmMakingsPost.RzToolButton2Click(Sender: TObject);
var
  szFieldName : string;
begin
  szFieldName := Self.cxDBTextEdit4.DataBinding.DataField;
  if not dwADO.UpdateTableData(dwTableName,'Id',0,Self.dsMaster) then
  begin
    ShowMessage('保存失败');
  end else
  begin
    ShowMessage('保存成功');
    dwADO.OpenSQL(Self.dsMaster,dwSQLText);
    RzToolButton1.Click;
  end;
end;

procedure TfrmMakingsPost.RzToolButton4Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmMakingsPost.RzToolButton5Click(Sender: TObject);
begin
  Self.dsMaster.Post;
end;

end.
