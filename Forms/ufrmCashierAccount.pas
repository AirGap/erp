unit ufrmCashierAccount;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore, dxSkinBlueprint,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinHighContrast,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinSevenClassic, dxSkinSharpPlus, dxSkinTheAsphaltWorld,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint, Vcl.ComCtrls,
  RzButton, cxLabel, cxTextEdit, Vcl.ExtCtrls, RzPanel,Data.DB,Data.Win.ADODB;

type
  TfrmCashierAccount = class(TForm)
    RzPanel1: TRzPanel;
    cxTextEdit1: TcxTextEdit;
    cxLabel1: TcxLabel;
    RzBitBtn1: TRzBitBtn;
    RzBitBtn2: TRzBitBtn;
    RzBitBtn3: TRzBitBtn;
    RzBitBtn4: TRzBitBtn;
    RzBitBtn5: TRzBitBtn;
    ListView1: TListView;
    RzBitBtn6: TRzBitBtn;
    procedure FormShow(Sender: TObject);
    procedure cxTextEdit1PropertiesChange(Sender: TObject);
    procedure cxTextEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure ListView1DblClick(Sender: TObject);
    procedure RzBitBtn3Click(Sender: TObject);
    procedure RzBitBtn5Click(Sender: TObject);
    procedure RzBitBtn1Click(Sender: TObject);
    procedure RzBitBtn2Click(Sender: TObject);
    procedure RzBitBtn6Click(Sender: TObject);
    procedure ListView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    procedure ShowListView(List : TListView;AIndex : Byte; AName : string);
  public
    { Public declarations }
    g_SuffixName : string;
  end;

var
  frmCashierAccount: TfrmCashierAccount;
  g_TableName : string = 'sk_BorrowAccount';


implementation

uses
   uDataModule,TreeFillThrd,global,ufrmPostAccounts,ufunctions;

{$R *.dfm}

procedure TfrmCashierAccount.ShowListView(List : TListView;AIndex : Byte; AName : string);
var
  Query: TADOQuery;
  PNode: PNodeData;
  ListItem : TListItem;

begin

  Query := TADOQuery.Create(nil);
  try

      Query.Connection := DM.ADOconn;
      case AIndex of
        0:
        begin
          Query.SQL.Text := 'Select * from ' + g_TableName + ' where ' + 'PID' + ' = 0' ;
        end;
        1:
        begin
          Query.SQL.Text := 'Select * from ' + g_TableName + ' where ' + 'PID' + ' = 0 and name like "%'+ AName +'%"';
        end;
      end;

      if Query.Active then
         Query.Close;

      Query.Open;
      List.Clear;
      while not Query.Eof do
      begin
        New(PNode);
        PNode^.Caption    := Query.FieldByName('name').AsString;
        PNode^.parent     := Query.FieldByName('parent').AsInteger;
        PNode^.Code       := Query.FieldByName('Code').AsString;
        PNode^.Input_time := Query.FieldByName('Input_time').AsDateTime;
        PNode^.End_time   := Query.FieldByName('End_time').AsDateTime;
        PNode^.remarks    := Query.FieldByName('remarks').AsString;
        PNode^.PatType    := Query.FieldByName('Pattype').AsInteger;
        if PNode^.PatType = g_pacttype then
        begin
          ListItem := List.Items.Add ;
          ListItem.Data := PNode;
          ListItem.Caption := PNode.Code;
          ListItem.SubItems.Add(PNode^.Caption);
          ListItem.SubItems.Add(FormatDateTime('yyyy-MM-dd',PNode^.Input_time));
          ListItem.SubItems.Add(PNode^.remarks);
        end;

        Query.Next;
      end;

  finally
    Query.Free;
  end;

end;

procedure TfrmCashierAccount.cxTextEdit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then
  begin
    Self.RzBitBtn4.Click;
  end;
end;

procedure TfrmCashierAccount.cxTextEdit1PropertiesChange(Sender: TObject);
var
  s : string;

begin
  s := Self.cxTextEdit1.Text;
  if Length(s) = 0 then
  begin
    ShowListView(Self.ListView1,1,s);
  end;
end;

procedure TfrmCashierAccount.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 27 then Close;
end;

procedure TfrmCashierAccount.FormShow(Sender: TObject);
var
  s : string;

begin
  s := Caption + ' - ' + g_SuffixName;
  Caption := s;
  ShowListView(Self.ListView1,0,'');
end;

procedure TfrmCashierAccount.ListView1DblClick(Sender: TObject);
var
  PNode: PNodeData;
//  Child : TfrmBranch;
//  Child: TfrmSelectType;
begin
  if nil <> Self.ListView1.Selected then
  begin
    PNode := PNodeData(Self.ListView1.Selected.Data);
    if Assigned(PNode) then
    begin
      g_Parent   := pNode.parent;
      g_IsSelect := False;
      Visible    := False;
    //  Child      := TfrmBranch.Create(Application);
    //  Child.Show;
      Close;

      {
      Child := TfrmSelectType.Create(Self);
      try
        Child .ShowModal;
      finally
        Child .Free;
      end;

      if not g_IsSelect then  Visible := True;
      }
    end;

  end;

end;

procedure TfrmCashierAccount.ListView1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 46 then
  begin
    Self.RzBitBtn5.Click;
  end;
end;

procedure TfrmCashierAccount.RzBitBtn1Click(Sender: TObject);
var
  s : string;

begin
  s := Self.cxTextEdit1.Text;
  if Length(s) = 0 then
  begin
     Application.MessageBox('请输入要搜索的名称!!',m_title,MB_OK + MB_ICONQUESTION )
  end
  else
  begin
    ShowListView(Self.ListView1,1,s);
  end;

end;

procedure TfrmCashierAccount.RzBitBtn3Click(Sender: TObject);
var
  Child : TfrmPostAccounts;
  szName: string;
  szNode: PNodeData;

begin
  if m_user.dwType = 0 then
  begin
    Child:= TfrmPostAccounts.Create(Self);
    try
      Child.g_TableName := g_TableName;
      Child.g_TableType := 1;
      Child.g_Prefix    := 'ZK-';
      Child.g_Suffix    := '';

      if Sender = RzBitBtn3 then
      begin //新增
        Child.g_PostCode := DM.getDataMaxDate(g_TableName);
        Child.g_IsModify := False;
        Child.ShowModal ;
        ShowListView(Self.ListView1,0,'');
      end else
      if Sender = RzBitBtn4 then
      begin //编辑
        Child.g_IsModify := True;
        if Assigned( Self.ListView1.Selected ) then
        begin
          szNode := PNodeData(Self.ListView1.Selected.Data);
          if Assigned(szNode) then
          begin
            Child.g_PostCode   := szNode.Code;
            Child.cxLookupComboBox1.Text := szNode.Caption;
            Child.RzMemo1.Text := szNode.remarks;
            Child.dtp1.Date    := szNode.Input_time;
            Child.ShowModal;
            ShowListView(Self.ListView1,0,'');
          end;

        end;

      end;

    finally
      Child.Free;
    end;

  end;
end;

procedure TfrmCashierAccount.RzBitBtn5Click(Sender: TObject);
var
  szNode : PNodeData;
  szCode : string;
  i : Integer;
  s : string;

  szFileName  : string;
  szCodeList  : string;
  szExpenditure : string;
  szPaymoney  : string;
  szPayoutlist: string;
  szTableList : array[0..11] of TTableList;
  szDir : string;

begin
  if m_user.dwType = 0 then
  begin
    if Self.ListView1.Selected <> nil then
    begin
        szNode := PNodeData(Self.ListView1.Selected.Data);
        if Assigned(szNode) then
        begin
           szCode := szNode^.Code;
           szFileName := szNode^.photo;
           if MessageBox(handle, PChar('你确认要删除"' + szNode.Caption + '"'),
                 '提示', MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = IDYES then
           begin

              DM.getTreeCode(g_TableName,szNode.parent,szCodeList);

           //   szTableList[0].ATableName := g_Table_Branch_Work; //考勤表
           //   szTableList[0].ADirectory := g_Branch_dir_Work;   //考勤附件目录

              szTableList[1].ATableName := g_Table_Expenditure; //混凝土表
              szTableList[1].ADirectory := g_Table_Concrete;    //混凝土附件目录

              szTableList[2].ATableName :=  g_Table_Branch_Mechanics;  //机械表
              szTableList[2].ADirectory := '';     //机械附件目录

              szTableList[3].ATableName := g_Table_BranchMoney;  //支款交易记录表
              szTableList[3].ADirectory := g_dir_BranchMoney;    //支款附件目录

              szTableList[4].ATableName := g_Table_DetailTable;  //支款交易记录明细表
              szTableList[4].ADirectory := g_dir_DetailTable;    //支款明细附件目录

              szTableList[5].ATableName := g_Table_Branch_RebarManage;  //支款钢筋表
              szTableList[5].ADirectory := g_dir_BranchMechanics;  //支款钢筋附件目录

              szTableList[6].ATableName := g_Table_Branch_EnterStorage; //架体入库单
              szTableList[6].ADirectory := '';  //架体入库单附件目录

              szTableList[7].ATableName := g_Table_Qualifications; //资质
              szTableList[7].ADirectory := ''; //资质目录

              szTableList[8].ATableName := g_Table_pactDebtmoney; //结算表
              szTableList[8].ADirectory := '';

              szTableList[9].ATableName := g_Table_Branch_EngineeringQuantity; //工程量表
              szTableList[9].ADirectory := g_dir_EngineeringQuantity;

              szTableList[10].ATableName := g_Table_Branch_EngineeringDetailed; //工程量明细表
              szTableList[10].ADirectory := '';

              szTableList[11].ATableName := g_Table_Expenditure;
              szTableList[11].ADirectory := g_Table_Expenditure;

              DeleteTableFile(szTableList,szCodeList);

              m_IsModify := True;
              DM.InDeleteData(g_TableName,szCodeList);
              ShowListView(Self.ListView1,0,'');
           end;

        end;

    end;

  end;

end;

procedure TfrmCashierAccount.RzBitBtn6Click(Sender: TObject);
var
  Child : TfrmBranch;
begin
  Child := TfrmBranch.Create(Application);
  try
    g_Parent := 0;
    g_ModuleIndex := 100;
    Child.Show;
  finally
  end;
  Close;
end;

procedure TfrmCashierAccount.RzBitBtn2Click(Sender: TObject);
var
  Child : TfrmBranch;
begin
  g_Parent := 0;
  g_ModuleIndex := 0;
  g_pacttype := 0;
  Child := TfrmBranch.Create(Application);
  try
    Child.Show;
  finally
  end;
  Close;
end;


end.
