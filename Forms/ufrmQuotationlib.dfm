object frmQuotationlib: TfrmQuotationlib
  Left = 0
  Top = 0
  Caption = #25253#20215#24211
  ClientHeight = 590
  ClientWidth = 1190
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object RzPanel1: TRzPanel
    Left = 0
    Top = 0
    Width = 1190
    Height = 571
    Align = alClient
    BorderOuter = fsNone
    TabOrder = 0
    object Splitter2: TSplitter
      Left = 328
      Top = 0
      Width = 10
      Height = 571
      Color = 16250613
      ParentColor = False
      ResizeStyle = rsUpdate
      ExplicitLeft = 178
      ExplicitHeight = 335
    end
    object RzPanel10: TRzPanel
      Left = 0
      Top = 0
      Width = 328
      Height = 571
      Align = alLeft
      BorderInner = fsFlat
      BorderOuter = fsNone
      BorderSides = [sdLeft, sdRight]
      TabOrder = 0
      object tv: TTreeView
        Left = 1
        Top = 32
        Width = 326
        Height = 539
        Align = alClient
        BevelInner = bvNone
        BevelOuter = bvNone
        DragMode = dmAutomatic
        HideSelection = False
        Images = DM.TreeImageList
        Indent = 19
        RowSelect = True
        SortType = stBoth
        TabOrder = 0
        OnClick = tvClick
        OnEdited = tvEdited
        OnKeyDown = tvKeyDown
        Items.NodeData = {
          0301000000260000000000000000000000FFFFFFFFFFFFFFFF00000000000000
          00000000000104A562F74E06527B7C}
      end
      object RzToolbar6: TRzToolbar
        Left = 1
        Top = 0
        Width = 326
        AutoStyle = False
        Images = DM.cxImageList1
        RowHeight = 28
        BorderInner = fsNone
        BorderOuter = fsFlat
        BorderSides = [sdTop]
        BorderWidth = 0
        GradientColorStyle = gcsCustom
        TabOrder = 1
        VisualStyle = vsGradient
        ToolbarControls = (
          RzSpacer16
          RzToolButton18
          RzSpacer26
          RzToolButton19
          RzSpacer17
          RzToolButton20
          RzSpacer21
          RzToolButton23
          RzSpacer22
          RzToolButton24)
        object RzSpacer16: TRzSpacer
          Left = 4
          Top = 4
        end
        object RzToolButton18: TRzToolButton
          Left = 12
          Top = 4
          Width = 52
          GradientColorStyle = gcsCustom
          SelectionColorStop = 16119543
          ImageIndex = 37
          ShowCaption = True
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          UseToolbarVisualStyle = False
          VisualStyle = vsGradient
          Caption = #26032#22686
          OnClick = RzToolButton18Click
        end
        object RzSpacer17: TRzSpacer
          Left = 124
          Top = 4
        end
        object RzToolButton20: TRzToolButton
          Left = 132
          Top = 4
          Width = 52
          GradientColorStyle = gcsCustom
          SelectionColorStop = 16119543
          ImageIndex = 51
          ShowCaption = True
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          UseToolbarVisualStyle = False
          VisualStyle = vsGradient
          Caption = #21024#38500
          OnClick = RzToolButton18Click
        end
        object RzSpacer21: TRzSpacer
          Left = 184
          Top = 4
        end
        object RzToolButton23: TRzToolButton
          Left = 192
          Top = 4
          Width = 52
          GradientColorStyle = gcsCustom
          SelectionColorStop = 16119543
          ImageIndex = 29
          ShowCaption = True
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          UseToolbarVisualStyle = False
          VisualStyle = vsGradient
          Caption = #19978#31227
          OnClick = RzToolButton23Click
        end
        object RzSpacer22: TRzSpacer
          Left = 244
          Top = 4
        end
        object RzToolButton24: TRzToolButton
          Left = 252
          Top = 4
          Width = 52
          GradientColorStyle = gcsCustom
          SelectionColorStop = 16119543
          ImageIndex = 31
          ShowCaption = True
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          UseToolbarVisualStyle = False
          VisualStyle = vsGradient
          Caption = #19979#31227
          OnClick = RzToolButton24Click
        end
        object RzSpacer26: TRzSpacer
          Left = 64
          Top = 4
        end
        object RzToolButton19: TRzToolButton
          Left = 72
          Top = 4
          Width = 52
          GradientColorStyle = gcsCustom
          SelectionColorStop = 16119543
          ImageIndex = 0
          ShowCaption = True
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          UseToolbarVisualStyle = False
          VisualStyle = vsGradient
          Caption = #32534#36753
          OnClick = RzToolButton18Click
        end
      end
    end
    object RzPanel9: TRzPanel
      Left = 338
      Top = 0
      Width = 852
      Height = 571
      Align = alClient
      BorderInner = fsFlat
      BorderOuter = fsNone
      BorderSides = [sdLeft, sdRight]
      TabOrder = 1
      object RzToolbar5: TRzToolbar
        Left = 1
        Top = 32
        Width = 850
        AutoStyle = False
        Images = DM.cxImageList1
        RowHeight = 28
        BorderInner = fsNone
        BorderOuter = fsFlat
        BorderSides = [sdTop]
        BorderWidth = 0
        GradientColorStyle = gcsCustom
        TabOrder = 0
        VisualStyle = vsGradient
        ToolbarControls = (
          RzSpacer13
          RzToolButton15
          RzSpacer1
          RzToolButton2
          RzSpacer2
          RzToolButton16
          RzSpacer14
          RzToolButton17
          RzSpacer6
          RzToolButton5
          RzSpacer7
          RzToolButton6
          RzSpacer8
          RzToolButton1
          RzSpacer10)
        object RzSpacer13: TRzSpacer
          Left = 4
          Top = 4
        end
        object RzToolButton15: TRzToolButton
          Left = 12
          Top = 4
          Width = 55
          GradientColorStyle = gcsCustom
          SelectionColorStop = 16119543
          ImageIndex = 37
          ShowCaption = True
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          UseToolbarVisualStyle = False
          VisualStyle = vsGradient
          Caption = #26032#22686
          Enabled = False
          OnClick = RzToolButton15Click
        end
        object RzToolButton16: TRzToolButton
          Left = 138
          Top = 4
          Width = 55
          GradientColorStyle = gcsCustom
          SelectionColorStop = 16119543
          ImageIndex = 3
          ShowCaption = True
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          UseToolbarVisualStyle = False
          VisualStyle = vsGradient
          Caption = #20445#23384
          Enabled = False
          OnClick = RzToolButton15Click
        end
        object RzSpacer14: TRzSpacer
          Left = 193
          Top = 4
        end
        object RzToolButton17: TRzToolButton
          Left = 201
          Top = 4
          Width = 55
          GradientColorStyle = gcsCustom
          SelectionColorStop = 16119543
          ImageIndex = 51
          ShowCaption = True
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          UseToolbarVisualStyle = False
          VisualStyle = vsGradient
          Caption = #21024#38500
          Enabled = False
          OnClick = RzToolButton15Click
        end
        object RzToolButton1: TRzToolButton
          Left = 384
          Top = 4
          Width = 55
          GradientColorStyle = gcsCustom
          ImageIndex = 8
          ShowCaption = True
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          UseToolbarVisualStyle = False
          VisualStyle = vsGradient
          Caption = #36864#20986
          OnClick = RzToolButton1Click
        end
        object RzSpacer1: TRzSpacer
          Left = 67
          Top = 4
        end
        object RzSpacer2: TRzSpacer
          Left = 130
          Top = 4
        end
        object RzToolButton2: TRzToolButton
          Left = 75
          Top = 4
          Width = 55
          GradientColorStyle = gcsCustom
          SelectionColorStop = 16119543
          ImageIndex = 0
          ShowCaption = True
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          UseToolbarVisualStyle = False
          VisualStyle = vsGradient
          Caption = #32534#36753
          Enabled = False
          OnClick = RzToolButton15Click
        end
        object RzSpacer6: TRzSpacer
          Left = 256
          Top = 4
          Grooved = True
        end
        object RzSpacer7: TRzSpacer
          Left = 316
          Top = 4
        end
        object RzToolButton5: TRzToolButton
          Left = 264
          Top = 4
          Width = 52
          GradientColorStyle = gcsCustom
          SelectionColorStop = 16119543
          ImageIndex = 71
          ShowCaption = True
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          UseToolbarVisualStyle = False
          VisualStyle = vsGradient
          Caption = #23548#20837
          Enabled = False
          OnClick = RzToolButton5Click
        end
        object RzToolButton6: TRzToolButton
          Left = 324
          Top = 4
          Width = 52
          GradientColorStyle = gcsCustom
          SelectionColorStop = 16119543
          ImageIndex = 73
          ShowCaption = True
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          UseToolbarVisualStyle = False
          VisualStyle = vsGradient
          Caption = #23548#20986
          Enabled = False
          OnClick = RzToolButton15Click
        end
        object RzSpacer8: TRzSpacer
          Left = 376
          Top = 4
        end
        object RzSpacer10: TRzSpacer
          Left = 439
          Top = 4
        end
      end
      object Grid: TcxGrid
        Left = 1
        Top = 64
        Width = 850
        Height = 507
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        PopupMenu = pm
        TabOrder = 1
        LevelTabs.Slants.Positions = [spLeft, spRight]
        LevelTabs.Style = 6
        LockedStateImageOptions.AssignedValues = [lsiavFont, lsiavColor]
        LockedStateImageOptions.Font.Charset = DEFAULT_CHARSET
        LockedStateImageOptions.Font.Color = clWindowText
        LockedStateImageOptions.Font.Height = -12
        LockedStateImageOptions.Font.Name = 'Tahoma'
        LockedStateImageOptions.Font.Style = [fsBold]
        LockedStateImageOptions.ShowText = True
        LookAndFeel.Kind = lfUltraFlat
        LookAndFeel.NativeStyle = True
        RootLevelOptions.TabsForEmptyDetails = False
        object tvView1: TcxGridDBBandedTableView
          OnDblClick = tvView4DblClick
          OnKeyDown = tvView1KeyDown
          Navigator.Buttons.CustomButtons = <>
          OnEditing = tvView4Editing
          OnEditKeyDown = tvView4EditKeyDown
          OnGetCellHeight = tvView1GetCellHeight
          DataController.DataSource = dsMasterSource
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.FocusCellOnTab = True
          OptionsBehavior.FocusFirstCellOnNewRecord = True
          OptionsBehavior.GoToNextCellOnEnter = True
          OptionsBehavior.FocusCellOnCycle = True
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsSelection.InvertSelect = False
          OptionsSelection.MultiSelect = True
          OptionsSelection.CellMultiSelect = True
          OptionsView.CellAutoHeight = True
          OptionsView.GroupByBox = False
          OptionsView.HeaderHeight = 23
          OptionsView.Indicator = True
          OptionsView.BandHeaderHeight = 23
          Bands = <
            item
              Caption = #22522#26412#20449#24687
            end
            item
              Caption = #25104#26412#25253#20215
            end
            item
              Caption = #24037#33402
              Width = 140
            end>
          object tvView1Col1: TcxGridDBBandedColumn
            Caption = #24207#21495
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.Alignment.Horz = taCenter
            OnGetDisplayText = tvView4Col1GetDisplayText
            Options.Filtering = False
            Options.Sorting = False
            Width = 40
            Position.BandIndex = 0
            Position.ColIndex = 0
            Position.RowIndex = 0
          end
          object tvView1Col2: TcxGridDBBandedColumn
            Caption = #39033#30446#21517#31216
            DataBinding.FieldName = 'ProjectName'
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Options.Filtering = False
            Options.Sorting = False
            Width = 118
            Position.BandIndex = 0
            Position.ColIndex = 3
            Position.RowIndex = 0
          end
          object tvView1Col3: TcxGridDBBandedColumn
            Caption = #21697#29260
            DataBinding.FieldName = 'brand'
            PropertiesClassName = 'TcxTextEditProperties'
            Options.Filtering = False
            Options.Sorting = False
            Width = 87
            Position.BandIndex = 0
            Position.ColIndex = 4
            Position.RowIndex = 0
          end
          object tvView1Col4: TcxGridDBBandedColumn
            Caption = #35268#26684
            DataBinding.FieldName = 'Spec'
            PropertiesClassName = 'TcxTextEditProperties'
            Options.Filtering = False
            Options.Sorting = False
            Width = 67
            Position.BandIndex = 0
            Position.ColIndex = 5
            Position.RowIndex = 0
          end
          object tvView1Col5: TcxGridDBBandedColumn
            Caption = #21333#20301
            DataBinding.FieldName = 'Company'
            RepositoryItem = DM.UnitList
            Options.Filtering = False
            Options.Sorting = False
            Width = 51
            Position.BandIndex = 0
            Position.ColIndex = 6
            Position.RowIndex = 0
          end
          object tvView1Col6: TcxGridDBBandedColumn
            Caption = #32508#21512#21333#20215
            DataBinding.FieldName = 'SyntheticalPrice'
            PropertiesClassName = 'TcxCurrencyEditProperties'
            Options.Filtering = False
            Options.Sorting = False
            Width = 60
            Position.BandIndex = 0
            Position.ColIndex = 7
            Position.RowIndex = 0
          end
          object tvView1Col7: TcxGridDBBandedColumn
            Caption = #20027#26448#25253#20215
            DataBinding.FieldName = 'MainMaterialPrice'
            PropertiesClassName = 'TcxCurrencyEditProperties'
            Options.Filtering = False
            Options.Sorting = False
            Width = 60
            Position.BandIndex = 0
            Position.ColIndex = 8
            Position.RowIndex = 0
          end
          object tvView1Col8: TcxGridDBBandedColumn
            Caption = #20154#24037#25253#20215
            DataBinding.FieldName = 'ManpowerPrice'
            PropertiesClassName = 'TcxCurrencyEditProperties'
            Options.Filtering = False
            Options.Sorting = False
            Width = 60
            Position.BandIndex = 0
            Position.ColIndex = 9
            Position.RowIndex = 0
          end
          object tvView1Col9: TcxGridDBBandedColumn
            Caption = #20154#24037#21333#20215
            DataBinding.FieldName = 'ArtificialPrice'
            PropertiesClassName = 'TcxCurrencyEditProperties'
            Properties.OnValidate = tvView1Col9PropertiesValidate
            Options.Filtering = False
            Options.Sorting = False
            Width = 60
            Position.BandIndex = 1
            Position.ColIndex = 0
            Position.RowIndex = 0
          end
          object tvView1Col10: TcxGridDBBandedColumn
            Caption = #26448#26009#21333#20215
            DataBinding.FieldName = 'MaterialPrice'
            PropertiesClassName = 'TcxCurrencyEditProperties'
            Properties.OnValidate = tvView1Col10PropertiesValidate
            Options.Filtering = False
            Options.Sorting = False
            Width = 60
            Position.BandIndex = 1
            Position.ColIndex = 1
            Position.RowIndex = 0
          end
          object tvView1Col11: TcxGridDBBandedColumn
            Caption = #25104#26412#21333#20215
            DataBinding.FieldName = 'CostUnitPrice'
            PropertiesClassName = 'TcxCurrencyEditProperties'
            Properties.OnValidate = tvView1Col11PropertiesValidate
            Options.Filtering = False
            Options.Sorting = False
            Width = 60
            Position.BandIndex = 1
            Position.ColIndex = 2
            Position.RowIndex = 0
          end
          object tvView1Col12: TcxGridDBBandedColumn
            Caption = #26045#24037#24037#33402#25110#35828#26126
            DataBinding.FieldName = 'Technics'
            PropertiesClassName = 'TcxMemoProperties'
            Options.Filtering = False
            Options.Sorting = False
            Width = 120
            Position.BandIndex = 2
            Position.ColIndex = 0
            Position.RowIndex = 0
          end
          object tvView1Col13: TcxGridDBBandedColumn
            Caption = #26085#26399
            DataBinding.FieldName = 'InputDate'
            Width = 65
            Position.BandIndex = 0
            Position.ColIndex = 1
            Position.RowIndex = 0
          end
          object tvView1Col14: TcxGridDBBandedColumn
            Caption = #32534#21046#20154
            DataBinding.FieldName = 'Author'
            Width = 65
            Position.BandIndex = 0
            Position.ColIndex = 2
            Position.RowIndex = 0
          end
        end
        object Lv1: TcxGridLevel
          Caption = #20845#39033
          GridView = tvView1
        end
      end
      object Panel1: TPanel
        Left = 1
        Top = 0
        Width = 850
        Height = 32
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 2
        object cxLabel1: TcxLabel
          Left = 11
          Top = 8
          Caption = #20851#38190#23383#65306
          Transparent = True
        end
        object cxTextEdit1: TcxTextEdit
          Left = 64
          Top = 6
          Enabled = False
          TabOrder = 1
          TextHint = #39033#30446#21517#31216
          OnKeyDown = cxTextEdit1KeyDown
          Width = 202
        end
        object cxButton1: TcxButton
          Left = 272
          Top = 4
          Width = 60
          Height = 25
          Caption = #25628#32034
          Enabled = False
          LookAndFeel.NativeStyle = False
          LookAndFeel.SkinName = 'Office2013White'
          OptionsImage.Glyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            000000000000000000020000000E0B14308329448DFB1D2F58A5000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            0000000000020000000E0D1937883C6DB2FF5BB1F9FF325196F4000000000000
            00000000000100000004000000090000000D0000000F0000000F0000000C0000
            00070000000E0F1D3C864A7CBCFF73C4FFFF467CC3FF17254485000000000000
            0002000000081C130F465A3B31BC7C5043F87F5244FF7B4E42FA57382FC11E14
            1059112142875686C2FF88D0FFFF5186C7FF142343880000000F000000010302
            02104A332C91946B5DFDC6ACA1FFE4D1C6FFEDDDD2FFE2D0C5FFC0A599FF855C
            50FF6E6B7EFF98D4F8FF5B8ECBFF152545840000000D00000002000000076046
            3DA6B39288FFE9DAD0FFDAC0A1FFCBA87AFFC49B66FFCCAA7EFFDCC2A5FFE5D2
            C6FF9A766AFF736A77FF162747850000000E00000002000000002A201D4AAE88
            7CFFEFE6DFFFCDA67CFFCDA26BFFE3C28CFFEDD5A2FFE7CD9EFFD3B182FFD0AE
            88FFE7D5CAFF885F53FF25181464000000070000000000000000755B53ACDFCE
            C9FFDDC1A8FFC99865FFE8BE83FFE9C388FFEDCA97FFEFD3A7FFF2D9B0FFD5B1
            87FFDBBEA6FFC5ACA2FF5A3D33C10000000C0000000000000000A9877CE9F8F4
            F2FFC79873FFDEAB77FFEFCDABFFF0D0B1FFEDC9A1FFECC69AFFEFCFA9FFE9C9
            A4FFC89B77FFE6D6CEFF7C5448F10000000F0000000000000000C09C90FFFDFD
            FCFFBE875FFFEDCFB9FFF5DFD2FFF2D6C1FFF1CFB4FFEDC6A4FFECC19BFFEFC8
            A6FFC08B67FFF1E6DFFF8B6154FF0000000F0000000000000000AF9186E6F9F5
            F4FFC69474FFE8CDC3FFF9E8E4FFF6DED2FFF3D4C2FFF0CBB2FFEBB78EFFE5B7
            92FFC59172FFEBDFD9FF866055EE0000000D0000000000000000876F68B0E7D9
            D4FFE2C6B7FFC89072FFFAEFF2FFF9E7E4FFF6DDD3FFF1C8B2FFEBAF88FFC98E
            6CFFDCBBAAFFD3C0B7FF6B4F46BC00000009000000000000000026201E36CCAF
            A7FAFBF8F7FFCF9F88FFC78E72FFE9CDC6FFEDC7B5FFDD9F79FFC88865FFCE9D
            84FFF5EFEBFFB39387FF2A201D52000000040000000000000000000000036454
            4F84D9C2BAFFFDFBFAFFE2C6B8FFCB977EFFC08163FFCB977DFFE0C4B4FFFAF6
            F5FFC9B0A7FF6B564EA700000009000000010000000000000000000000000202
            020762534D81CEB2A9FAEADDD8FFF9F5F4FFFFFFFFFFF9F5F4FFE9DCD7FFC8AC
            A2FC62504B900404031000000002000000000000000000000000000000000000
            000000000003241F1D3486726BADB69B91E6CCADA1FFB99C92E988736CB22822
            1F3E000000060000000100000000000000000000000000000000}
          TabOrder = 2
          OnClick = cxButton1Click
        end
      end
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 571
    Width = 1190
    Height = 19
    Panels = <
      item
        Width = 50
      end>
  end
  object pm: TPopupMenu
    Images = DM.cxImageList1
    Left = 392
    Top = 288
    object N1: TMenuItem
      Caption = #26032#22686
      ImageIndex = 37
      OnClick = RzToolButton15Click
    end
    object N2: TMenuItem
      Caption = #32534#36753
      ImageIndex = 0
      OnClick = N2Click
    end
    object N3: TMenuItem
      Caption = #20445#23384
      ImageIndex = 3
      OnClick = N3Click
    end
    object N4: TMenuItem
      Caption = #21024#38500
      ImageIndex = 51
      OnClick = N4Click
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object N6: TMenuItem
      Caption = #22797#21046
      ImageIndex = 75
      OnClick = RzToolButton15Click
    end
    object N7: TMenuItem
      Caption = #31896#36148
      ImageIndex = 77
      OnClick = N7Click
    end
    object N8: TMenuItem
      Caption = '-'
    end
    object N9: TMenuItem
      Caption = #23548#20837
      ImageIndex = 71
      OnClick = N9Click
    end
    object N10: TMenuItem
      Caption = #23548#20986
      ImageIndex = 73
      OnClick = N10Click
    end
    object N12: TMenuItem
      Caption = '-'
    end
    object N11: TMenuItem
      Caption = #36864#20986
      ImageIndex = 8
      OnClick = N11Click
    end
  end
  object dsMaster: TClientDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = dsMasterAfterOpen
    AfterClose = dsMasterAfterClose
    AfterInsert = dsMasterAfterInsert
    Left = 504
    Top = 280
  end
  object dsMasterSource: TDataSource
    DataSet = dsMaster
    Left = 504
    Top = 352
  end
end
