unit ufrmBorrowAccount;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore, dxSkinBlueprint,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinHighContrast,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinSevenClassic, dxSkinSharpPlus, dxSkinTheAsphaltWorld,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint, Vcl.ComCtrls,
  RzButton, cxLabel, cxTextEdit, RzPanel, Vcl.ExtCtrls, Data.DB, Data.Win.ADODB,
  Vcl.Menus;

type
  TfrmBorrowAccount = class(TForm)
    RzGroupBox1: TRzGroupBox;
    RzPanel1: TRzPanel;
    cxTextEdit1: TcxTextEdit;
    cxLabel1: TcxLabel;
    RzBitBtn1: TRzBitBtn;
    RzBitBtn2: TRzBitBtn;
    RzBitBtn3: TRzBitBtn;
    RzBitBtn4: TRzBitBtn;
    RzBitBtn5: TRzBitBtn;
    ListView1: TListView;
    RzGroupBox2: TRzGroupBox;
    RzPanel2: TRzPanel;
    cxTextEdit2: TcxTextEdit;
    cxLabel2: TcxLabel;
    RzBitBtn6: TRzBitBtn;
    RzBitBtn7: TRzBitBtn;
    RzBitBtn8: TRzBitBtn;
    RzBitBtn9: TRzBitBtn;
    RzBitBtn10: TRzBitBtn;
    ListView2: TListView;
    Qry: TADOQuery;
    Splitter1: TSplitter;
    pm1: TPopupMenu;
    pm2: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    procedure cxTextEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxTextEdit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure ListView1DblClick(Sender: TObject);
    procedure ListView2DblClick(Sender: TObject);
    procedure RzBitBtn7Click(Sender: TObject);
    procedure RzBitBtn3Click(Sender: TObject);
    procedure RzBitBtn5Click(Sender: TObject);
    procedure RzBitBtn10Click(Sender: TObject);
    procedure RzBitBtn1Click(Sender: TObject);
    procedure RzBitBtn2Click(Sender: TObject);
    procedure RzBitBtn6Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure N1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure N5Click(Sender: TObject);
    procedure N6Click(Sender: TObject);
  private
    { Private declarations }
    g_TableName_Borrow  : string ;
    g_TableName_Loan    : string ;
    procedure DeleteListView(List : TListView ; lptype : Byte );
    procedure ShowListView(List : TListView ; AIndex : Byte ; AName,ATableName : string ; AType : Byte );
  public
    { Public declarations }
  end;

var
  frmBorrowAccount: TfrmBorrowAccount;

implementation

{$R *.dfm}

uses
  ufrmBorrowMoney,
  uDataModule,
  ufrmPostAccounts,
  global,
  ufunctions,TreeFillThrd;

procedure TfrmBorrowAccount.ShowListView(List : TListView ; AIndex : Byte ; AName,ATableName : string ; AType : Byte );
var
  Query: TADOQuery;
  PNode: PNodeData;
  ListItem : TListItem;
  szType : Integer;

begin
  Query := TADOQuery.Create(nil);
  try

      Query.Connection := DM.ADOconn;
      case AIndex of
        0:
        begin
          Query.SQL.Text := 'Select * from ' + ATableName + ' where ' + 'PID' + ' = 0' ;
        end;
        1:
        begin
          Query.SQL.Text := 'Select * from ' + ATableName + ' where ' + 'PID' + ' = 0 and name like "%'+ AName +'%"';
        end;
      end;
      OutputLog(Query.SQL.Text);
      if Query.Active then
         Query.Close;

      Query.Open;

      List.Clear;
      while not Query.Eof do
      begin
        New(PNode);
        PNode^.Caption    := Query.FieldByName('name').AsString;
        PNode^.parent     := Query.FieldByName('parent').AsInteger;
        PNode^.Code       := Query.FieldByName('Code').AsString;
        PNode^.Input_time := Query.FieldByName('Input_time').AsDateTime;
        PNode^.End_time   := Query.FieldByName('End_time').AsDateTime;
        PNode^.remarks    := Query.FieldByName('remarks').AsString;
        szType := Query.FieldByName('PatType').AsInteger;
        if szType = AType then
        begin
          ListItem := List.Items.Add ;
          ListItem.Data := PNode;
          ListItem.Caption := PNode.Code;
          ListItem.SubItems.Add(PNode^.Caption);
          ListItem.SubItems.Add(FormatDateTime('yyyy-MM-dd',PNode^.Input_time));
          ListItem.SubItems.Add(PNode^.remarks);
        end;

        Query.Next;
      end;

  finally
    Query.Free;
  end;
end;

procedure TfrmBorrowAccount.cxTextEdit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then
  begin
    Self.RzBitBtn1.Click;
  end;
end;

procedure TfrmBorrowAccount.cxTextEdit2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then
  begin
    Self.RzBitBtn6.Click;
  end;
end;

procedure TfrmBorrowAccount.DeleteListView(List : TListView ; lptype : Byte );
var
  szNode : PNodeData;
  szCode : string;
  Code : string;
  szCodeList    : string;
  szTableList : array[0..2] of TTableList;
  szTableName : string;

begin

  if m_user.dwType = 0 then
  begin
      szNode := PNodeData(List.Selected.Data);
      if Assigned(szNode) then
      begin
         case lptype of
           1:
           begin
             szTableName := g_TableName_Borrow ;
           end;
           2:
           begin
             szTableName := g_TableName_Loan;
           end;
         end;
         DM.getTreeCode(szTableName, szNode^.parent,szCodeList); //获取要删除的id表

         szCode := szNode^.Code;

         if MessageBox(handle, PChar('你确认要删除"' + szNode.Caption + '"'),
               '提示', MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = IDYES then
          begin
            szTableList[0].ATableName := 'sk_BorrowMoney';  //借款条目
            szTableList[0].ADirectory := 'BorrowMoney';

            szTableList[1].ATableName := 'sk_GiveBorrowMoney'; //还款明细进度
            szTableList[1].ADirectory := 'MakeMoney';

            DeleteTableFile(szTableList,szCodeList);
            DM.InDeleteData(szTableName,szCodeList);

            m_IsModify := True;
            ShowListView(List,0,'',szTableName,lptype);

          end;

      end;

  end;

end;

procedure TfrmBorrowAccount.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 27 then  Close;
end;

procedure TfrmBorrowAccount.FormShow(Sender: TObject);
begin
  g_TableName_Borrow  := 'sk_BorrowMoney_Tree';
  g_TableName_Loan    := 'sk_Loan_Tree' ;

  ShowListView(Self.ListView1,0,'',g_TableName_Borrow,1);
  ShowListView(Self.ListView2,0,'',g_TableName_Loan,2);
end;

procedure TfrmBorrowAccount.ListView1DblClick(Sender: TObject);
var
  PNode: PNodeData;
  Data : Pointer;
begin
  if nil <> Self.ListView1.Selected then
  begin
    Data  := Self.ListView1.Selected.Data;

    if Assigned(Data) then
    begin
      PNode := PNodeData(Data);
      if Assigned(PNode) then
      begin
        g_Parent   := pNode.parent;
        g_pacttype := 1;
        TfrmBorrowMoney.Create(Application);
        Close;
      end;
    end;
  end;
end;

procedure TfrmBorrowAccount.ListView2DblClick(Sender: TObject);
var
  PNode: PNodeData;
  Data : Pointer;

begin
  if nil <> Self.ListView2.Selected then
  begin
    Data  := Self.ListView2.Selected.Data;

    if Assigned(Data) then
    begin
      PNode := PNodeData(Data);
      if Assigned(PNode) then
      begin
        g_Parent   := pNode.parent;
        g_pacttype := 2;
        TfrmBorrowMoney.Create(Application);
        Close;
      end;
    end;
  end;
end;

procedure TfrmBorrowAccount.N1Click(Sender: TObject);
begin
  Self.RzBitBtn3.Click;
end;

procedure TfrmBorrowAccount.N2Click(Sender: TObject);
begin
  Self.RzBitBtn4.Click;
end;

procedure TfrmBorrowAccount.N3Click(Sender: TObject);
begin
  Self.RzBitBtn5.Click;
end;

procedure TfrmBorrowAccount.N4Click(Sender: TObject);
begin
  Self.RzBitBtn8.Click;
end;

procedure TfrmBorrowAccount.N5Click(Sender: TObject);
begin
  Self.RzBitBtn3.Click;
end;

procedure TfrmBorrowAccount.N6Click(Sender: TObject);
begin
  Self.RzBitBtn10.Click;
end;

procedure TfrmBorrowAccount.RzBitBtn10Click(Sender: TObject);
begin
  DeleteListView(Self.ListView2,2);
end;

procedure TfrmBorrowAccount.RzBitBtn1Click(Sender: TObject);
var
  s : string;
begin
  s := Self.cxTextEdit1.text; //借款
  if Length(s) <> 0 then
  begin
    ShowListView(Self.ListView1,1,s,g_TableName_Borrow,1);
  end
  else
  begin
    Application.MessageBox('请输入要搜索的名称!!',m_title,MB_OK + MB_ICONQUESTION )
  end;

end;

procedure TfrmBorrowAccount.RzBitBtn2Click(Sender: TObject);
begin
  g_Parent := 0;
  g_pacttype := 1;
  g_TreeType := 1;
  TfrmBorrowMoney.Create(Application);
  Close;
end;

procedure TfrmBorrowAccount.RzBitBtn3Click(Sender: TObject);
var
  Child : TfrmPostAccounts;
  szName: string;
  szNode: PNodeData;

begin
  if m_user.dwType = 0 then
  begin
    Child:= TfrmPostAccounts.Create(Self);
    try
      Child.g_TableType := 1;  //写合同分类ID
      if Sender = RzBitBtn3 then  //借款添加
      begin //新增
        g_pacttype := 1;
        Child.g_TableName := g_TableName_Borrow;
        Child.g_PostCode := DM.getDataMaxDate(g_TableName_Borrow);
        Child.g_IsModify := False;
        Child.ShowModal ;
        ShowListView(Self.ListView1,0,'',g_TableName_Borrow,1);
      end else
      if Sender = RzBitBtn4 then  //借款编辑
      begin //编辑
        Child.g_IsModify := True;
        Child.g_TableName := g_TableName_Borrow;
        if Assigned( Self.ListView1.Selected ) then
        begin
          szNode := PNodeData(Self.ListView1.Selected.Data);
          if Assigned(szNode) then
          begin
            Child.g_PostCode   := szNode.Code;
            Child.cxLookupComboBox1.Text := szNode.Caption;
            Child.RzMemo1.Text := szNode.remarks;
            Child.dtp1.Date    := szNode.Input_time;
            Child.ShowModal;
            ShowListView(Self.ListView1,0,'',g_TableName_Borrow,1);
          end;

        end;

      end else
      if Sender = RzBitBtn8 then  //贷款新增
      begin
        g_pacttype := 2;
        Child.g_TableName := g_TableName_Loan;
        Child.g_PostCode := DM.getDataMaxDate(g_TableName_Loan);
        Child.g_IsModify := False;
        Child.ShowModal ;
        ShowListView(Self.ListView2,0,'',g_TableName_Loan,2);
      end else
      if Sender = RzBitBtn9 then  //贷款编辑
      begin
        Child.g_IsModify := True;
        Child.g_TableName := g_TableName_Loan;
        if Assigned( Self.ListView2.Selected ) then
        begin
          szNode := PNodeData(Self.ListView2.Selected.Data);
          if Assigned(szNode) then
          begin
            Child.g_PostCode   := szNode.Code;
            Child.cxLookupComboBox1.Text := szNode.Caption;
            Child.RzMemo1.Text := szNode.remarks;
            Child.dtp1.Date    := szNode.Input_time;
            Child.ShowModal;
            ShowListView(Self.ListView2,0,'',g_TableName_Loan,2);
          end;

        end;
      end;

    finally
      Child.Free;
    end;

  end;

end;

procedure TfrmBorrowAccount.RzBitBtn5Click(Sender: TObject);
begin
  DeleteListView(Self.ListView1,1);
end;

procedure TfrmBorrowAccount.RzBitBtn6Click(Sender: TObject);
var
  s : string;
begin
  s := Self.cxTextEdit2.text;
  if Length(s) <> 0 then
  begin
    ShowListView(Self.ListView2,1,s,g_TableName_Loan,1);
  end
  else
  begin
    Application.MessageBox('请输入要搜索的名称!!',m_title,MB_OK + MB_ICONQUESTION )
  end;

end;

procedure TfrmBorrowAccount.RzBitBtn7Click(Sender: TObject);
begin
  g_Parent := 0;
  g_pacttype := 2;
  g_TreeType := 2;
  TfrmBorrowMoney.Create(Application);
  Close;
end;



end.
