unit ufrmEmployee;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, RzPanel, Vcl.ToolWin,
  Vcl.ComCtrls, RzButton, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, cxLabel, cxTextEdit, cxStyles,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxNavigator, Data.DB, cxDBData, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Data.Win.ADODB,ufrmBaseController;

type
  TfrmEmployee = class(TfrmBaseController)
    RzToolbar1: TRzToolbar;
    RzSpacer1: TRzSpacer;
    RzToolButton1: TRzToolButton;
    RzSpacer2: TRzSpacer;
    RzToolButton2: TRzToolButton;
    RzSpacer3: TRzSpacer;
    RzToolButton3: TRzToolButton;
    RzSpacer4: TRzSpacer;
    RzToolButton5: TRzToolButton;
    StatusBar1: TStatusBar;
    tvTable: TcxGridDBTableView;
    Lv: TcxGridLevel;
    Grid: TcxGrid;
    tvTableColumn1: TcxGridDBColumn;
    tvTableColumn2: TcxGridDBColumn;
    ADOTab: TADOQuery;
    DataTab: TDataSource;
    procedure RzToolButton5Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure RzToolButton1Click(Sender: TObject);
    procedure RzToolButton2Click(Sender: TObject);
    procedure RzToolButton3Click(Sender: TObject);
    procedure tvTableEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure tvTableColumn1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure tvTableEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure ADOTabAfterOpen(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmEmployee: TfrmEmployee;

implementation

uses
   uDataModule,global;

{$R *.dfm}

procedure TfrmEmployee.ADOTabAfterOpen(DataSet: TDataSet);
begin
  TADOQuery(DataSet).Properties.Get_Item('Update Criteria').Value :=0;
end;

procedure TfrmEmployee.FormActivate(Sender: TObject);
begin
//  sk_Maintain_Tab
  with Self.ADOTab do
  begin
    Close;
    SQL.Clear;
    SQL.Text := 'Select * from ' + g_Table_Maintain_Tab;
    Open;
  end;
end;

procedure TfrmEmployee.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Self.ADOTab.Close;
end;

procedure TfrmEmployee.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 27 then
  begin
    Close;
  end;
end;

procedure TfrmEmployee.RzToolButton1Click(Sender: TObject);
begin

  with Self.ADOTab do
  begin

    if State <> dsInactive then
    begin

      Append;
      Self.tvTableColumn2.FocusWithSelection;
      Self.Grid.SetFocus;
      keybd_event(VK_RETURN,0,0,0);

    end;

  end;

end;

procedure TfrmEmployee.RzToolButton2Click(Sender: TObject);
begin
  //����
  IsDeleteEmptyData(Self.ADOTab ,
                 g_Table_Maintain_Tab ,
                 Self.tvTableColumn2.DataBinding.FieldName);
end;

procedure TfrmEmployee.RzToolButton5Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmEmployee.tvTableColumn1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  AText := IntToStr(ARecord.Index + 1);
end;

procedure TfrmEmployee.tvTableEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  if AItem.Index = Self.tvTableColumn1.Index then
  begin
    AAllow := False;
  end;
end;

procedure TfrmEmployee.tvTableEditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = 13) and (AItem.Index = Self.tvTableColumn2.Index) then
  begin
    if Self.tvTable.Controller.FocusedRow.IsLast then
    begin
      with Self.ADOTab do
      begin
        if State <> dsInactive then
        begin
          Append;
          Self.tvTableColumn2.FocusWithSelection;
        end;
      end;
    end;
  end;

end;

procedure TfrmEmployee.RzToolButton3Click(Sender: TObject);
begin
//ɾ��
  IsDelete(Self.tvTable);
end;

end.
