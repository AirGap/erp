unit ufrmMakings;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, cxStyles, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator, Data.DB, cxDBData,
  cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxClasses, cxGridCustomView, cxGrid, cxTextEdit, cxLabel, RzButton, RzPanel,
  Vcl.ComCtrls, Vcl.ExtCtrls,UtilsTree, TreeUtils,ufrmBaseController,FillThrdTree,
  Data.Win.ADODB, cxMaskEdit, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, Vcl.Menus, Vcl.StdCtrls, Datasnap.DBClient, cxCurrencyEdit,UnitADO,
  dxmdaset;

type
  TfrmMakings = class(TfrmBaseController)
    RzPanel10: TRzPanel;
    tv: TTreeView;
    RzToolbar6: TRzToolbar;
    RzSpacer16: TRzSpacer;
    RzToolButton18: TRzToolButton;
    RzSpacer17: TRzSpacer;
    RzToolButton20: TRzToolButton;
    RzSpacer21: TRzSpacer;
    RzToolButton23: TRzToolButton;
    RzSpacer22: TRzSpacer;
    RzToolButton24: TRzToolButton;
    RzSpacer26: TRzSpacer;
    RzToolButton19: TRzToolButton;
    RzPanel9: TRzPanel;
    RzToolbar5: TRzToolbar;
    RzSpacer13: TRzSpacer;
    RzToolButton15: TRzToolButton;
    RzToolButton16: TRzToolButton;
    RzSpacer14: TRzSpacer;
    RzToolButton17: TRzToolButton;
    RzSpacer18: TRzSpacer;
    RzToolButton21: TRzToolButton;
    cxLabel2: TcxLabel;
    cxTextEdit2: TcxTextEdit;
    Grid: TcxGrid;
    tvView: TcxGridDBTableView;
    tvViewColumn1: TcxGridDBColumn;
    tvViewColumn2: TcxGridDBColumn;
    tvViewColumn4: TcxGridDBColumn;
    tvViewColumn3: TcxGridDBColumn;
    tvViewColumn5: TcxGridDBColumn;
    Lv: TcxGridLevel;
    Splitter2: TSplitter;
    RzPanel1: TRzPanel;
    StatusBar1: TStatusBar;
    RzToolButton1: TRzToolButton;
    RzSpacer1: TRzSpacer;
    RzSpacer2: TRzSpacer;
    RzSpacer3: TRzSpacer;
    RzToolButton2: TRzToolButton;
    RzSpacer4: TRzSpacer;
    RzToolButton3: TRzToolButton;
    Print: TPopupMenu;
    N2: TMenuItem;
    tvViewColumn6: TcxGridDBColumn;
    dsMakings: TClientDataSet;
    ds1: TDataSource;
    tvViewColumn7: TcxGridDBColumn;
    tvViewColumn9: TcxGridDBColumn;
    tvViewColumn10: TcxGridDBColumn;
    tvViewColumn11: TcxGridDBColumn;
    tvViewColumn12: TcxGridDBColumn;
    RzSpacer5: TRzSpacer;
    RzToolButton4: TRzToolButton;
    RzSpacer6: TRzSpacer;
    N1: TMenuItem;
    tvViewColumn8: TcxGridDBColumn;
    N3: TMenuItem;
    dxSelectPrint: TdxMemData;
    dsSelectPrint: TDataSource;
    dxSelectPrintMakingCaption: TStringField;
    dxSelectPrintModelIndex: TStringField;
    dxSelectPrintBrand: TStringField;
    dxSelectPrintspec: TStringField;
    dxSelectPrintGoodsNo: TStringField;
    dxSelectPrintBarCode: TStringField;
    dxSelectPrintManufactor: TStringField;
    dxSelectPrintNoColour: TStringField;
    dxSelectPrintBuyingPrice: TCurrencyField;
    dxSelectPrintUnitPrice: TCurrencyField;
    dxSelectPrintInputDate: TDateField;
    dxSelectPrintProjectName: TStringField;
    dxSelectPrintProjectCode: TStringField;
    dxSelectPrintSign_Id: TIntegerField;
    dxSelectPrintId: TIntegerField;
    pm1: TPopupMenu;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    N11: TMenuItem;
    N12: TMenuItem;
    N13: TMenuItem;
    RzToolButton5: TRzToolButton;
    RzSpacer7: TRzSpacer;
    RzToolButton6: TRzToolButton;
    procedure FormShow(Sender: TObject);
    procedure RzToolButton18Click(Sender: TObject);
    procedure tvEdited(Sender: TObject; Node: TTreeNode; var S: string);
    procedure tvDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure tvDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure tvKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure RzToolButton15Click(Sender: TObject);
    procedure RzToolButton23Click(Sender: TObject);
    procedure RzToolButton24Click(Sender: TObject);
    procedure RzToolButton1Click(Sender: TObject);
    procedure tvViewColumn1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure tvViewEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure tvViewEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure tvViewEditValueChanged(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem);
    procedure tvViewKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure cxTextEdit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RzToolButton21Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure tvViewDblClick(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure dsMakingsAfterInsert(DataSet: TDataSet);
    procedure dsMakingsAfterScroll(DataSet: TDataSet);
    procedure dsMakingsAfterEdit(DataSet: TDataSet);
    procedure RzToolButton4Click(Sender: TObject);
    procedure tvViewColumnSizeChanged(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
    procedure RzToolButton2Click(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure N5Click(Sender: TObject);
    procedure N6Click(Sender: TObject);
    procedure N7Click(Sender: TObject);
    procedure N11Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure N12Click(Sender: TObject);
    procedure tvClick(Sender: TObject);
    procedure N13Click(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure RzToolButton5Click(Sender: TObject);
    procedure RzToolButton6Click(Sender: TObject);
  private
    { Private declarations }
    dwInsertCount  : Integer;
    g_TreeView     : TNewUtilsTree;
    g_IsDataPost   : Boolean;
    g_NewRowCount  : Integer;
    g_SelectNodeId : Integer;
    IsSave :Boolean;
    dwADO:TADO;
    //function IsDeleteEmptyData(lpView: TcxGridDBTableView):Boolean;
    function IstreeEdit(S : string ; Node: TTreeNode):Boolean;
    procedure TreeAddName(Tv : TTreeView); //树增加名称
    procedure UpDateIndex(lpNode : TTreeNode);
    function GetRowId(lpRowCount : Integer):string;
    procedure GetViewList(Sender: TObject; Node: TTreeNode);
    procedure SetcxGrid(lpGridView : TcxGridDBTableView ; index : Byte ;
lpSuffix:string);
  public
    { Public declarations }
    dwParentHandle : HWND;
    dwIsReadonly : Boolean;
  end;

var
  frmMakings: TfrmMakings;

implementation

uses
   uDataModule,global , Clipbrd , ufrmMakingsPost , ufrmReport,ufrmIsViewGrid;

{$R *.dfm}
{
function TfrmMakings.IsSaveData():Boolean;
begin
  
  if IsSave then
  begin
    if MessageBox(handle, '有数据未保存,是否需要保存？', '提示', MB_ICONQUESTION
              + MB_YESNO) = IDYES then
    begin
      Self.qry.UpdateBatch(arAll);
      IsSave := False;
      Self.qry.Requery();
    end;
  end;

end;
}
procedure TfrmMakings.UpDateIndex(lpNode : TTreeNode);
var
  s : string;
begin
  S := 'UPDATE ' + g_Table_CompanyTree + ' SET TreeIndex=''' + IntToStr(lpNode.Index) + ''' WHERE SignName="' + lpNode.Text + '"';
  with DM.Qry do
  begin
    Close;
    SQL.Clear;
    SQL.Text := s;
    ExecSQL;
  end;
end;

procedure TfrmMakings.TreeAddName(Tv : TTreeView); //树增加名称
var
  Node , ChildNode : TTreeNode;
  s : string;

begin
  with Tv do
  begin
    Node := Selected;
    if Assigned(Node) then
    begin
    //  s := GetNewBHStr(g_Table_CompanyTree,'ID','SIGN',10);
      g_TreeView.AddChildNode(s,'',Date,m_OutText);
    end;
  end;

end;

function TfrmMakings.IstreeEdit(S : string ; Node: TTreeNode):Boolean;
var
  szCodeList : string;
  PrevNode , NextNode : TTreeNode;

begin
  if s = m_OutText then
  begin
    treeCldnode(Node,szCodeList);
    g_TreeView.DeleteTree(Node);
  end else
  begin
    g_TreeView.ModifyNodeSignName(s,Node);
    UpDateIndex(Node);
    PrevNode   := Tv.Selected.getPrevSibling;//上
    if PrevNode.Index <> -1 then UpDateIndex(PrevNode);
    NextNode   := Tv.Selected.getNextSibling;//下
    if NextNode.Index <> -1 then UpDateIndex(NextNode);
  end;
end;

procedure BarCodePrint(  Source : TDataSource );
begin

  Application.CreateForm(TfrmReport,frmReport);
  try
    frmReport.dwIsReportTitle := False; //是否输出标题
    frmReport.frxDBDataset.DataSource := Source;
    frmReport.frxDBDataset.UserName   := '商品管理';
    frmReport.dwReportName := '条码';
    frmReport.ShowModal;
    {
    frmReport.Show;
    frmReport.cxButton3.Click;
    frmReport.Close;
    }
  finally
    frmReport.Free;
  end;

end;

procedure TfrmMakings.N11Click(Sender: TObject);
begin
  inherited;
  GetViewList(Sender,tv.Selected);
end;

procedure TfrmMakings.N12Click(Sender: TObject);
  procedure GridCopy(View : TcxGridDBTableView);
  var
    m , n : Integer;
  begin

    if View.OptionsSelection.MultiSelect then //多选复制
    begin
      View.CopyToClipboard(False);
    end else //单选只复制单元格
    with View,View.Controller,View.DataController do
    begin
      m := FocusedItemIndex;
      n := FocusedRowIndex;
      Clipboard.AsText := VarToStr(GetValue(n,m));
    end;

    if Clipboard.AsText <> '' then
    begin
      Application.MessageBox( '复制成功！' ,'提示:', MB_OK + MB_ICONINFORMATION);
    end;
  end;

begin
  inherited;
  GridCopy(tvView);
end;

procedure TfrmMakings.N13Click(Sender: TObject);
var
  Row,Col :TStringList;
  r,c:Integer;
  Copystrings:Tstringlist;
  s : string;
  i,j,row1,row2:Integer;

begin
  inherited;
  s := SplitClipboardStr(Clipboard.AsText);
  Row := TStringList.Create;
  Row := SplitString(s,'@');

  for i := 0 to  Row.Count - 2 do
  begin
    Application.ProcessMessages;
    if Length(  Row[i] ) <> 0 then
    begin
      Col := Splitstring(Row[i],'|');
      dsMakings.First;
      dsMakings.Insert;
      for J := 0 to Col.Count-1 do
      begin
        
        with tvView do
        begin
          if (Columns[j].Index <> tvViewColumn6.Index) and 
             (Columns[j].Index <> tvViewColumn7.Index) then
          begin
            tvView.Columns[j].EditValue := Col[j];   
          end; 
           
        end;  
        
      end;
      dsMakings.Post; 
      
    end;

  end;

end;

procedure TfrmMakings.N1Click(Sender: TObject);
begin
  inherited;
  BarCodePrint(Self.ds1);
  {
  Application.CreateForm(TfrmReport,frmReport);
  try
    frmReport.dwIsReportTitle := False; //是否输出标题
    frmReport.frxDBDataset.DataSource := Self.ds1;
    frmReport.frxDBDataset.UserName   := '商品管理';
    frmReport.dwReportName := '条码';
    frmReport.ShowModal;
  finally
    frmReport.Free;
  end;
  }
end;

procedure TfrmMakings.N2Click(Sender: TObject);
begin
  inherited;
  DM.BasePrinterLink1.Component := Self.Grid;
  DM.BasePrinterLink1.ReportTitleText := '';
  DM.BasePrinter.Preview(True, nil);
end;

procedure TfrmMakings.N3Click(Sender: TObject);
var
  I: Integer;
  lvValue : Variant;
  j: Integer;
  lvFieldName : Variant;
begin
  inherited;
  dxSelectPrint.Active := False;
  dxSelectPrint.Active := True;
  with Self.tvView , Self.tvView.DataController , Self.tvView.Controller do
  begin
    BeginUpdate();
    for i := 0 to SelectedRowCount-1 do
    begin
      FocusedRow := SelectedRows[i];

      dxSelectPrint.Append;
      for j := 0 to Dataset.Fields.Count -1 do
      begin

        lvFieldName := DataSet.Fields[j].FieldName;
        if 'Id'  <> lvFieldName then
        begin
          lvValue := DataSet.FieldByName( lvFieldName ).Value;
          if lvValue <> null then
          begin
            dxSelectPrint.FieldByName(lvFieldName).Value := lvValue;
          end;
        end;
      end;
      dxSelectPrint.Post;
    end;
    EndUpdate;
  end;
  BarCodePrint(Self.dsSelectPrint);
end;

procedure TfrmMakings.N4Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton15.Click;
end;

procedure TfrmMakings.N5Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton2.Click;
end;

procedure TfrmMakings.N6Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton16.Click;
end;

procedure TfrmMakings.N7Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton17.Click;
end;

function IscxGridFocused(aGrid:TcxGrid):Boolean;
var
  AContainer: TcxCustomEdit;
  aSite:TcxGridSite;  
begin  
  Result := False;
  if aGrid.ActiveView=nil then  
    Exit;  
  aSite := aGrid.ActiveView.Site;  
  if Screen.ActiveControl is TcxGridSite then  
    Result := (TcxGridSite(Screen.ActiveControl)=aSite)  
  else  
  begin  
    AContainer := nil;  
    if Screen.ActiveControl is TcxCustomEdit then  
      AContainer := TcxCustomEdit(Screen.ActiveControl)  
    else if (Screen.ActiveControl.Parent<>nil)and  
      (Screen.ActiveControl.Parent is TcxCustomEdit) then  
      AContainer := TcxCustomEdit(Screen.ActiveControl.Parent);
  
    if Assigned(AContainer) then  
      Result := (AContainer.Parent is TcxGridSite)and(TcxGridSite(AContainer.Parent)=aSite);  
  end;      
end;

procedure TfrmMakings.RzToolButton15Click(Sender: TObject);
var
  szRowCount : Integer;
  szRecNo : Integer;
  szRowIndex : Integer;
  I : Integer;
  s : string;
begin
  if Sender = Self.RzToolButton15 then
  begin
    Application.CreateForm(TfrmMakingsPost,frmMakingsPost);
    try
      frmMakingsPost.dwNodeId  := g_SelectNodeId;
      frmMakingsPost.dwTableName := g_Table_MakingsList;
      frmMakingsPost.ShowModal;
    finally
      frmMakingsPost.Free;
    end;
    {                                                      
    //新增
    Append;
    Self.tvView.Columns[1].FocusWithSelection;
  //  IsSave :=True;
  //  Inc(g_NewRowCount);
    Self.Grid.SetFocus;
    keybd_event(VK_RETURN,0,0,0);
    }
  end else
  if Sender = Self.RzToolButton16 then
  begin
    if dwADO.UpdateTableData(g_Table_MakingsList,tvViewColumn6.DataBinding.FieldName,0,dsMakings) then
    begin
      dwInsertCount := 0;
       if MessageBox(handle, '数据保存成功', '提示', MB_ICONQUESTION
          + MB_YESNO) = IDYES then
      Self.dsMakings.MergeChangeLog;
      GetViewList(Sender,tv.Selected);
    end;  
  end else
  if Sender = Self.RzToolButton17 then
  begin
    //删除
    IsDelete(Self.tvView);
  //  DeleteSelection(Self.tvView,'',g_Table_MakingsList,'');
  //  Self.qry.Requery();
  end;
end;


function InDeleteData(ATable,code : string):Integer;stdcall;
var
  s : string;
begin

  if Length(ATable) <>  0 then
  begin
  //  s := 'delete * from ' + ATable + ' where TreeId =' + Code  + '';
    s := 'delete * from '+ ATable +' where Sign_Id in('+ Code  +')';
    with  DM.Qry do
    begin
      Close;
      SQL.Clear;
      SQL.Text := s;
      Result   := ExecSQL;
    end;

  end;

end;

procedure TfrmMakings.RzToolButton18Click(Sender: TObject);
var
  Node : TTreeNode;
  szCodeList : string;

begin
  if Sender = Self.RzToolButton18 then
  begin
    TreeAddName(Self.tv);
  end else
  if Sender = Self.RzToolButton19 then
  begin
    Self.tv.Selected.EditText;
  end else
  if Sender = Self.RzToolButton20 then
  begin
     if MessageBox(handle, '是否删除材料分类节点？', '提示', MB_ICONQUESTION
    + MB_YESNO) = IDYES then
    begin
        Node := Self.Tv.Selected;
        if Assigned(Node) then
        begin
          GetIndexList(Node,szCodeList);
          InDeleteData(g_Table_MakingsList,szCodeList);
          g_TreeView.DeleteTree(Node);
          Self.dsMakings.Close;
        //  Self.qry.Requery();
        end;
    end;

  end;

end;

procedure TfrmMakings.RzToolButton1Click(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TfrmMakings.RzToolButton21Click(Sender: TObject);
var
  s : string;
  lvSQLText : string;
begin
  inherited;
  s := Self.cxTextEdit2.Text;
  lvSQLText :=  'Select * from '+ g_Table_MakingsList +
                ' WHERE Sign_Id=' + IntToStr(g_SelectNodeId) +
                ' AND ' + tvViewColumn2.DataBinding.FieldName + '="' + s + '"';
  dwADO.OpenSQL(dsMakings,lvSQLText);
end;

procedure TfrmMakings.RzToolButton23Click(Sender: TObject);
var
  PrevNode , NextNode , SourceNode , TargetNode : TTreeNode;
begin
  PrevNode   := Tv.Selected.getPrevSibling;
  NextNode   := Tv.Selected.getNextSibling;
  SourceNode := Tv.Selected;
  if (PrevNode.Index <> -1) then
  begin
    if NextNode <> nil then
      PrevNode.MoveTo(NextNode, naInsert)
    else
      PrevNode.MoveTo(SourceNode, naAdd);
  end;

  SourceNode := Tv.Selected; //中
  UpDateIndex(SourceNode);
//  PrevNode   := Tv.Selected.getPrevSibling;//上
//  UpDateIndex(PrevNode);
  NextNode   := Tv.Selected.getNextSibling;//下
  UpDateIndex(NextNode);
end;


procedure TfrmMakings.RzToolButton24Click(Sender: TObject);
var
  OldNode , Node , SourceNode ,PrevNode: TTreeNode;
begin
  Node := Self.Tv.Selected;
  OldNode := Node.getNextSibling;
  if OldNode.Index <> -1 then
  begin
  //  ShowMessage('Index:' + IntToStr(OldNode.Index));
    OldNode.MoveTo(Node, naInsert);

    SourceNode := Tv.Selected; //中
    UpDateIndex(SourceNode);
    PrevNode   := Tv.Selected.getPrevSibling;//上
    UpDateIndex(PrevNode);
  //  NextNode   := Tv.Selected.getNextSibling;//下
  //  UpDateIndex(NextNode);
  end;
end;

procedure TfrmMakings.RzToolButton2Click(Sender: TObject);
var
  lvGoodsNo : string;
begin
  inherited;
  with dsMakings do
  begin
    if ( State = dsEdit ) or (State = dsInsert) then
    begin
      Application.MessageBox('需要保存后才可以编辑','提示：',MB_OK + MB_ICONWARNING)  ;
      Exit;
    end;  
    
  end;  
  lvGoodsNo := Self.tvViewColumn6.EditValue;
  Application.CreateForm(TfrmMakingsPost,frmMakingsPost);
  try
    frmMakingsPost.dwIsEdit  := True;
    frmMakingsPost.dwNodeId  := g_SelectNodeId;
    frmMakingsPost.dwGoodsNo := lvGoodsNo;
    frmMakingsPost.dwTableName := g_Table_MakingsList;
    frmMakingsPost.ShowModal;
  finally
    frmMakingsPost.Free;
  end;
end;

procedure TfrmMakings.SetcxGrid(lpGridView : TcxGridDBTableView ; index : Byte ;
lpSuffix:string);
var
  i : Integer;
begin
  for I := 0 to lpGridView.ColumnCount - 1 do
  begin
    Application.ProcessMessages;
    SetGrid(lpGridView.Columns[i],index, lpSuffix);
  end;
end;

procedure TfrmMakings.RzToolButton4Click(Sender: TObject);
var
  Child : TfrmIsViewGrid;
  i : Integer;  //表格设置
begin
  inherited;
  //http://www.a-hospital.com/
  Child := TfrmIsViewGrid.Create(Application);
  try
    Child.g_fromName := Name;
    Child.ShowModal;
    SetcxGrid(tvView,0, Name);
  finally
    Child.Free;
  end;
end;

procedure TfrmMakings.RzToolButton5Click(Sender: TObject);
{
'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=' + '%s' +
                ';Extended Properties="Excel 8.0;HDR=YES;IMEX=0";Persist Security Info=False';
                }
  procedure ExcelToCxGrid(cxGrid :  TcxGridDBTableView ;   aSQLText:string='Select * FROM ');
  var
    szConnect : string;
    I: Integer;
    szADOQuery : TADOQuery;
    szOpenDlg: TOpenDialog;
    j : Integer;
    s : string;
    FieldName : string;
    UserName : string;
    ExcelApp : Variant;
    szFildList : TStringList;
    szConn : TADOConnection;
    szSheetName : string;
  begin
    {
    IMEX 有三种模式，各自引起的读写行为也不同，容後再述：
         0 is Export mode
         1 is Import mode
         2 is Linked mode (full update capabilities
    }
    szOpenDlg := TOpenDialog.Create(nil);
    try

      if szOpenDlg.Execute then
      begin
        szConnect  := Format(Connect,[szOpenDlg.FileName]) ;
        szConn := TADOConnection.Create(nil);
        try
          szConn.LoginPrompt := False;
          szconn.ConnectionString := szConnect;
          szconn.Connected := True;

          szFildList := TStringList.Create;
          try
            szConn.GetTableNames(szFildList,False);
            if szFildList.Count <> 0 then szSheetName := szFildList.Strings[0];
          finally
            szFildList.Free;
          end;

          if szSheetName = '' then
          begin
            ShowMessage('引擎找不到工作本');
            Exit;
          end;

            szADOQuery := TADOQuery.Create(nil);
            try

              with szADOQuery do
              begin
                Connection := szConn;
                Close;
                SQL.Text := aSQLText+' [' +  szSheetName + ']'; //$
                Parameters.Clear;
                ParamCheck := false;
                Open;
                if RecordCount <> 0 then
                begin

                  while not Eof do
                  begin
                    Application.ProcessMessages;
                    cxGrid.DataController.DataSet.Append;
                    //if not DataController.DataSet.Active then  DataController.DataSet.Active:=True;
                    for I := 0 to Fields.Count-1 do
                    begin
                      FieldName := Fields[i].FieldName;
                      s := FieldByName(  FieldName ).AsString;
                      with cxGrid do
                      begin

                        BeginUpdate;
                        for j := 0 to ColumnCount-1 do
                        begin
                          if ( tvViewColumn1.Caption <> FieldName ) and
                             ( tvViewColumn6.Caption <> FieldName )  and
                             ( tvViewColumn7.Caption <> FieldName )then
                          begin
                            if Columns[j].Caption = FieldName then
                            begin

                               UserName := Columns[j].DataBinding.FieldName;
                               if UserName <> '' then
                               begin
                                 DataController.DataSet.FieldByName(UserName).AsString := s;
                               end;

                            end;

                          end;

                        end;
                        EndUpdate;
                      end;

                    end;
                    Next;
                  end;

                end;


              end;
            finally
              szADOQuery.Free;
            end;
          szConn.Connected := False;
        finally
          szConn.Free;
        end;

      end;

    finally
      szOpenDlg.Free;
    end;

  end;

begin
  inherited;
  ExcelToCxGrid(tvView);
end;

procedure TfrmMakings.RzToolButton6Click(Sender: TObject);
begin
  inherited;
  CxGridToExcel(Self.Grid,'材料明细表');
end;

procedure TfrmMakings.GetViewList(Sender: TObject; Node: TTreeNode);
var
  s : string;
  PData: PNodeData;
  szRowCount : Integer;
  lvSQLText : string;
begin
  inherited;

  if Node.Level >= 1 then
  begin

    PData := PNodeData(Node.Data);
    if Assigned(PData) then
    begin
      g_SelectNodeId := PData^.Index;
      lvSQLText :=  'Select * from '+ g_Table_MakingsList +' WHERE Sign_Id=' + IntToStr(g_SelectNodeId);
      dwADO.OpenSQL(dsMakings,lvSQLText);
    end;
  end else
  begin
    Self.dsMakings.Close;
  end;
end;


procedure TfrmMakings.tvClick(Sender: TObject);
begin
  inherited;
  GetViewList(Sender,tv.Selected);
end;

procedure TfrmMakings.tvDragDrop(Sender, Source: TObject; X, Y: Integer);
var
  TargetNode, SourceNode: TTreeNode;

begin
  TargetNode := tv.DropTarget;
  SourceNode := tv.Selected;
  if MessageBox(handle, '您确认要移动合同吗？', '提示', MB_ICONQUESTION
    + MB_YESNO) = IDYES then
    if g_TreeView.ModifyNodePID(PNodeData(TargetNode.Data)^.Index, SourceNode) then
       g_TreeView.GetTreeTable(0);

end;

procedure TfrmMakings.tvDragOver(Sender, Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
begin
  if (Source = Self.Tv) then
     Accept := True
  else
     Accept := False;
end;

procedure TfrmMakings.tvEdited(Sender: TObject; Node: TTreeNode; var S: string);
begin
  IstreeEdit(s,Node);
end;

procedure TfrmMakings.tvKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    13:
      begin
        Self.tv.Selected.Parent.Selected := True;
        Self.RzToolButton18.Click;
      end;
    46:
      begin
        Self.RzToolButton20.Click;
      end;
  end;
end;

procedure TfrmMakings.tvViewColumn1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  inherited;
  AText := IntToStr(ARecord.Index + 1);
end;

procedure TfrmMakings.tvViewColumnSizeChanged(Sender: TcxGridTableView;
  AColumn: TcxGridColumn);
begin
  inherited;
  SetcxGrid(tvView,1, Name);
end;

procedure TfrmMakings.tvViewDblClick(Sender: TObject);
begin
  inherited;
  if dwIsReadonly then
  begin
    Close;
  end else
  begin
    if tvView.Controller.FocusedRowIndex <> -1 then
    begin
      Self.RzToolButton2.Click;
    end;
  end;
end;

procedure TfrmMakings.tvViewEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  inherited;
  dwADO.ADOIsEditing(Self.dsMakings,AAllow);
  if AItem.Index = Self.tvViewColumn1.Index then
  begin
    AAllow := False;
  end;
end;

procedure TfrmMakings.tvViewEditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
  begin
    if AItem.Index = Self.tvViewColumn3.Index then
    begin
      Self.tvView.DataController.Append;
      Self.tvView.Columns[0].FocusWithSelection;
    end;

  end;  

end;

procedure TfrmMakings.tvViewEditValueChanged(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem);
begin
  inherited;
  IsSave := True;
end;

procedure TfrmMakings.tvViewKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = 46 then
  begin
    Self.RzToolButton17.Click;
  end;
end;

procedure TfrmMakings.cxTextEdit2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = 13 then
  begin
    Self.RzToolButton21.Click;
  end;
end;

procedure TfrmMakings.dsMakingsAfterEdit(DataSet: TDataSet);
begin
  inherited;
  if not DataSet.IsEmpty then
  begin
    IsSave := True;
  end;
end;



//查找行数
function GetRowCount(ATabName , AFieldName , APrefix : string):Integer;stdcall;
var
  i : Integer;
  s : string;
begin
  Result := 0;
  s := 'SELECT Count(*) FROM ' + ATabName + ' Where Sign_Id=' + APrefix + ' AND '+ AFieldName +' <> ""'  ;
  OutputLog(s);
  with DM.Qry do
  begin
    Close;
    SQL.Clear;
    SQL.Text := s;
    Open;
  //  Sort := AFieldName + ' ASC';
    if RecordCount <> 0 then
    begin
      Result := Fields[0].AsInteger
    end;
  end;
  Inc(result);
end;

function GetLastLine(ATabName , AFieldName , APrefix : string):string;
var
  i : Integer;
  s : string;
begin
  Result := '0';
  s := 'SELECT '+ AFieldName +' FROM ' +  ATabName + ' Where Sign_Id=' + APrefix + ' AND '+ AFieldName +' <> ""'  ;
  OutputLog(s);
  with DM.Qry do
  begin
    Close;
    SQL.Text := s;
    Open;
    Sort := AFieldName + ' ASC ';
    Last;
    if RecordCount <> 0 then
      i := FieldByName(AFieldName).AsInteger
  end;
  Inc(i);
  Result := IntToStr(i);
end;

function TfrmMakings.GetRowId(lpRowCount : Integer):string;
var
  I: Integer;
  s: string;
  
begin
  //跟距行数计算ID
  for I := 0 to 5 do s := s + '0';
  i := StrToIntDef(  IntToStr(g_SelectNodeId) + s , 0) + lpRowCount + Self.dsMakings.ChangeCount;
  Result := IntToStr(i);
  
end;

function IsIDExists(ATabName , AFieldName , APrefix ,AID : string):Boolean;
var
  s : string;
begin
  s := 'SELECT '+ AFieldName +' FROM ' +  ATabName + ' Where Sign_Id=' + APrefix + ' AND '+ AFieldName +'="' + AID + '"';
  OutputLog(s);
  with DM.Qry do
  begin
    Close;
    SQL.Text := s;
    Open;
    Result := RecordCount <> 0;
  end;
end;


procedure TfrmMakings.dsMakingsAfterInsert(DataSet: TDataSet);

  function GetMaxId():Integer;
  var
    lvSQLText : string;
    
  begin
    lvSQLText :=  'Select MAX(GoodsNo) from '+ g_Table_MakingsList;
    with DM.Qry do
    begin
      Close;
      SQL.Text := lvSQLText;
      Open;
      if RecordCount <> 0 then  Result := Fields[0].AsInteger
    end;  
  end; 
  
  function IsExists(lpId : string):Boolean;
  var
    I: Integer;
    
  begin
    Result := False;
    with tvView do
    begin
      
      for I := 0 to ViewData.RowCount-1 do
      begin
        if SameText(VarToStr( ViewData.Rows[i].Values[tvViewColumn6.Index] ) , lpId) then
        begin
          Result := True;
          
          Exit;
        end;  
        
      end; 
       
    end; 
     
  end; 

  function GetIndex():string;
  var
    lvFieldName : string;
    lvIndex : string;
    i: Integer;
    
  begin
    lvFieldName := Self.tvViewColumn6.DataBinding.FieldName;
    lvIndex := GetRowId( GetRowCount(g_Table_MakingsList,lvFieldName,IntToStr(g_SelectNodeId) ) ) ;
    while IsExists(lvIndex) do
    begin
      i := StrToIntDef(lvIndex,0);
      if i = 0 then
      begin
        Break;
      end;  
      Inc(i);
      lvIndex := IntToStr(i);
    end; 
    Result := lvIndex;
    { 
    if IsExists( lvIndex ) then
    begin
      Self.mmo1.Lines.Add('存在:' + lvIndex);
    end; 
    }
    {
    if IsIDExists(g_Table_MakingsList,lvFieldName,IntToStr(g_SelectNodeId),lvIndex) then
    begin
      lvIndex := GetLastLine(  g_Table_MakingsList,lvFieldName,IntToStr(g_SelectNodeId)  );
    end;
    }
  end;   
var
  DD2 : string;
  s : string;
  szId : string;
  szBarCode : string;
  i : Integer;
  szFieldName : string;
begin
  inherited;
  with DataSet do
  begin
    DD2 := GetIndex();
    
    s := '';
    for I := 0 to 9 - Length( DD2 ) do
        s := s + '0';

    szId := '69' + s + DD2 ;
    FieldByName('Sign_Id').Value := g_SelectNodeId;
    FieldByName(tvViewColumn6.DataBinding.FieldName).Value := DD2;
    FieldByName(tvViewColumn8.DataBinding.FieldName).Value := Date;
    if DD2 <> '' then
    begin
      szBarCode := EAN13BarCode(szId);
      FieldByName(tvViewColumn7.DataBinding.FieldName).Value := szBarCode;
    end;
    Inc(dwInsertCount);
  
  end;
end;

procedure TfrmMakings.dsMakingsAfterScroll(DataSet: TDataSet);
var
  pMsg : TMakings;

begin
  inherited;
  with DataSet do
  begin
    if (Self.tvView.ViewData.RowCount <> 0) and ( dwIsReadonly ) then
    begin
      pMsg.dwGoodsNo := Self.tvViewColumn6.EditValue;
      pMsg.dwMakingCaption := Self.tvViewColumn2.EditValue;
      pMsg.dwModelIndex := Self.tvViewColumn4.EditValue;
      pMsg.dwSpec       := Self.tvViewColumn3.EditValue;
      pMsg.dwBrand      := Self.tvViewColumn5.EditValue;
      pMsg.dwUnitPrice  := Self.tvViewColumn11.EditValue;
      pMsg.dwBuyingPrice:= Self.tvViewColumn12.EditValue;

      
      SendMessage(dwParentHandle, WM_LisView, 0, LPARAM(@pMsg));  // 发消息
    end;
  end;
end;

procedure TfrmMakings.FormActivate(Sender: TObject);
begin
  inherited;
  SetcxGrid(tvView,0, Name);
end;

procedure TfrmMakings.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  inherited;
  dwADO.UpdateTableData(g_Table_MakingsList,tvViewColumn6.DataBinding.FieldName,0,dsMakings)
end;

procedure TfrmMakings.FormDestroy(Sender: TObject);
begin
  inherited;
  Self.dsMakings.Close;
end;

procedure TfrmMakings.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  
  if (ssCtrl in Shift) then
  begin
    case Key of
      67:;// N12.Click;
      86: N13.Click;
    end;
  end;
  
  if Key = 27 then
  begin
    Close;
  end;
end;

procedure TfrmMakings.FormShow(Sender: TObject);
begin
  g_TreeView := TNewUtilsTree.Create(Self.tv,
                                     DM.ADOconn,
                                     g_Table_MakingsTree,
                                     '材料分类');
  g_TreeView.GetTreeTable(0);
  g_NewRowCount := 0;
end;

end.
