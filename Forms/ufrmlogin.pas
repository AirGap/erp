unit ufrmlogin;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, jpeg, ExtCtrls, RzButton, StdCtrls, DB, ADODB, Mask, RzEdit, ComCtrls,
  RzBmpBtn, Menus, ImgList,pngimage,IniFiles, dxGDIPlusClasses;

type
  Tfrmlogin = class(TForm)
    Image1: TImage;
    Label2: TLabel;
    RzBmpButton1: TRzBmpButton;
    RzEdit1: TRzEdit;
    RzEdit2: TRzEdit;
    RzBmpButton2: TRzBmpButton;
    RzBmpButton3: TRzBmpButton;
    procedure RzBitBtn2Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure FormResize(Sender: TObject);
    procedure RzBmpButton1Click(Sender: TObject);
    procedure RzEdit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RzBmpButton2Click(Sender: TObject);
    procedure RzBmpButton3Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Image1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    { Private declarations }
    procedure CreateParams(var Params: TCreateParams);override;
  public
    m_Blend: BLENDFUNCTION;
    IsLogin: Boolean;
    { Public declarations }
  end;

var
  frmlogin: Tfrmlogin;

implementation

{$R *.dfm}

uses
  uDataModule,MD5,global;

procedure Tfrmlogin.CreateParams(var Params: TCreateParams);    
begin
  inherited;
  Params.WndParent := 0;    
end;

procedure Tfrmlogin.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if not m_user.dwIsLogin then ExitProcess(0);
end;

procedure Tfrmlogin.FormCreate(Sender: TObject);
var
  szIni : TIniFile;
  s : string;
  
begin

  IsLogin := False;
  szIni := TIniFile.Create(GetFilePath + '/Config.ini');
  try
    s := szIni.ReadString(m_Node,'UserId','');
    if Length(s) <> 0 then Self.RzEdit1.Text:= s
  finally
    szIni.Free;
  end;
  borderstyle:=bsnone;
  brush.Style:=bsclear;
  AlphaBlend  := True;
  AlphaBlendValue := 255;

{

设置为True，然后设置AlphaBlendValue的值就ok了，255为不透明，0为完全透明。
至于label嘛，有个Transparent属性，设为True就OK了
}
  Self.RzEdit2.PasswordChar:=char(42);
  {
  m_Blend.BlendOp := AC_SRC_OVER; //   the   only   BlendOp   defined   in   Windows   2000
  m_Blend.BlendFlags := 0; //   Must   be   zero
  m_Blend.AlphaFormat := AC_SRC_ALPHA; //This   flag   is   set   when   the   bitmap   has   an   Alpha   channel
  m_Blend.SourceConstantAlpha := 255;

  SetTransparent('','pngfile',100);
  }
  SetWindowPos(Handle, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE or SWP_NOSIZE);
end;

procedure Tfrmlogin.FormMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if (Button = mbLeft) then
  begin
    ReleaseCapture();
    Perform(WM_SYSCOMMAND, SC_MOVE or HTCAPTION, 0);
  end;
end;

procedure Tfrmlogin.FormResize(Sender: TObject);
var hr :thandle;
begin
  hr:=createroundrectrgn(1,1,width-2,height-2,6,6);
  setwindowrgn(handle,hr,true);
end;

procedure Tfrmlogin.FormShow(Sender: TObject);
begin
  Self.RzEdit2.SetFocus;
end;

procedure Tfrmlogin.Image1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if (Button = mbLeft) then
  begin
    ReleaseCapture();
    Perform(WM_SYSCOMMAND, SC_MOVE or HTCAPTION, 0);
  end;
end;

procedure Tfrmlogin.RzBitBtn2Click(Sender: TObject);
begin
  ExitProcess(0);
end;

procedure Tfrmlogin.RzBmpButton1Click(Sender: TObject);
var
  sqlstr : string;
  szUser,szPass : string;
  szIni : TIniFile;
  s : string;

begin
  szUser := Self.RzEdit1.Text;
  szPass := Self.RzEdit2.Text;
  if Length(szUser) = 0  then
  begin
    Self.RzEdit1.SetFocus;
    Application.MessageBox('登陆账号不得为空!',m_title,MB_OK + MB_ICONHAND );
  end
  else
  begin
    if Length(szPass) = 0 then
    begin
      Self.RzEdit2.SetFocus;
      Application.MessageBox('登陆密码不得为空!',m_title,MB_OK + MB_ICONHAND );
    end
    else
    begin
      sqlstr := Format('Select * from users where ch_name =''%s'' and ch_pass = ''%s''',
      [szUser,MD5s(szPass)]);
      try
        DM.Qry.Close;
        DM.Qry.SQL.Clear;
        DM.Qry.SQL.Text := sqlstr;
        DM.Qry.Open;
        if not DM.Qry.IsEmpty then
        begin
          //  GetMem(m_user,SizeOf(m_user));
          m_user.dwUserName := szUser;
          m_user.dwPassWord := szPass;
          m_user.dwIsLogin  := True;
          m_user.dwType     := DM.Qry.FieldByName('ch_type').AsInteger;

          case m_user.dwType of
            0: m_user.dwAuth := '系统管理员';
            1: m_user.dwAuth := '操作员';
            2: m_user.dwAuth := '信息员';
          end;

          szIni := TIniFile.Create(GetFilePath + '/Config.ini');
          try
            szIni.WriteString(m_Node,'UserId',szUser);
          finally
            szIni.Free;
          end;
          IsLogin := True;
          Close;
        end
        else
        begin
          Application.MessageBox('账号或密码错误!',m_title,MB_OK + MB_ICONHAND );
          Self.RzEdit2.SetFocus;
          Self.RzEdit2.Text := '';
        end;
      except
        on E : Exception do 
      end;

    end;
    
  end;

end;

procedure Tfrmlogin.RzBmpButton2Click(Sender: TObject);
begin
  ExitProcess(0);
end;

procedure Tfrmlogin.RzBmpButton3Click(Sender: TObject);
begin
  Self.WindowState:=wsMinimized;
end;

procedure Tfrmlogin.RzEdit2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then  Self.RzBmpButton1.Click;
end;

end.
