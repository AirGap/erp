unit ufrmAppropriation;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, cxGroupBox, Vcl.StdCtrls,
  cxTextEdit, cxCurrencyEdit, cxMemo, RzButton, Vcl.Menus, cxButtons,
  Vcl.ComCtrls, dxCore, cxDateUtils, cxMaskEdit, cxDropDownEdit, cxCalendar;

type
  TfrmAppropriation = class(TForm)
    cxGroupBox1: TcxGroupBox;
    Label1: TLabel;
    Label23: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label25: TLabel;
    Label5: TLabel;
    Label24: TLabel;
    Label22: TLabel;
    Label28: TLabel;
    cxMemo9: TcxMemo;
    cxMemo1: TcxMemo;
    Label2: TLabel;
    cxMemo2: TcxMemo;
    cxMemo3: TcxMemo;
    cxMemo4: TcxMemo;
    cxMemo5: TcxMemo;
    cxMemo6: TcxMemo;
    cxMemo7: TcxMemo;
    cxMemo8: TcxMemo;
    cxMemo10: TcxMemo;
    Label3: TLabel;
    cxCurrencyEdit1: TcxCurrencyEdit;
    Label4: TLabel;
    Label6: TLabel;
    RzBitBtn1: TRzBitBtn;
    RzBitBtn2: TRzBitBtn;
    cxButton1: TcxButton;
    Label7: TLabel;
    cxDateEdit1: TcxDateEdit;
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure RzBitBtn2Click(Sender: TObject);
    procedure RzBitBtn1Click(Sender: TObject);
    procedure cxMemo10KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxCurrencyEdit1PropertiesChange(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    g_PostCode    : string;
    g_ProjectDir  : string;
    g_PostNumbers : string;
    g_TableName   : string;
    g_IsModify    : Boolean;
    g_Prefix      : string;
  end;

var
  frmAppropriation: TfrmAppropriation;

implementation

uses
   global,uDataModule,UnitExpCalc,System.Math,System.StrUtils;

{$R *.dfm}

procedure TfrmAppropriation.cxButton1Click(Sender: TObject);
var
  szItemPosition : string;
  szItemUnit : string;
  szItemType : string; //规格
  szItemValue: Single;
  szCode   : string;
  szParent : string;

  str, infoStr, signParamStr, showItemStr: string;
  f1: double;
  i, len, decN, signP: integer;
  r : string;
  s : string;


begin
  str := Self.cxMemo10.Text;
  if str <> '' then
  begin
    len := Length(str);
    for i := 1 to len do
    begin
      if ord(str[i]) in [10, 13] then str[i] := ' ';    // 先去掉回车符

      if (ord(str[i]) = 163) and ((i + 1) <= len) then  // 替换全角的括号
      begin
        if ord(str[i + 1]) = 168 then
        begin
          str[i] := '(';
          str[i + 1] := ' ';
        end;

        if ord(str[i + 1]) = 169 then
        begin
          str[i] := ')';
          str[i + 1] := ' ';
        end;
      end;  // if
      if (ord(str[i]) = 163) and ((i + 1) <= len) then  // 替换全角的逗号
        if ord(str[i + 1]) = 172 then
        begin
          str[i] := ',';
          str[i + 1] := ' ';
        end;
    end;   // for
    str :=  StringReplace(str, ' ', '', [rfReplaceAll]);
    // ********************************
    InitSignParam();      // 清除前面自定义的参数，恢复常数 pi, e
    // 表达式中以 @@之后的 表示符号参数标记内容 格式为 'a=1,b=2.1,c=3'
    signP := pos('@@', str);
    if signP > 0 then
    begin
      signParamStr := midStr(str, signP + 2, Length(str) - signP - 1);
      signParamStr := StringReplace(signParamStr, ' ', '', [rfReplaceAll]);
      AddSignParam(signParamStr);
      str := leftStr(str, signP - 1);
    end;

     // ********************************
    if CalcExp(str, f1, infoStr) then
    begin
      decN := 100;
      if decN <= 0 then
      begin
        r := floatToStr(RoundTo(f1, decN))  //结果
      end
      else
      begin
        r := floatToStr(f1);  //结果
      end;

      if signP > 0 then
         showItemStr := str + '@@' + signParamStr + '  =  ' + r //结果
      else
         showItemStr := str + ' = ' + r;

      Self.cxCurrencyEdit1.Value := StrToFloatDef( r , 0 );
    end
    else
    begin

      if signP > 0 then
         showItemStr := str + '@@' + signParamStr + '  =  出错' + infoStr
      else
        showItemStr := str + ' = 出错: ' + infoStr;

      ShowMessage(showItemStr);
    end;
  end
  else
    Application.MessageBox('输入为空，请输入表达式！','提示',MB_ICONWARNING);

end;

procedure TfrmAppropriation.cxCurrencyEdit1PropertiesChange(Sender: TObject);
begin
  Self.Label6.Caption := MoneyConvert(Self.cxCurrencyEdit1.Value);
end;

procedure TfrmAppropriation.cxMemo10KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (ssCtrl in Shift) and (Key = 229) then
  begin
  //  Self.cxButton1.Click;
  end;
end;

procedure TfrmAppropriation.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
     13:
     begin
       //回车
       Self.RzBitBtn1.Click;
     end;
     27:
     begin
       //ESC
       Self.RzBitBtn2.Click;
     end;
  end;
end;

procedure TfrmAppropriation.RzBitBtn1Click(Sender: TObject);
var
  szAppropriation : string;
  szInvoice : string;
  szHydropower : string;
  szfine    : string;
  sztax     : string;
  szArmourforwood : string;
  szLtIsa   : string;
  szreceiptsMoney : Currency;
  szRemarks : string;
  sqlstr : string;
  szFormula : string;
  szDates   : string;
  szRealmoney : string;

begin
  szAppropriation := Self.cxMemo1.Text; //应拨款
  szfine          := Self.cxMemo2.Text; //罚款
  szArmourforwood := Self.cxMemo3.Text; //甲供材
  szRealmoney     := Self.cxMemo4.Text; //实拨款项
  sztax           := Self.cxMemo5.Text; //税金
  szInvoice       := Self.cxMemo6.Text; //发票
  szHydropower    := Self.cxMemo7.Text; //水电
  szLtIsa         := Self.cxMemo8.Text; //它项
  szRemarks       := Self.cxMemo9.Text; //备注
  szFormula       := Self.cxMemo10.Text;//公式
  szreceiptsMoney := Self.cxCurrencyEdit1.Value; //实收现金
  szDates         := Self.cxDateEdit1.Text;

  if g_IsModify then
  begin
    //修改
    sqlstr := Format('Update '+ g_tableName +' set remarks="%s",'+
                                        'Appropriation="%s",'+
                                        'Invoice="%s",'+
                                        'Hydropower="%s",'+
                                        'fine="%s",'+
                                        'tax="%s",'+
                                        'Armourforwood="%s",'+
                                        'LtIsa="%s",'+
                                        'ReceiptsMoney=%f,'+
                                        'Formula="%s",'         +
                                        'Dates="%s",'           +
                                        'Realmoney="%s"'        +
                                        'where Numbers="%s"',[
                                        szRemarks,
                                        szAppropriation,
                                        szInvoice,
                                        szHydropower,
                                        szfine,
                                        sztax,
                                        szArmourforwood ,
                                        szLtIsa,
                                        szReceiptsMoney,
                                        szFormula,
                                        szDates,
                                        szRealmoney,
                                        g_PostNumbers]);
    OutputLog(sqlstr);

    with DM.Qry do
     begin
       Close;
       SQL.Clear;
       SQL.Text := sqlstr;
       if 0 <> ExecSQL  then
       begin
         m_IsModify := True;
         Application.MessageBox('编辑成功' , '提示' ,MB_OK + MB_ICONINFORMATION);
       end else
       begin
         Application.MessageBox('编辑失败' , '提示' ,MB_OK + MB_ICONINFORMATION);
       end;
     end;


  end else
  begin
    //添加
    sqlstr := Format( 'Insert into '+ g_tableName +' (code,'+
                              'Numbers,'      +
                              'Remarks,'      +
                              'Appropriation,'+
                              'Invoice,'      +
                              'Hydropower,'   +
                              'fine,'         +
                              'tax,'          +
                              'Armourforwood,'+
                              'LtIsa,'        +
                              'receiptsMoney,'+
                              'Formula,'      +
                              'Dates,'        +
                              'Realmoney'     +
                              ') values("%s","%s","%s","%s","%s","%s","%s","%s","%s","%s",%f,"%s","%s","%s")',[
                                                      g_PostCode,
                                                      g_PostNumbers,
                                                      szRemarks,
                                                      szAppropriation,
                                                      szInvoice,
                                                      szHydropower,
                                                      szfine,
                                                      sztax,
                                                      szArmourforwood,
                                                      szLtIsa,
                                                      szreceiptsMoney,
                                                      szFormula,
                                                      szDates,
                                                      szRealmoney
                                                      ] );

    OutputLog(sqlstr);

    with DM.Qry do
     begin
       Close;
       SQL.Clear;
       SQL.Text := sqlstr;
       if 0 <> ExecSQL  then
       begin
         g_PostNumbers := DM.getDataMaxDate(g_TableName);
         m_IsModify := True;
         Application.MessageBox('添加成功' , '提示' ,MB_OK + MB_ICONINFORMATION);
       end else
       begin
         Application.MessageBox('添加失败' , '提示' ,MB_OK + MB_ICONINFORMATION);
       end;
     end;

  end;

end;

procedure TfrmAppropriation.RzBitBtn2Click(Sender: TObject);
begin
  Close;
end;

end.
